import sys, os
os.path.dirname(sys.executable)
import sys
sys.path.append("/home/ec2-user/bert-question-answering/src")
import numpy as np
from utils import qaclasses36 as qaclasses
import logging
import json


import tensorflow as tf

flags = tf.flags
FLAGS = flags.FLAGS
## Required parameters
flags.DEFINE_string(
    "input_file", None,
    "The bert base path corresponding to the pre-trained BERT model."
    "This specifies the model architecture.")
flags.DEFINE_string(
    "output_dir", None,
    "The output directory where the model checkpoints will be written.")
flags.DEFINE_bool("do_preprocess", True, "Whether to process File.")


def main(_):
    if FLAGS.do_preprocess:

################################## Load Necessary Config Details  ####################################


        print('loading data')
        with open(FLAGS.input_file,'r') as f:
            d = json.load(f)
        logging.info('data loaded')
        r = qaclasses.parse_squad(d)
        logging.info('data parsed')
        ids, segment_ids, input_mask, start_idx, end_idx = [np.array(_) for _ in zip(*[x.encode() for x in r])]
        np.save(FLAGS.output_dir + 'train_segment_ids.npy', segment_ids)
        np.save(FLAGS.output_dir + 'train_input_mask.npy', input_mask)
        np.save(FLAGS.output_dir + 'train_start_idx.npy', start_idx)
        np.save(FLAGS.output_dir + 'train_end_idx.npy', end_idx)
        np.save(FLAGS.output_dir + 'train_ids.npy', ids)
        logging.info('data encoded')


if __name__ == "__main__":
  # flags.mark_flag_as_required("bert_base")
  # flags.mark_flag_as_required("resource_dir")
  flags.mark_flag_as_required("do_preprocess")
  tf.app.run()

