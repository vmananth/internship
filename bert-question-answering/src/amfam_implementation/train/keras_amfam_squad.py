import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
from bertfam import load_pretrained_tf_model, load_model, BertBertTokenizer
from tensorflow.python.keras import Model
from tensorflow.python.keras.layers import Dense, Lambda
from tensorflow.python.keras.activations import softmax
import numpy as np
from tensorflow.python import keras
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras.utils import multi_gpu_model
from tensorflow.python.keras.callbacks import EarlyStopping, ModelCheckpoint

import tensorflow as tf

flags = tf.flags
FLAGS = flags.FLAGS

flags.DEFINE_string(
    "bertfam_model", None,
    "The bert base path corresponding to the pre-trained BERT model."
    "This specifies the model architecture.")
flags.DEFINE_string(
    "resource_dir", None,
    "The output directory where the model checkpoints will be written.")
flags.DEFINE_bool("do_train", True, "Whether to train File.")


def main(_):
    if FLAGS.do_train:

################################## Load Necessary Config Details  ####################################

        output_dir = FLAGS.resource_dir+ '/processed_model/amfam/amfam_keras_model/'
        input_dir = FLAGS.resource_dir+ '/preprocessed_data/amfam/'
        logdir= FLAGS.resource_dir+ '/processed_model/amfam/amfam_keras_model/scalars/'
        tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir, update_freq='batch')
        es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=2)
        # checkpoint
        filepath = output_dir + "weights.best.hdf5"
        checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
        BERTFAM_MODEL = FLAGS.bertfam_model


################################# Load Processed Data #################################################

        print('loading input')
        ids = np.load(input_dir + 'train_ids.npy')
        # mask = ids!=0
        segment_ids = np.load(input_dir + 'train_segment_ids.npy')
        input_mask = np.load(input_dir + 'train_input_mask.npy')
        start_idx = np.load(input_dir + 'train_start_idx.npy')
        end_idx = np.load(input_dir + 'train_end_idx.npy')
        target_shape = list(ids.shape) + [1]  # [ids.shape[0],1,ids.shape[1]]#
        start_ids = np.zeros(target_shape, dtype=int)
        end_ids = np.zeros(target_shape, dtype=int)
        y_answerable = np.zeros(2)
        for i, idx in enumerate(start_idx):
            start_ids[i, idx, 0] = 1

        for i, idx in enumerate(end_idx):
            end_ids[i, idx, 0] = 1


        print('input loaded')

        # print(ids.shape,segment_ids.shape,input_mask.shape,start_ids.shape,end_ids.shape)

        print('ids shape: ', ids.shape)
        print('segment shape: ', segment_ids.shape)
        print('Start index shape: ', start_ids.shape)
        print('End Index shape: ', end_ids.shape)

############### Load Google Pretrained Bert for down stream question answering task  #################

        model = load_pretrained_tf_model(pooled_output=True, seg_ids=True)
        encoding_out = model.get_layer('seq_output').layers[-1].multi_head_attention.output[0]


        def softmx(x):
            return softmax(x, axis=1)


        start_layer = Dense(1, activation=softmx)
        start_out = start_layer(encoding_out)
        end_layer = Dense(1, activation=softmx)
        end_out = end_layer(encoding_out)

        squad_model = Model(model.inputs, [start_out, end_out])

        # # define the model with the same inputs as the original model and the new outputs
        # model = Model(model.inputs, [start_out, end_out])

        model_json = squad_model.to_json()
        with open(output_dir + "model_amfam_keras_squad_google.json", "w") as json_file:
            json_file.write(model_json)


        optimizer = Adam(lr=1e-5)
        # parallel_squad_model = squad_model  # p3.8xl


        squad_model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])

        squad_model.summary()

        squad_model.fit([ids, input_mask, segment_ids], [start_ids, end_ids], batch_size=12, validation_split=0.2, epochs=20,
                                 verbose=1,
                                 callbacks=[tensorboard_callback, es, checkpoint])

        print('finished training')
        print('saving model')
        output_filepath = output_dir + 'model_amfam_keras_squad_google.hdf5'
        squad_model.save_weights(output_filepath)



if __name__ == "__main__":
  flags.mark_flag_as_required("bertfam_model")
  flags.mark_flag_as_required("resource_dir")
  flags.mark_flag_as_required("do_train")
  tf.app.run()

