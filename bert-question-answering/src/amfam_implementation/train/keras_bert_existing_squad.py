# import unittest
# from bertmaster import modeling
from keras_bert import load_trained_model_from_checkpoint, Tokenizer
import json
import numpy as np
from keras.layers import Dense
from keras.models import Model
from keras.activations import softmax
from keras.callbacks import ModelCheckpoint
from keras.optimizers import Adam
from keras.utils import multi_gpu_model
import sys

version = 'v2'
output_dir = '/home/ec2-user/bert-question-answering/resources/processed_model/amfam/initial_keras_model/'
logdir= '/home/ec2-user/bert-question-answering/resources/processed_model/amfam/initial_keras_model/scalars_google/'
input_dir = '/home/ec2-user/bert-question-answering/resources/preprocessed_data/amfam/'


bert_layer_num = 11
bln = (int(bert_layer_num) - 12)*4 - 1


# tf.train.list_variables(model checkpoint file)
# base_path = '/Users/dxd036/projects/stanford-qa/bertmaster/uncased_L-12_H-768_A-12'
base_path = '/home/ec2-user/uncased_L-12_H-768_A-12'

config_path = base_path + '/bert_config.json'
checkpoint_path = base_path + '/bert_model.ckpt'
dict_path = base_path + '/vocab.txt'
input_data = 'train-v2.0.json'#'dev-v2.0.json'#'train-v2.0.json'#'dev-v2.0.json'#'test_sample_squad.json'#'dev-v2.0.json'


model = load_trained_model_from_checkpoint(config_path, checkpoint_path)
# model.summary(line_length=120)
encoding_out = model.layers[bln].output

def softmx(x):
    return softmax(x,axis=1)

start_layer = Dense(1,activation=softmx)
start_out = start_layer(encoding_out)
end_layer = Dense(1,activation=softmx)
end_out = end_layer(encoding_out)

squad_model = Model(model.inputs,[start_out,end_out])
# squad_model.load_weights('weights-1.hdf5')

# with open(input_data,'r') as f:
#     d = json.load(f)
#
# logging.info('data loaded')
# r = qaclasses.parse_squad(d)
# logging.info('data parsed')
# ids,segment_ids,input_mask,start_idx,end_idx = [np.array(_) for _ in zip(*[x.encode() for x in r])]
# logging.info('data encoded')
# target_shape = list(ids.shape) + [1]#[ids.shape[0],1,ids.shape[1]]#
# start_ids = np.zeros(target_shape,dtype=int)
# end_ids = np.zeros(target_shape,dtype=int)
# for i,idx in enumerate(start_idx):
#     start_ids[i,idx,0] = 1
#
# for i,idx in enumerate(end_idx):
#     end_ids[i,idx,0] = 1

print('loading input')
ids = np.load(input_dir + 'train_ids.npy')
mask = ids!=0
segment_ids = np.load(input_dir + 'train_segment_ids.npy')
input_mask = np.load(input_dir + 'train_input_mask.npy')
start_idx = np.load(input_dir + 'train_start_idx.npy')
end_idx = np.load(input_dir + 'train_end_idx.npy')
target_shape = list(ids.shape) + [1]#[ids.shape[0],1,ids.shape[1]]#
start_ids = np.zeros(target_shape,dtype=int)
end_ids = np.zeros(target_shape,dtype=int)
for i,idx in enumerate(start_idx):
    start_ids[i,idx,0] = 1

for i,idx in enumerate(end_idx):
    end_ids[i,idx,0] = 1

# print(ids.shape,segment_ids.shape,input_mask.shape,start_ids.shape,end_ids.shape)

model_json = squad_model.to_json()

with open(output_dir + "model_keras_squad_google.json", "w") as json_file:
    json_file.write(model_json)

for layer in squad_model.layers:
    if layer.count_params() > 0:
        layer.trainable = True


optimizer = Adam(lr=1e-5)
parallel_squad_model = multi_gpu_model(squad_model,gpus=4)#p3.8xl
parallel_squad_model.compile(loss='binary_crossentropy',optimizer=optimizer)

#max batch size on 1 gpu is 6, using 4 so batch size is 24
parallel_squad_model.fit([ids,segment_ids],[start_ids,end_ids],batch_size=24,epochs=2)

print('saving model')

output_filepath = output_dir + 'model_keras_squad_google.hdf5'
squad_model.save_weights(output_filepath)
print('done')

if False:
    x = squad_model.predict([ids,segment_ids])
    ys = x[0][0]
    ye = x[1][0]

    for y in zip(zip(*x),start_ids,end_ids):
        print(y[0][0].argmax(),y[0][1].argmax(),y[1].argmax(),y[2].argmax())


    ####scratch below, works above

    r[2].encode()
    ids,segment_ids,input_mask,start_idx,end_idx = r[2].encode()
    x = squad_model.predict([[np.array(ids)],[np.array(segment_ids)]])
    ys = x[0]
    ye = x[1]

    embedding = model.predict([[np.array(ids)],[np.array(segment_ids)]])