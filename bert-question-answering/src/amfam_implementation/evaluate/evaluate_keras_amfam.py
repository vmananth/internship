import collections

import re
import string
import sys
import sys
sys.path.append("/home/ec2-user/bert-question-answering/src")
from utils.qaclasses36 import QuestionContext
from utils.qaclasses36 import parse_squad
from bertfam.utils import get_custom_objects
from tensorflow.keras.models import Model, model_from_json,load_model
from tensorflow.keras.activations import softmax
import json
from tensorflow.python.keras.utils import multi_gpu_model
import numpy as np
import pickle
import itertools
import requests

preprocessed_dir = '/home/ec2-user/bert-question-answering/resources/preprocessed_data'

def top_n_answers(start_vec,end_vec,offset,max_idx,max_answer_len=30,from_n=10,n=5):
    possibles = [(0,0)]+[(s,e) for s,e in itertools.product(start_vec.argsort()[-from_n:],end_vec.argsort()[-from_n:])
                         if (e-s < max_answer_len) and (s<e) and (s>offset)
                         and (e>offset)
                         and (e<=max_idx)]
    possibles = np.array(possibles)
    likelihoods = np.array([start_vec[s]+end_vec[e] for s,e in possibles])
    best_options = np.argsort(likelihoods)[-1:-n-1:-1]
    return possibles[best_options],likelihoods[best_options]



def softmx(x):
    return softmax(x,axis=1)


custom_objects = get_custom_objects() #custom objects inside bertfam
custom_objects['softmx'] = softmx

with open("/home/ec2-user/bert-question-answering/resources/processed_model/amfam/amfam_keras_model/model_amfam_keras_squad_google.json", "r") as f:
    m_json = f.read()

model = model_from_json(m_json,custom_objects=custom_objects)
# parallel_squad_model = multi_gpu_model(model, 4)  # p3.8xl
#
# model = multi_gpu_model(model, 2)
# model.load_weights('/home/ec2-user/bert-question-answering/resources/processed_model/amfam/bertfam_keras_model/weights.best.hdf5', by_name=True)
model.load_weights('/home/ec2-user/bert-question-answering/resources/processed_model/amfam/amfam_keras_model/model_amfam_keras_squad_google.hdf5')



# GOOGLE_QA_FINE_TUNED_MODEL = "/home/ec2-user/bert-question-answering/resources/processed_model/amfam/bertfam_keras_model/fine_tuned_model.hdf5"
# model = load_model(GOOGLE_QA_FINE_TUNED_MODEL, custom_objects=custom_objects)
print(model.summary())


def normalize_answer(s):
  """Lower text and remove punctuation, articles and extra whitespace."""
  def remove_articles(text):
    regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
    return re.sub(regex, ' ', text)
  def white_space_fix(text):
    return ' '.join(text.split())
  def remove_punc(text):
    exclude = set(string.punctuation)
    return ''.join(ch for ch in text if ch not in exclude)
  def lower(text):
    return text.lower()
  return white_space_fix(remove_articles(remove_punc(lower(s))))

def get_tokens(s):
  if not s: return []
  return normalize_answer(s).split()

def compute_exact(gold_answer, a_pred):
    try:
        gold_eval = []
        for pred in a_pred:
            gold_eval.append(int(normalize_answer(gold_answer) == normalize_answer(pred)))
        return max(gold_eval)
    except:
        print('Error')

def compute_f1(gold_answer, a_pred):
    try:
        f1_eval = []
        for pred in a_pred:
                  gold_toks = get_tokens(gold_answer)
                  pred_toks = get_tokens(pred)
                  common = collections.Counter(gold_toks) & collections.Counter(pred_toks)
                  num_same = sum(common.values())
                  if len(gold_toks) == 0 or len(pred_toks) == 0:
                    # If either is no-answer, then F1 is 1 if they agree, 0 otherwise
                    f1_eval.append(int(gold_toks == pred_toks))
                  if num_same == 0:
                    f1_eval.append(0)
                    continue
                  precision = 1.0 * num_same / len(pred_toks)
                  recall = 1.0 * num_same / len(gold_toks)
                  f1 = (2 * precision * recall) / (precision + recall)
                  f1_eval.append(f1)

        return max(f1_eval)
    except:
        print('Error')



input_data = '/home/ec2-user/bert-question-answering/resources/source_data/squad/dev-v2.0.json'
with open(input_data,'r') as f:
    d = json.load(f)
all_question_context_answer = parse_squad(d)


# seen = set()
# unique_questions = []
# for object in all_question_context_answer:
#     if object.question not in seen and object.is_impossible == False:
#         seen.add(object.question)
#         unique_questions.append((object.question, object.context))


predictions = []
exact_scores = {}
f1_scores = {}
question_id = 0

for question_context in all_question_context_answer:
    gold_answer = question_context.answer_str
    a_pred = []
    question = question_context.question
    context = question_context.context
    qa_id = question_context.id
    qc = QuestionContext(context, question)
    ids, segment_ids = [np.array(_) for _ in qc.pack()]
    mask = ids != 0
    predictions = model.predict([[ids],[mask],[segment_ids]])
    mx = max(qc.context_token_map.keys())
    predictions = [p.ravel() for p in predictions]
    choices = top_n_answers(predictions[0], predictions[1], qc.context_offset, mx)
    res = {}
    for i, ((start_idx, end_idx), likelihood) in enumerate(zip(*choices)):
        start_word_number = qc.get_word_number_from_token_idx(start_idx)
        end_word_number = qc.get_word_number_from_token_idx(end_idx)
        if (start_idx != 0) and (end_idx != 0):
            if start_word_number == end_word_number:
                end_word_number += 1
            answer_str = qc.get_word_range(start_word_number, end_word_number)
            # with open('good.pkl','wb') as f:
            #     pickle.dump(predictions,f)
        else:
            answer_str = ''
            # with open('bad.pkl','wb') as f:
            #     pickle.dump(predictions,f)
        result = {'start_idx': int(start_idx), 'start_number': start_word_number,
                  'end_idx': int(end_idx), 'end_number': end_word_number,
                  'answer_str': answer_str, 'likelihood': float(likelihood),
                  }
        res[str(i)] = result


    for i in res:
        if res[i]['answer_str']:
            a_pred.append(res[i]['answer_str'])

    # for object in all_question_context_answer:
    #     if object.question == question and object.answer_str:
    #         gold_answers.append(object.answer_str)

    #     print('Instance : ')
    #     print('gold answers: ', gold_answers)
    #     print('predicted_answers: ', a_pred)


    if a_pred and gold_answer:
        exact_scores[question_context.id] = compute_exact(gold_answer, a_pred)
        f1_scores[question_context.id] = compute_f1(gold_answer, a_pred)

    question_id+=1
    print('processed examples: ', question_id)


print('done')
total = len(exact_scores)
total_f1 = len(f1_scores)
eval_dict = collections.OrderedDict([
    ('exact', 100.0 * sum(exact_scores.values()) / total),
    ('f1', 100.0 * sum(f1_scores.values()) / total),
    ('total exact', total),
    ('total f1', total_f1)])
print(eval_dict)