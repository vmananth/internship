#### Set Project path
PROJ_DIR=/home/ec2-user/bert-question-answering
#### Set BERT Base Dir
BERT_FAM_DIR=/home/ec2-user/bertfam-base-uncased-seq.hdf5
#### Set Resource Dir
RESOURCE_DIR=$PROJ_DIR/resources
#### Use CPU, each with 8 seqlen-512 samples

export PYTHONPATH=$PROJ_DIR/src

cd $PROJ_DIR/src/amfam_implementation/evaluate

python evaluate_keras_amfam.py \
  $@
