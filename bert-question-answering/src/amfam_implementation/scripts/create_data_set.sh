#!/bin/bash

#### Set Project path
PROJ_DIR=/home/ec2-user/bert-question-answering
#### Set BERT Base Dir
BERT_DIR=/home/ec2-user/uncased_L-12_H-768_A-12

#### Use CPU, each with 8 seqlen-512 samples

python $PROJ_DIR/src/amfam_implementation/pre_process/create_data.py \
  --input_file=$PROJ_DIR/resources/source_data/squad/train-v2.0.json \
  --output_dir=$PROJ_DIR/resources/preprocessed_data/amfam/ \
  --do_preprocess=True
  $@