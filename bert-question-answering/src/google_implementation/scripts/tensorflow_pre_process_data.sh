#!/bin/bash

#### Set Project path
PROJ_DIR=/home/ec2-user/bert-question-answering
#### Set BERT Base Dir
BERT_DIR=/home/ec2-user/uncased_L-12_H-768_A-12

#### Use CPU, each with 8 seqlen-512 samples


export PYTHONPATH=$PROJ_DIR/src

cd $PROJ_DIR/src/google_implementation/pre_process
python create_dataset.py \
  --vocab_file=$BERT_DIR/vocab.txt \
  --bert_config_file=$BERT_DIR/bert_config.json \
  --input_file=$PROJ_DIR/resources/source_data/squad/train-v2.0.json \
  --output_dir=$PROJ_DIR/resources/preprocessed_data \
  --do_pre_process=True
  $@