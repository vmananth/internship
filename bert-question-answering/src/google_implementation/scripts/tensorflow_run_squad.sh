#!/bin/bash

#### Set Project path
PROJ_DIR=/home/ec2-user/bert-question-answering
#### Set BERT Base Dir
BERT_DIR=/home/ec2-user/uncased_L-12_H-768_A-12
#### Squad Dir
SQUAD_DIR=/home/ec2-user/bert-question-answering/resources/source_data/squad
#### Output Dir
OUTPUT_DIR=/home/ec2-user/bert-question-answering/resources/processed_model

#### Use CPU, each with 8 seqlen-512 samples

export PYTHONPATH=$PROJ_DIR/src

cd $PROJ_DIR/src/google_implementation/train
python run_squad.py \
  --vocab_file=$BERT_DIR/vocab.txt \
  --bert_config_file=$BERT_DIR/bert_config.json \
  --init_checkpoint=$BERT_DIR/bert_model.ckpt \
  --do_train=True \
  --train_file=$SQUAD_DIR/train-v2.0.json \
  --do_predict=True \
  --predict_file=$SQUAD_DIR/dev-v2.0.json \
  --train_batch_size=10 \
  --learning_rate=3e-5 \
  --num_train_epochs=2.0 \
  --max_seq_length=384 \
  --doc_stride=128 \
  --output_dir=$OUTPUT_DIR \
  --use_tpu=False \
  --tpu_name=$TPU_NAME \
  --version_2_with_negative=True \
  $@