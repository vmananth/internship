#!/bin/bash

#### Set Project path
PROJ_DIR=/home/ec2-user/bert-question-answering
#### Set BERT Base Dir
BERT_DIR=/home/ec2-user/uncased_L-12_H-768_A-12
#### Squad Dir
SQUAD_DIR=/home/ec2-user/bert-question-answering/resources/source_data/squad
#### Output Dir
OUTPUT_DIR=/home/ec2-user/bert-question-answering/resources/processed_model

#### Use CPU, each with 8 seqlen-512 samples

export PYTHONPATH=$PROJ_DIR/src

cd $PROJ_DIR/src/google_implementation/evaluate
python evaluate-v2.0.py $SQUAD_DIR/dev-v2.0.json $OUTPUT_DIR/predictions.json \
  --na-prob-file $OUTPUT_DIR/null_odds.json
  $@