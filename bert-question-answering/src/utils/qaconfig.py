VOCAB_FILE = '/home/ec2-user/uncased_L-12_H-768_A-12/vocab.txt'

# TOKENIZER = tokenization.FullTokenizer(VOCAB_FILE,do_lower_case=True)

MAX_TOKENS = 512

TOKEN_UNK = '[UNK]'  # Token for unknown words
TOKEN_CLS = '[CLS]'  # Token for classification
TOKEN_SEP = '[SEP]'  # Token for separation
TOKEN_PAD = ''  # Token for padding