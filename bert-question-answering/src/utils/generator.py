import numpy as np

def batch_generator(ids_train, segment_train, start_index_train, end_index_train, batch_size = 4):
    indices = np.arange(len(ids_train))
    batch=[]
    while True:
            # it might be a good idea to shuffle your data before each epoch
            np.random.shuffle(indices)
            for i in indices:
                batch.append(i)
                if len(batch)==batch_size:
                    yield [ids_train[batch], segment_train[batch]], [start_index_train[batch], end_index_train[batch]]
                    # batch=[]