# Bert-Question-Answering

## Following are the steps to fine tune a pretrained bert model for a down stream task such as Question Answering



### Structure of the Repo
   - bert-question-answering
     - resources
       - preprocessed_data
       - processed_data
       - source_data
     - src
       - bert
       - amfam_implementation
         - pre_process
         - train
         - evaluate
         - scripts
       - google_implementation
         - pre_process
         - train
         - evaluate
         - scripts
         
 
###

- The amfam_implementation is the fine tuning of bert question answering using Keras
- The google_implementation is the fine tuning of bert question answering using Tensorflow
- Each Package consists of the necessary scripts to run the fine tuning with ease.  


### 

Steps
   -  Choose a particular implementation and navigate to the corresponding directory in src folder.
   -  Run the scripts in order
      - preprocess_data
      - train data
      - evaluate on trained model for scores.
      
      
### 
-  source_data directory is where the squad data files reside.
-  preprocessed_data directory is where the tf record or intermediate files reside after running the pre-process script.
-  preprocessed_model directory is where the checkpoints and logs are stored during and after fine tuning.

### Results
![Benchmark](resources/results/Benchmark.png)
