IP=10.160.42.133
ssh -i ~/Documents/key/ieh106-dxd036.pem ubuntu@$IP


sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
mkdir deploy


scp -r -i ~/Documents/key/ieh106-dxd036.pem ~/projects/stanford-qa/deploy/solr ubuntu@$IP:deploy
scp -r -i ~/Documents/key/ieh106-dxd036.pem ~/projects/stanford-qa/deploy/flask-app ubuntu@$IP:deploy
scp -r -i ~/Documents/key/ieh106-dxd036.pem ~/projects/stanford-qa/deploy/nginx-flask ubuntu@$IP:deploy

cd deploy/flask-app
# cp ./Dockerfile /var/lib/snapd/void
sudo docker build -t flask -f ./Dockerfile .

sudo docker network create --driver bridge my_bridge

sudo docker run --name flask-app \
    --link solr:solr \
    -v /home/ubuntu/deploy:/deploy \
    -d --network my_bridge flask


cd ~
sudo docker run --name nginx-app --network my_bridge \
      -v /home/ubuntu/deploy/nginx-flask:/etc/nginx:ro \
      --link flask-app:flask-app \
      --link solr:solr \
      -p 443:443 -p 80:80 -d nginx

sudo docker run --name solr -d -t solr

sudo docker cp $HOME/deploy/solr/processed/ solr:/opt/solr/

sudo docker exec -it --user=solr solr bin/solr create_core -c prm -d sample_techproducts_configs
sudo docker exec -it --user=solr solr bin/post -c prm /opt/solr
sudo docker network connect my_bridge solr

sudo docker cp $HOME/deploy/flask-app/templates/ flask-app:/templates
sudo docker cp $HOME/deploy/flask-app/static/ flask-app:/static

# sudo docker exec -it --user=solr solr bin/solr delete -c prm
# sudo docker exec -it --user=solr solr bin/solr create_core -c gettingstarted
# sudo docker exec -it --user=solr solr bin/post -c gettingstarted example/exampledocs/manufacturers.xml
