import flask
import json
import pickle
import itertools
import requests
# import grequests
#from requests import async
# from multiprocessing import Pool

SQUAD_URL = 'https://10.160.42.98'
SOLR_URL = 'http://solr:8983/solr/prm/select?fl=*%2Cscore&q='
CERT = False#'nginx.crt'
n_solr_docs = 5
# _pool = None
app = flask.Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
PRM_HTML = '/deploy/solr/html/'


@app.route('/',methods=['GET','POST'])
# @app.route('/<path:path>',methods=['GET'])
def index(path=None):
    if path is None:
        if flask.request.method == 'GET':
            return flask.render_template('qa_page.html')
        if flask.request.method == 'POST':
            try:
                data = flask.request.get_json()
                res = requests.post(SQUAD_URL,json=data,verify=CERT)
                return flask.jsonify(res.json())
            except Exception as e:
                print(str(e),e.args)
            return flask.jsonify({'msg':'got it'})
    r = requests.get('https://prm.amfam.com/'+path,verify=False)
    return r.content

# def fetch(question,context,url=SQUAD_URL):
#     print(question)
#     print(context)
#     import requests
#     return requests.post(SQUAD_URL,json={'context':context,'question':question}).json()
#

# @app.route('/query',methods=['GET','POST'])
# def query():
#     if flask.request.method == 'GET':
#         return flask.render_template('query_page.html')
#     if flask.request.method == 'POST':
#         try:
#             data = flask.request.get_json()
#             print(data)
#             r = requests.get(SOLR_URL+data['question'])
#             res = {j:{} for j in range(n_solr_docs)}
#             for doc_i,doc in enumerate(r.json()['response']['docs'][:n_solr_docs]):
#                 print(doc)
#                 res[doc_i]['doc_id'] = doc['id'].split('/')[-1]
#                 context = doc['content'][0].strip()
#                 res[doc_i]['context'] = context
#
#             questions = [data['question']]*n_solr_docs
#             contexts = [res[i]['context'] for i in range(n_solr_docs)]
#             print(questions)
#             print(contexts)
#             responses = _pool.starmap(fetch,zip(questions,contexts))
#             print(responses)
#             print('#'*80)
#             for i,v in enumerate(responses):
#                 res[i].update(v)
#             return flask.jsonify(res)
#         except Exception as e:
#             print(str(e),e.args)
#             for k,v in res.items():
#                 print(k)
#                 for k1,v1 in v.items():
#                     print('\t',k1)
#                     print('\t',v1)
#                 print()
#         return flask.jsonify({'msg':'got it'})


@app.route('/query',methods=['GET','POST'])
def query():
    if flask.request.method == 'GET':
        return flask.render_template('query_page_aws.html')
    if flask.request.method == 'POST':
        try:
            data = flask.request.get_json()
            print(data)
            r = requests.get(SOLR_URL+data['question']+'&fq=id:*'+data['fq']+'*')
            res = {j:{} for j in range(n_solr_docs)}
            res[-1] = {'overall':0,'docid':'','answer_str':''}
            reqs = []
            for doc_i,doc in enumerate(r.json()['response']['docs'][:n_solr_docs]):
                print(doc)
                nm = doc['id'].split('/')[-1]
                res[doc_i]['doc_id'] = nm
                res[doc_i]['score'] = doc['score']
                context = doc['content'][0].strip()
                res[doc_i]['context'] = context
                try:
                    with open(PRM_HTML+nm.replace('.txt','.html'),'r') as f:
                        html = f.read()
                    res[doc_i]['html'] = html
                except:
                    print('didnt find ',PRM_HTML+nm.replace('.txt','.html'))
                    res[doc_i]['html'] = context
                # print(context)
                result = requests.post(SQUAD_URL,json={'context':context,'question':data['question']},verify=CERT).json()
                for k,v in result.items():
                    print(v)
                    res[doc_i][k] = v
                    if ((res[doc_i]['score']*v['likelihood'] > res[-1]['overall']) and
                            (v['answer_str'] != '')):
                        res[-1] = {'overall':res[doc_i]['score']*v['likelihood'],
                        'docid':doc_i,'answer_str':v['answer_str'],
                        'answer_id':k}
            return flask.jsonify(res)
        except Exception as e:
            print(str(e),e.args)
            for k,v in res.items():
                print(k)
                for k1,v1 in v.items():
                    print('\t',k1)
                    print('\t',v1)
                print()
        return flask.jsonify({'msg':'got it'})


# @app.route('/',methods=['GET','POST'])
# def index():
#     if flask.request.method == 'GET':
#         return flask.render_template('qa_page.html')
#     if flask.request.method == 'POST':
#         try:
#             data = flask.request.get_json()
#             qc = QuestionContext(data['context'],data['question'])
#             ids,segment_ids = [np.array(_) for _ in qc.pack()]
#             predictions = squad_model.predict([[ids],[segment_ids]])
#             mx = max(qc.context_token_map.keys())
#             predictions = [p.ravel() for p in predictions]
#             start_idx,end_idx = [int(p[:mx].argmax()) for p in predictions]
#             print(qc.context_token_map)
#             print(start_idx,end_idx)
#             start_word_number = qc.get_word_number_from_token_idx(start_idx)
#             end_word_number = qc.get_word_number_from_token_idx(end_idx)
#             print(end_word_number,start_word_number)
#             if (start_idx != 0) and (end_idx != 0):
#                 answer_str = qc.get_word_range(start_word_number,end_word_number)
#                 # with open('good.pkl','wb') as f:
#                 #     pickle.dump(predictions,f)
#             else:
#                 answer_str = ''
#                 # with open('bad.pkl','wb') as f:
#                 #     pickle.dump(predictions,f)
#             res = {'start_idx':start_idx,'start_number':start_word_number,
#                     'end_idx':end_idx,'end_number':end_word_number,
#                     'answer_str':answer_str}
#
#             return flask.jsonify(res)
#         except Exception as e:
#             print(str(e),e.args)
#         return flask.jsonify({'msg':'got it'})


if __name__ == '__main__':
    # _pool = Pool(4)
    app.run(debug=True)



#
