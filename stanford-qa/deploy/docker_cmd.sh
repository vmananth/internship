cd model
docker build -t squad:1 .
docker run --name squad1 -d -p 8000:5000 squad:1
docker save -o /Users/dxd036/Downloads/squad1.tar squad:1
gzip -k /Users/dxd036/Downloads/squad1.tar
# docker load -i <path to image tar file>

# r = requests.post('http://localhost:8000',json={'question':'how are you?','contex
#    ...: t':"it is very sunny today"})


IP='10.160.42.98'
ssh -i ~/Documents/key/ieh106-dxd036.pem ubuntu@$IP
lsblk #supresses /dev/
# NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
# xvda    202:0    0   75G  0 disk
# └─xvda1 202:1    0   75G  0 part /
# xvdb    202:16   0  256G  0 disk
# loop0     7:0    0 91.1M  1 loop /snap/core/6531
# loop1     7:1    0 87.9M  1 loop /snap/core/5742
# loop2     7:2    0 17.9M  1 loop /snap/amazon-ssm-agent/1068
# loop3     7:3    0 16.5M  1 loop /snap/amazon-ssm-agent/784
# loop4     7:4    0 89.3M  1 loop /snap/core/6673
sudo file -s /dev/xvdb
#/dev/xvdb: data
#since it doesn't already have a file system, create one:
# sudo mkfs -t xfs /dev/xvdb
sudo mkdir /squad
sudo chown `whoami` /squad
sudo mount /dev/xvdb /squad
mkdir /squad/nginx

sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
# sudo service docker stop
# sudo service docker start

# snap remove docker
# then remove the docker directory, and old version
#
# rm -R /var/lib/docker
#
# sudo apt-get remove docker docker-engine docker.io

# scp -r -i ~/Documents/key/ieh106-dxd036.pem ~/projects/stanford-qa/deploy ubuntu@$IP:/squad
cd /squad/deploy/model
# cp ./Dockerfile /var/lib/snapd/void
sudo docker build -t squad:1 -f ./Dockerfile .


# /etc/ssl/nginx.crt;
# ssl_certificate_key /etc/ssl/nginx.key
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /squad/deploy/nginx/nginx.key -out /squad/deploy/nginx/nginx.crt
# scp -i ~/Documents/key/ieh106-dxd036.pem ubuntu@$IP:/squad/deploy/nginx/nginx.crt ./


# gunzip squad1.tar.gz
sudo docker network create --driver bridge my_bridge
#sudo docker load -i squad1.tar
sudo docker run --name model1 -d --network my_bridge squad:1
sudo docker run --name model2 -d --network my_bridge squad:1
sudo docker run --name model3 -d --network my_bridge squad:1
sudo docker run --name model4 -d --network my_bridge squad:1


sudo docker run --name nginx-app --network my_bridge \
      -v /squad/deploy/nginx:/etc/nginx:ro \
      -v /squad/deploy/nginx:/etc/ssl/certs \
      --link model1:model1 \
      --link model2:model2 \
      --link model3:model3 \
      --link model4:model4 \
      -p 443:443 -p 80:80 -d nginx

docker save -o /squad/deploy/model/squad1.tar squad:1
gzip -k /squad/squad1.tar

sudo docker exec -it <container-name> sh
in container: kill 1
