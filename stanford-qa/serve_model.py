import flask
from qaclasses36 import QuestionContext
from keras.models import Model, model_from_json
import keras_bert
from keras.activations import softmax
import json
import numpy as np
import pickle
import itertools
import requests

version = 'v2'
bert_layer_num = '11'#sys.argv[1]
bln = (int(bert_layer_num) - 12)*4 - 1
SOLR_URL = 'http://localhost:8983/solr/prm/select?q='
n_solr_docs = 5

with open("%s/%s_squad_model-bl_%s.json"%(version,version,bert_layer_num), "r") as f:
    m_json = f.read()

def softmx(x):
    return softmax(x,axis=1)


def top_n_answers(start_vec,end_vec,offset,max_idx,max_answer_len=30,from_n=10,n=5):
    possibles = [(0,0)]+[(s,e) for s,e in itertools.product(start_vec.argsort()[-from_n:],end_vec.argsort()[-from_n:])
                         if (e-s < max_answer_len) and (s<e) and (s>offset)
                         and (e>offset)
                         and (e<=max_idx)]
    possibles = np.array(possibles)
    likelihoods = np.array([start_vec[s]+end_vec[e] for s,e in possibles])
    best_options = np.argsort(likelihoods)[-1:-n-1:-1]
    return possibles[best_options],likelihoods[best_options]

custom_objects = keras_bert.get_custom_objects()
custom_objects['softmx'] = softmx

squad_model = model_from_json(m_json,custom_objects=custom_objects)
squad_model.load_weights('%s/%s_weights-bl_%s-02.hdf5'%(version,version,bert_layer_num))
y = QuestionContext('I feel great!','hi how are you?')
ids,segment_ids = [np.array(_) for _ in y.pack()]
x = squad_model.predict([[ids],[segment_ids]])
print('#'*80)
print(x[0].argmax())
print(x[1].argmax())
print(y.context_token_map)


app = flask.Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True


@app.route('/',methods=['GET','POST'])
def index():
    if flask.request.method == 'GET':
        return flask.render_template('qa_page.html')
    if flask.request.method == 'POST':
        try:
            data = flask.request.get_json()
            qc = QuestionContext(data['context'],data['question'])
            ids,segment_ids = [np.array(_) for _ in qc.pack()]
            predictions = squad_model.predict([[ids],[segment_ids]])
            mx = max(qc.context_token_map.keys())
            predictions = [p.ravel() for p in predictions]
            choices = top_n_answers(predictions[0],predictions[1],qc.context_offset,mx)
            res = {}
            for i,((start_idx,end_idx),likelihood) in enumerate(zip(*choices)):
                start_word_number = qc.get_word_number_from_token_idx(start_idx)
                end_word_number = qc.get_word_number_from_token_idx(end_idx)
                if (start_idx != 0) and (end_idx != 0):
                    if start_word_number == end_word_number:
                        end_word_number += 1
                    answer_str = qc.get_word_range(start_word_number,end_word_number)
                    # with open('good.pkl','wb') as f:
                    #     pickle.dump(predictions,f)
                else:
                    answer_str = ''
                    print(start_idx,end_idx)
                    # with open('bad.pkl','wb') as f:
                    #     pickle.dump(predictions,f)
                res[int(i)] = {'start_idx':int(start_idx),'start_number':start_word_number,
                    'end_idx':int(end_idx),'end_number':end_word_number,
                    'answer_str':answer_str,'likelihood':float(likelihood)}

            return flask.jsonify(res)
        except Exception as e:
            print(str(e),e.args)
        return flask.jsonify({'msg':'got it'})

@app.route('/query',methods=['GET','POST'])
def query():
    if flask.request.method == 'GET':
        return flask.render_template('query_page.html')
    if flask.request.method == 'POST':
        try:
            data = flask.request.get_json()
            # print(data)
            r = requests.get(SOLR_URL+data['question'])
            res = {j:{} for j in range(n_solr_docs)}
            for doc_i,doc in enumerate(r.json()['response']['docs'][:n_solr_docs]):
                # print(doc)
                res[doc_i]['doc_id'] = doc['id'].split('/')[-1]
                context = doc['content'][0].strip()
                res[doc_i]['context'] = context
                # print(context)
                qc = QuestionContext(context,data['question'])
                ids,segment_ids = [np.array(_) for _ in qc.pack()]
                predictions = squad_model.predict([[ids],[segment_ids]])
                mx = max(qc.context_token_map.keys())
                predictions = [p.ravel() for p in predictions]
                choices = top_n_answers(predictions[0],predictions[1],qc.context_offset,mx)
                for i,((start_idx,end_idx),likelihood) in enumerate(zip(*choices)):
                    start_word_number = qc.get_word_number_from_token_idx(start_idx)
                    end_word_number = qc.get_word_number_from_token_idx(end_idx)
                    if (start_idx != 0) and (end_idx != 0):
                        if start_word_number == end_word_number:
                            end_word_number += 1
                        answer_str = qc.get_word_range(start_word_number,end_word_number)
                        # with open('good.pkl','wb') as f:
                        #     pickle.dump(predictions,f)
                    else:
                        answer_str = ''
                        print(start_idx,end_idx)
                        # with open('bad.pkl','wb') as f:
                        #     pickle.dump(predictions,f)
                    result = {'start_idx':int(start_idx),'start_number':start_word_number,
                        'end_idx':int(end_idx),'end_number':end_word_number,
                        'answer_str':answer_str,'likelihood':float(likelihood),
                        }
                    res[doc_i][str(i)] = result
            return flask.jsonify(res)
        except Exception as e:
            print(str(e),e.args)
            print(i,start_idx,end_idx,likelihood)
            print(start_word_number)
            print(end_word_number)
            print(qc.context_token_map)
            for k,v in res.items():
                print(k)
                for k1,v1 in v.items():
                    print('\t',k1)
                    print('\t',v1)
                print()
        return flask.jsonify({'msg':'got it'})


# @app.route('/',methods=['GET','POST'])
# def index():
#     if flask.request.method == 'GET':
#         return flask.render_template('qa_page.html')
#     if flask.request.method == 'POST':
#         try:
#             data = flask.request.get_json()
#             qc = QuestionContext(data['context'],data['question'])
#             ids,segment_ids = [np.array(_) for _ in qc.pack()]
#             predictions = squad_model.predict([[ids],[segment_ids]])
#             mx = max(qc.context_token_map.keys())
#             predictions = [p.ravel() for p in predictions]
#             start_idx,end_idx = [int(p[:mx].argmax()) for p in predictions]
#             print(qc.context_token_map)
#             print(start_idx,end_idx)
#             start_word_number = qc.get_word_number_from_token_idx(start_idx)
#             end_word_number = qc.get_word_number_from_token_idx(end_idx)
#             print(end_word_number,start_word_number)
#             if (start_idx != 0) and (end_idx != 0):
#                 answer_str = qc.get_word_range(start_word_number,end_word_number)
#                 # with open('good.pkl','wb') as f:
#                 #     pickle.dump(predictions,f)
#             else:
#                 answer_str = ''
#                 # with open('bad.pkl','wb') as f:
#                 #     pickle.dump(predictions,f)
#             res = {'start_idx':start_idx,'start_number':start_word_number,
#                     'end_idx':end_idx,'end_number':end_word_number,
#                     'answer_str':answer_str}
#
#             return flask.jsonify(res)
#         except Exception as e:
#             print(str(e),e.args)
#         return flask.jsonify({'msg':'got it'})


if __name__ == '__main__':
    app.run(debug=True)



#
