import qaclasses36 as qaclasses
from keras.models import Model, model_from_json
import tensorflow as tf
import json
import numpy as np
# import sys
import keras
from keras_bert import load_trained_model_from_checkpoint, Tokenizer
from keras.layers import Dense
from keras.activations import softmax
from keras.callbacks import ModelCheckpoint

import logging

bert_layer_num = '11'
# bert_layer_num = sys.argv[1]
bln = (int(bert_layer_num) - 12)*4 - 1

with open("squad_model-bl_%s.json"%bert_layer_num, "r") as f:
    m_json = f.read()


base_path = 'bertmaster/uncased_L-12_H-768_A-12'

config_path = base_path + '/bert_config.json'
checkpoint_path = base_path + '/bert_model.ckpt'
dict_path = base_path + '/vocab.txt'
# input_data = 'train-v2.0.json'#'dev-v2.0.json'#'train-v2.0.json'#'dev-v2.0.json'#'test_sample_squad.json'#'dev-v2.0.json'
# intermediate_filepath = 'weights-bl_%s-{epoch:02d}.hdf5'%bert_layer_num
# intermediate_checkpoint = ModelCheckpoint(intermediate_filepath)
input_data = 'train-v2.0.json'





with open(input_data,'r') as f:
    d = json.load(f)


r = qaclasses.parse_squad(d)

ids,segment_ids,input_mask,start_idx,end_idx = [np.array(_) for _ in zip(*[x.encode() for x in r])]

np.save('preprocessed/train_ids.npy',ids)
np.save('preprocessed/train_segment_ids.npy',segment_ids)
np.save('preprocessed/train_input_mask.npy',input_mask)
np.save('preprocessed/train_start_idx.npy',start_idx)
np.save('preprocessed/train_end_idx.npy',end_idx)

model = load_trained_model_from_checkpoint(config_path, checkpoint_path)
encoding_out = model.layers[bln].output

bert11 = Model(model.inputs,encoding_out)
for i in range(int(ids.shape[0]/2),ids.shape[0]):
    if i%100 == 0:
        print(i)
    temp = bert11.predict([[ids[i]],[segment_ids[i]]])
    np.save('preprocessed/bert_output/bert11_output_idx_%i.npy'%i,temp)


# temp1 = np.load('preprocessed/bert_output/bert11_output_idx_%i.npy'%i)
# np.all(temp1==temp)
#
# for i in range(1000):
#     print(i)
#     temp1 = np.load('preprocessed/bert_output/bert11_output_idx_%i.npy'%i)


# import h5py
# with h5py.File('bert11_out10.h5','w') as f:
#     f.create_dataset('bert_layer11_output',data=temp)
