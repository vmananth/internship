from bertmaster import tokenization
import typing
import qaconfig

VOCAB_FILE = qaconfig.VOCAB_FILE
MAX_TOKENS = qaconfig.MAX_TOKENS
TOKEN_UNK = qaconfig.TOKEN_UNK
TOKEN_CLS = qaconfig.TOKEN_CLS
TOKEN_SEP = qaconfig.TOKEN_SEP
TOKEN_PAD = qaconfig.TOKEN_PAD

# VOCAB_FILE = 'bertmaster/uncased_L-12_H-768_A-12/vocab.txt'

TOKENIZER = tokenization.FullTokenizer(VOCAB_FILE,do_lower_case=True)

# MAX_TOKENS = 512
#
# TOKEN_UNK = '[UNK]'  # Token for unknown words
# TOKEN_CLS = '[CLS]'  # Token for classification
# TOKEN_SEP = '[SEP]'  # Token for separation
# TOKEN_PAD = ''  # Token for padding


class QuestionContext():
    def __init__(self,
            context: str,
            question: str):
        self.context = context
        self.question = question

    def get_word_number_from_token_idx(self,token_idx):
        if token_idx < self.context_offset:
            return '0'
        return self.context_token_map[token_idx][2]

    def get_word_from_token_idx(self,token_idx):
        if token_idx < self.context_offset:
            return '0'
        return self.context_token_map[token_idx][1]

    def get_word_range(self,start_num,end_num):
        return ' '.join([self.word_numbers[_] for _ in range(start_num,end_num)])

    def tokenize_question(self):
        return TOKENIZER.tokenize(self.question)

    def pack(self,max_tokens=MAX_TOKENS):
        qt = TOKENIZER.tokenize(self.question)
        self.context_offset = len(qt)+2
        current_idx = len(qt)+2
        self.context_token_map = {}
        self.word_numbers = {}
        ct = []
        for word_number,x in enumerate(self.context.split()):
            self.word_numbers[word_number] = x
            tokensx = TOKENIZER.tokenize(x)
            ct.extend(tokensx)
            for token in tokensx:
                self.context_token_map[current_idx] = (token,x,word_number)
                current_idx += 1
        tokens = ([TOKEN_CLS] + qt + [TOKEN_SEP] + ct)[:(max_tokens - 1)] + \
                [TOKEN_SEP]
                # + [TOKEN_PAD]*(MAX_TOKENS - len(qt) - len(ct) - 3)
        segment_ids = ([0]*(self.context_offset) + [1]*(len(ct)))[:(max_tokens - 1)] + \
                [1]
                # [0]*(MAX_TOKENS - len(qt) - len(ct) - 3)

        unk_id = TOKENIZER.vocab.get(TOKEN_UNK)
        ids = [TOKENIZER.vocab.get(token,unk_id) for token in tokens]
        l = (max_tokens - len(tokens))
        ids += ([0]*l)
        segment_ids += ([0]*l)
        # input_mask = [1]*len(tokens) + [0]*l
        return ids,segment_ids

class QandABase():
    def __init__(self,
            context: str,
            question: str,
            answer_str: str,
            answer_start: str,
            is_impossible: bool):
        self.context = context
        self.question = question
        self.answer_str = answer_str
        self.answer_start = answer_start
        self.is_impossible = is_impossible


    def tokenize_context(self):
        raise NotImplementedError('not implemented')

    def tokenize_question(self):
        raise NotImplementedError('not implemented')

    def answer_token_idx(self):
        raise NotImplementedError('not implemented')

    def extract(self):
        raise NotImplementedError('not implemented')

class Squad(QandABase):
    def __init__(self,context,question,answer_str,answer_start,is_impossible):
        super().__init__(context,question,answer_str,answer_start,is_impossible)

    def tokenize_context(self):
        return TOKENIZER.tokenize(self.context)

    def tokenize_question(self):
        return TOKENIZER.tokenize(self.question)

    def answer_token_idx(self):
        if self.is_impossible:
            return -1,-1
        start_idx = len(TOKENIZER.tokenize(self.context[:self.answer_start]))
        return start_idx,start_idx+len(TOKENIZER.tokenize(self.answer_str))

    def encode(self,max_tokens=MAX_TOKENS):
        tokens,segment_ids,q_len = self._pack(max_tokens)
        unk_id = TOKENIZER.vocab.get(TOKEN_UNK)
        ids = [TOKENIZER.vocab.get(token,unk_id) for token in tokens]
        l = (MAX_TOKENS - len(tokens))
        ids += ([0]*l)
        segment_ids += ([0]*l)
        input_mask = [1]*len(tokens) + [0]*l
        start_idx,end_idx = self.answer_token_idx()
        if start_idx >= 0:
            start_idx = (q_len + start_idx)
            if start_idx <= MAX_TOKENS:
                end_idx = min(q_len + end_idx,MAX_TOKENS)
            else:
                start_idx = 0
                end_idx = 0
        else:
            start_idx = 0
            end_idx = 0
        return ids,segment_ids,input_mask,start_idx,end_idx

    def _pack(self,max_tokens=MAX_TOKENS):
        qt = self.tokenize_question()
        ct = self.tokenize_context()
        tokens = ([TOKEN_CLS] + qt + [TOKEN_SEP] + ct)[:(max_tokens - 1)] + \
                [TOKEN_SEP]
                # + [TOKEN_PAD]*(MAX_TOKENS - len(qt) - len(ct) - 3)
        segment_ids = ([0]*(len(qt)+2) + [1]*(len(ct)))[:(max_tokens - 1)] + \
                [1]
                # [0]*(MAX_TOKENS - len(qt) - len(ct) - 3)
        return tokens,segment_ids,len(qt)+2


class QandA(Squad):
    def __init__(self,context,question,answer_str,answer_start,is_impossible):
        super().__init__(context,question,answer_str,answer_start,is_impossible)

    def answer_token_idx(self):
        if self.is_impossible:
            return -1,-1
        start_idx = self.answer_start
        return start_idx,start_idx+len(TOKENIZER.tokenize(self.answer_str))


def parse_squad(d):
    results = []
    for topic in d['data']:
        for paragraph in topic['paragraphs']:
            context = paragraph['context']
            for qa in paragraph['qas']:
                # print(qa)
                question = qa['question']
                # print(question)
                # print(qa['answers'])
                is_impossible = qa['is_impossible']
                id = qa['id']
                if not is_impossible:
                    for x in qa['answers']:
                        answer_str = x['text']
                        answer_start = x['answer_start']
                        a = Squad(context,question,answer_str,answer_start,is_impossible)
                        results.append(a)

                else:
                    answer_str,answer_start = None,None
                    a = Squad(context,question,answer_str,answer_start,is_impossible)
                    results.append(a)
    return results
