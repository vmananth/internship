

#aws-adfs-login
#source ~/.aws/sessions/amfam-ieh106-data-scientist-r.env


from pyathena import connect
import pandas as pd
import os
import sys

aws_bucket = 's3://sda-nlpresearch/glue_db'
aws_access_key_id = os.environ['AWS_ACCESS_KEY_ID']
aws_secretaccess_key = os.environ['AWS_SECRET_ACCESS_KEY']
aws_session_token = os.environ['AWS_SESSION_TOKEN']
qry = "select * from nlpresearch.claimnotes_asof20190328 where claimid=696275.0"

conn = connect(aws_access_key_id=aws_access_key_id,
               aws_secret_access_key=aws_secretaccess_key,
               s3_staging_dir=aws_bucket,
               aws_session_token=aws_session_token,
               region_name='us-east-1')

df = pd.read_sql(qry, conn)
print(df.head())
df.sort_values('noteupdatetime',inplace=True)
df.reset_index(drop=True,inplace=True)



select * from nlpresearch.claimnotes_asof20190328 where claimid=696275.0;
