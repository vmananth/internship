import json
import numpy as np
import tensorflow as tf
from dataclasses import dataclass
from bertmaster import tokenization
import typing

VOCAB_FILE = 'bertmaster/uncased_L-12_H-768_A-12/vocab.txt'

TOKENIZER = tokenization.FullTokenizer(VOCAB_FILE,do_lower_case=True)

@dataclass
class QandABase():
    context: str
    question: str
    answers_str: typing.List[str]
    answers_start: typing.List[int]
    is_impossible: bool

    def tokenize_c(self):
        raise NotImplementedError
    def tokenize_q(self):
        raise NotImplementedError


class Squad(QandABase):
    def __init__(self,context,question,answer_str,answer_start,is_impossible):
        super().__init__(context,question,answer_str,answer_start,is_impossible)
    def tokenize_c(self):
        return TOKENIZER.tokenize(self.context)
    def tokenize_q(self):
        return TOKENIZER.tokenize(self.question)

with open('dev-v2.0.json','r') as f:
    d = json.load(f)

data = d['data']

import copy
d1 = copy.deepcopy(d)
d1['data'] = d1['data'][:2]
for i in range(len(d1['data'])):
    d1['data'][i]['paragraphs'] = d1['data'][i]['paragraphs'][:2]

with open('test_sample_squad.json','w') as f:
    json.dump(d1,f)


for _ in data:
    print('#'*80)
    print(_.keys())
    print(_['title'],len(_['paragraphs']))






#
