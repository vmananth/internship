import unittest
from bertmaster import modeling
import qaclasses
import tensorflow as tf
from keras_bert import load_trained_model_from_checkpoint, Tokenizer

base_path = '/Users/dxd036/projects/stanford-qa/bertmaster/uncased_L-12_H-768_A-12'
config_path = base_path + '/bert_config.json'
checkpoint_path = base_path + '/bert_model.ckpt'
dict_path = base_path + '/vocab.txt'
model = load_trained_model_from_checkpoint(config_path, checkpoint_path)
model.summary(line_length=120)

model = load_trained_model_from_checkpoint(config_path, checkpoint_path,training=True)
model.summary(line_length=120)

class TestBertStuff(unittest.TestCase):
    def test_bert_model(self):
        config = modeling.BertConfig(len(qaclasses.TOKENIZER.vocab))
        model = modeling.BertModel(config,is_training=False,input_ids=input_ids)



if __name__ == '__main__':
    input_ids = tf.constant([[1], [2]])
    input_mask = tf.constant([[1], [1]]) #should be 1, except 0 if padded
    token_type_ids = tf.constant([[0],[0]]) #0 for sentence A, 1 for sentence B
    config = modeling.BertConfig(len(qaclasses.TOKENIZER.vocab))
    model = modeling.BertModel(config,is_training=False,input_ids=input_ids)
    batch_size,seq_length,hidden_size = modeling.get_shape_list(seq_out)
    # help(model)
    print('pooled')
    print(model.get_pooled_output())
    print('embedding')
    print(model.get_embedding_output())
    print('all')
    print(model.get_all_encoder_layers())
    print('embedding table')
    print(model.get_embedding_table())
    print('sequence')
    seq_out = model.get_sequence_output()
    print(seq_out)
    batch_size,seq_length,hidden_size = modeling.get_shape_list(seq_out)
    print('batch_size:',batch_size)
    print('seq_length:',seq_length)
    print('hidden_size:',hidden_size)



 #
 # |  label_embeddings = tf.get_variable(...)
 # |  pooled_output = model.get_pooled_output()
 # |  logits = tf.matmul(pooled_output, label_embeddings)
