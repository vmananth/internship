import numpy as np
from keras.models import Model
from keras.layers import Dense, Input, Multiply, Lambda, Activation, Flatten, Maximum, Dot
from keras import backend as K

def get_shape(tensor):
    return tensor.shape


def self_attention_layer(tensor,mask,heads,head_size):
    query_layer = Dense(heads*head_size,activation='linear')
    query_out = query_layer(tensor)#from tensor
    wts,bias = query_layer.get_weights()
    query_layer.set_weights([np.ones_like(wts),bias])
    key_layer = Dense(heads*head_size,activation='linear')
    key_out = key_layer(tensor)#to tensor
    wts,bias = key_layer.get_weights()
    key_layer.set_weights([-np.ones_like(wts),bias])
    value_layer = Dense(heads*head_size,activation='linear')
    value_out = value_layer(tensor)#to tensor
    wts,bias = value_layer.get_weights()
    value_layer.set_weights([2*np.ones_like(wts),bias])
    dot_layer = Dot(axes=2)
    # attention_layer = Lambda(lambda x: K.dot(x[0],K.permute_dimensions(x[1],(0,2,1))))
    dot_out = dot_layer([query_out,key_out])
    scaling_layer = Lambda(lambda x: x/np.sqrt(heads*head_size))
    scaling_out = scaling_layer(dot_out)
    softmax_layer = Activation('softmax')
    softmax_out = softmax_layer(scaling_out)
    print('s',softmax_out.shape)
    print('v',value_out.shape)
    # attention_layer = Lambda(lambda x: K.dot(x[0],x[1]))
    transpose_out = Lambda(lambda x: K.permute_dimensions(x,(0,2,1)))(softmax_out)
    attention_layer = Dot(axes=1)
    # attention_out = attention_layer([softmax_out,value_out])
    attention_out = attention_layer([transpose_out,value_out])
    return attention_out#softmax_out#



if __name__ == '__main__':
    max_length = 2
    embedding_dim = 4
    np.random.seed(3543)
    X = np.random.uniform(-1,1,(3,max_length,embedding_dim))

    inputs = Input(shape=(max_length,embedding_dim))
    attention = self_attention_layer(inputs,None,2,2)
    model = Model([inputs],[attention])
    model.compile(loss='mean_squared_error',optimizer='adam')
    results = model.predict(X[:1])
    # results = results.reshape((results.shape[0],results.shape[1],results.shape[3]))
    print(results.shape)
    x = X[0]
    wq,bq = model.layers[1].get_weights()
    wk,bk = model.layers[2].get_weights()
    wv,bv = model.layers[7].get_weights()
    xq = x.dot(wq) + bq
    xk = x.dot(wk) + bk
    xv = x.dot(wv) + bv
    y = xq.dot(xk.T)/np.sqrt(2*2)
    y = np.exp(y)
    y = y/(y.sum(axis=1)[:,np.newaxis])
    y1 = y.dot(xv)
    assert np.allclose(y1,results)




#
