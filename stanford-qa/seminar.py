import matplotlib.pyplot as plt
import numpy as np

np.random.seed(345)

sentence1 = 'bert is conceptually simple .'.split()
sentence2 = 'it beats all other models .'.split()
yticks = ['[CLS]'] + sentence1 + ['[SEP]'] + sentence2 + ['[SEP]'] + ['[PAD]']*4

mat = np.random.normal(size=(len(sentence1)+1,20))
mat2 = np.random.normal(size=(len(sentence2),20))
sep = np.random.normal(size=20)
mat = np.vstack([mat]+[sep]+[mat2]+[sep]+[np.random.normal(size=20)]*4)


with plt.xkcd():
    plt.figure(figsize=(10,6))
    plt.imshow(mat)
    plt.yticks(range(len(yticks)),yticks)
    plt.ylabel('INPUT = tokens')
    plt.title('OUTPUT = embedding')
    plt.xticks([])
    plt.savefig('seminar/images/bert_1slide.png')
    # plt.show()
    plt.close()



np.random.seed(543)
sentence1 = 'in what country is normandy located ?'.split()
sentence2 = 'a region in france . they were descended from'.split()
yticks = ['[CLS]'] + sentence1 + ['[SEP]'] +['...']*2+ sentence2 + ['...']*2+['[SEP]']

mat = np.random.normal(size=(len(sentence1)+1,20))
mat2 = np.random.normal(size=(len(sentence2),20))
sep = np.random.normal(size=20)
ellipsis = np.zeros_like(sep)
mat = np.vstack([mat]+[sep]+[ellipsis]*2+[mat2]+[ellipsis]*2+[sep])


with plt.xkcd():
    plt.figure(figsize=(10,6))
    plt.imshow(mat)
    plt.yticks(range(len(yticks)),yticks)
    plt.ylabel('INPUT = tokens')
    plt.title('OUTPUT = embedding')
    plt.xticks([])
    plt.savefig('seminar/images/bert_squad.png')
    # plt.show()
    plt.close()


np.random.seed(543)
sentence1 = 'in what country is normandy located ?'.split()
sentence2 = 'a region in france . they were descended from'.split()
yticks = ['[CLS]'] + sentence1 + ['[SEP]'] +['...']*2+ sentence2 + ['...']*2+['[SEP]']

mat = np.random.normal(size=(len(sentence1)+1,20))
mat2 = np.random.normal(size=(len(sentence2),20))
sep = np.random.normal(size=20)
ellipsis = np.zeros_like(sep)
mat = np.vstack([mat]+[sep]+[ellipsis]*2+[mat2]+[ellipsis]*2+[sep])


with plt.xkcd():
    plt.figure(figsize=(1,3))
    plt.imshow(mat[yticks.index('france')][:,np.newaxis])
    plt.title('v_s')
    plt.yticks([])
    plt.xticks([])
    plt.savefig('seminar/images/v_s.png')
    # plt.show()
    plt.close()


with plt.xkcd():
    plt.figure(figsize=(1,6))
    plt.imshow(mat.dot(mat[yticks.index('france')][:,np.newaxis]))
    plt.title('pred')
    plt.yticks([])
    plt.xticks([])
    plt.savefig('seminar/images/v_s_dot.png')
    # plt.show()
    plt.close()


with plt.xkcd():
    plt.figure(figsize=(1,6))
    plt.imshow(mat.dot(mat[yticks.index('france')+1][:,np.newaxis]))
    plt.title('pred')
    plt.yticks([])
    plt.xticks([])
    plt.savefig('seminar/images/v_e_dot.png')
    # plt.show()
    plt.close()


actual = np.zeros((len(yticks),1))
actual[yticks.index('france')] = 1
with plt.xkcd():
    plt.figure(figsize=(1,6))
    plt.imshow(actual)
    plt.title('target')
    plt.yticks([])
    plt.xticks([])
    plt.savefig('seminar/images/target.png')
    # plt.show()
    plt.close()

    # plt.text(0.25,0.75,'Analytics(Data)->Strategy',fontsize=30)
    # plt.text(0.25,0.5,'[Analytics]*[Data]=[Strategy]',fontsize=30)
    # plt.text(0.25,0.25,'Analytics(Data)=Strategy',fontsize=30)
    # plt.show()



#
