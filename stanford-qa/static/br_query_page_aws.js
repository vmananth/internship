
// (?:\s*<[^>]+>\s*)*\s+(?:\s*<[^>]+>\s*)* replace spaces
  var answer;
  var selectedDocId = 0;
  const postUrl = '/br_query';



  function submit() {
    document.getElementById("submitButton").innerHTML = "Submit <i class='fa fa-spinner w3-spin' style='font-size:18px'></i>";
    document.getElementById("docText").innerText = "";
    for (i=0;i<5;i++) {
      document.getElementById("answer"+i).innerText = "";
      document.getElementById("doc"+i).innerText = "";

      hideFn("answer"+i);
      hideFn("doc"+i);}

    document.getElementById("best").innerText = "";

    hideFn("best1");
    Data = {
      "question":document.getElementById("question").value,
      "fq":document.querySelector('input[name="line"]:checked').value};
    console.log(Data);
    params = {
      "headers":{"content-type":"application/json"},
      "body":JSON.stringify(Data),
      "method":"POST"
    }
    fetch(postUrl,params)
    .then(res=>{return res.json()})
    .then(data=>{answer=data})
    .then(()=>{console.log(answer)})
    .catch(error=>console.log(error))
    .then(()=>updateDocs())

  }

  function selectedHi(docId) {
    x = document.getElementById("doc"+selectedDocId)
    x.className = x.className.replace(" w3-indigo"," ");
    document.getElementById("doc"+docId).className += " w3-indigo";
  }

  function updateAnswers(docId) {
    document.getElementById("docText").innerHTML = answer[docId]["html"].replace(/src="/g,'src="https://prm.amfam.com');
    selectedHi(docId);
    selectedDocId = docId;
    for (var i in answer[docId]) {
    if(answer[docId][i].answer_str != ""){
      document.getElementById("answer"+i).innerText = "(" + answer[docId][i].likelihood.toFixed(2) + "): " +answer[docId][i].answer_str;
    }else{document.getElementById("answer"+i).innerText = "(" + answer[docId][i].likelihood.toFixed(2) + "): " + "<<not in this document>>"}

  }
}

  function updateDocs() {
    document.getElementById("submitButton").innerHTML = "Submit";
    for (i=0;i<5;i++) {
      showFn("answer"+i);
    }
    for (var docId in answer) {
      if (docId != -1){
      document.getElementById("doc"+docId).innerHTML = "(" +
        answer[docId]['score'].toFixed(2) + "): " +
        answer[docId]["doc_id"].replace("root_","").replace(".txt","").split('_').join(' &shy;');
      showFn("doc"+docId);
      }
    }
    document.getElementById("best").innerText = answer[-1]['answer_str'];
    showFn("best1");

    document.getElementById("doc"+answer[-1]['docid']).click();
    document.getElementById("answer"+answer[-1]['answer_id']).click();
    document.getElementById("doc"+answer[-1]['docid']).focus();
  }

  function highLight(aId) {
    splitter = answer[selectedDocId][aId]['answer_str'];
    if (splitter != '') {
    joiner = '<mark>' + splitter + '</mark>';
    html1 = answer[selectedDocId]['html'].split(splitter).join(joiner).replace(/href="/g,'href="https://prm.amfam.com');
    document.getElementById("docText").innerHTML = html1.replace(/src="/g,'src="https://prm.amfam.com');
  }
  }

  function hideFn(id) {
    var e = document.getElementById(id);
    e.className = e.className.replace(" w3-show","");
  }
  function showFn(id) {
    console.log('shsowhid',id);
    var e = document.getElementById(id);
    // console.log(e);
    if (e.className.indexOf("w3-show") == -1) {
      e.className += " w3-show"}
    // console.log(e);
  }
