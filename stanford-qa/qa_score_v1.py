# import unittest
# from bertmaster import modeling
import qaclasses36 as qaclasses
from keras.models import Model, model_from_json
import tensorflow as tf
import json
import numpy as np
import sys
import keras
from keras_bert import load_trained_model_from_checkpoint, Tokenizer
from keras.layers import Dense
from keras.activations import softmax
from keras.callbacks import ModelCheckpoint

import logging

#bert_layer_num = '11'
bert_layer_num = sys.argv[1]
bln = (int(bert_layer_num) - 12)*4 - 1

with open("squad_model-bl_%s.json"%bert_layer_num, "r") as f:
    m_json = f.read()


base_path = 'bertmaster/uncased_L-12_H-768_A-12'

config_path = base_path + '/bert_config.json'
checkpoint_path = base_path + '/bert_model.ckpt'
dict_path = base_path + '/vocab.txt'
# input_data = 'train-v2.0.json'#'dev-v2.0.json'#'train-v2.0.json'#'dev-v2.0.json'#'test_sample_squad.json'#'dev-v2.0.json'
# intermediate_filepath = 'weights-bl_%s-{epoch:02d}.hdf5'%bert_layer_num
# intermediate_checkpoint = ModelCheckpoint(intermediate_filepath)
input_data = 'test_sample_squad.json'

model = load_trained_model_from_checkpoint(config_path, checkpoint_path)
encoding_out = model.layers[bln].output

def softmx(x):
    return softmax(x,axis=1)

start_layer = Dense(1,activation=softmx)
start_out = start_layer(encoding_out)
end_layer = Dense(1,activation=softmx)
end_out = end_layer(encoding_out)

squad_model = Model(model.inputs,[start_out,end_out])
squad_model.load_weights('weights-bl_%s-03.hdf5'%bert_layer_num)



with open(input_data,'r') as f:
    d = json.load(f)


r = qaclasses.parse_squad(d)

ids,segment_ids,input_mask,start_idx,end_idx = [np.array(_) for _ in zip(*[x.encode() for x in r])]

ids.tofile('preprocessed/train_ids.np')
segment_ids.tofile('preprocessed/train_segment_ids.np')
input_mask.tofile('preprocessed/train_input_mask.np')
start_idx.tofile('preprocessed/train_start_idx.np')
end_idx.tofile('preprocessed/train_end_idx.np')


target_shape = list(ids.shape) + [1]#[ids.shape[0],1,ids.shape[1]]#
start_ids = np.zeros(target_shape,dtype=int)
end_ids = np.zeros(target_shape,dtype=int)
for i,idx in enumerate(start_idx):
    start_ids[i,idx,0] = 1

for i,idx in enumerate(end_idx):
    end_ids[i,idx,0] = 1

x = squad_model.predict([ids,segment_ids])
for y in zip(zip(*x),start_ids,end_ids):
    print(y[0][0].argmax(),y[0][0].max().round(2),y[0][1].argmax(),y[0][1].max().round(2),y[1].argmax(),y[2].argmax(),sep='\t')


for y in zip(zip(*x),start_ids,end_ids):
    print(y[0][0][0]+y[0][1][0],y[0][0][1:].argmax(),y[0][0][1:].max().round(2),y[0][1][1:].argmax(),y[0][1][1:].max().round(2),y[1].argmax(),y[2].argmax(),sep='\t')




ys = x[0][0]
ye = x[1][0]
for _ in zip(sorted(enumerate(ys),key=lambda y: y[1])[-10:],
sorted(enumerate(ye),key=lambda y: y[1])[-10:]):
    print(_)


# print(ids.shape,segment_ids.shape,input_mask.shape,start_ids.shape,end_ids.shape)

model_json = squad_model.to_json()

with open("squad_model-bl_%s.json"%bert_layer_num, "w") as json_file:
    json_file.write(model_json)

squad_model.fit([ids,segment_ids],[start_ids,end_ids],batch_size=32,epochs=3,callbacks=[intermediate_checkpoint])

for _ in zip(sorted(enumerate(ys),key=lambda y: y[1])[-10:],
sorted(enumerate(ye),key=lambda y: y[1])[-10:]):
    print(_)

if False:
    x = squad_model.predict([ids,segment_ids])
    ys = x[0][0]
    ye = x[1][0]

    for y in zip(zip(*x),start_ids,end_ids):
        print(y[0][0].argmax(),y[0][1].argmax(),y[1].argmax(),y[2].argmax())


    ####scratch below, works above

    r[2].encode()
    ids,segment_ids,input_mask,start_idx,end_idx = r[2].encode()
    x = squad_model.predict([[np.array(ids)],[np.array(segment_ids)]])
    ys = x[0]
    ye = x[1]

    embedding = model.predict([[np.array(ids)],[np.array(segment_ids)]])
