import unittest
import qaclasses
import json

with open('test_sample_squad.json','r') as f:
    d = json.load(f)

class TestQABase(unittest.TestCase):
    def test_base_init(self):
        a = qaclasses.QandABase('','','',1,False)
        self.assertTrue(True)

    def test_base_tokenize_context(self):
        a = qaclasses.QandABase('','','',1,False)
        self.assertRaises(NotImplementedError,a.tokenize_context)

    def test_base_tokenize_question(self):
        a = qaclasses.QandABase('','','',1,False)
        self.assertRaises(NotImplementedError,a.tokenize_question)

    def test_base_answer_token_idx(self):
        a = qaclasses.QandABase('','','',1,False)
        self.assertRaises(NotImplementedError,a.answer_token_idx)

    def test_base_extract(self):
        a = qaclasses.QandABase('','','',1,False)
        self.assertRaises(NotImplementedError,a.extract)

class TestSquad(unittest.TestCase):
    d_possible = {'question' : 'Hi how are you?',
            'context' : 'I feel great today!',
            'answer_str' : 'great',
            'answer_start' : 7,
            'is_impossible' : False}
    d_impossible = {'question' : 'hi how are you?',
                'context' : 'the weather is cloudy',
                'answer_str' : None,
                'answer_start' : None,
                'is_impossible' : True}
    def test_squad_init_context(self):
        a = qaclasses.Squad(**self.d_possible)
        k = 'context'
        self.assertEqual(getattr(a,k),self.d_possible[k])

    def test_squad_init_question(self):
        a = qaclasses.Squad(**self.d_possible)
        k = 'question'
        self.assertEqual(getattr(a,k),self.d_possible[k])

    def test_squad_init_answer_str(self):
        a = qaclasses.Squad(**self.d_possible)
        k = 'answer_str'
        self.assertEqual(getattr(a,k),self.d_possible[k])

    def test_squad_init_answer_start(self):
        a = qaclasses.Squad(**self.d_possible)
        k = 'answer_start'
        self.assertEqual(getattr(a,k),self.d_possible[k])

    def test_squad_init_context_impossible(self):
        a = qaclasses.Squad(**self.d_impossible)
        k = 'context'
        self.assertEqual(getattr(a,k),self.d_impossible[k])

    def test_squad_init_question_impossible(self):
        a = qaclasses.Squad(**self.d_impossible)
        k = 'question'
        self.assertEqual(getattr(a,k),self.d_impossible[k])

    def test_squad_init_answer_str_impossible(self):
        a = qaclasses.Squad(**self.d_impossible)
        k = 'answer_str'
        self.assertEqual(getattr(a,k),self.d_impossible[k])

    def test_squad_init_answer_start_impossible(self):
        a = qaclasses.Squad(**self.d_impossible)
        k = 'answer_start'
        self.assertEqual(getattr(a,k),self.d_impossible[k])

    def test_squad_tokenize_question(self):
        a = qaclasses.Squad(**self.d_possible)
        self.assertEqual(a.tokenize_question(),['hi', 'how', 'are', 'you','?'])

    def test_squad_tokenize_context(self):
        a = qaclasses.Squad(**self.d_possible)
        self.assertEqual(a.tokenize_context(),['i', 'feel', 'great', 'today','!'])

    def test_wordpiece(self):
        self.assertEqual(qaclasses.TOKENIZER.tokenize("johanson's demystifying"),
            ['johan','##son',"'",'s','dem','##yst','##ifying'])

    def test_squad_answer_token_idx(self):
        a = qaclasses.Squad(**self.d_possible)
        self.assertEqual((2,3),a.answer_token_idx())

    def test_squad_answer_token_idx_impossible(self):
        a = qaclasses.Squad(**self.d_impossible)
        self.assertEqual((-1,-1),a.answer_token_idx())

    def test_squad_pack(self):
        a = qaclasses.Squad(**self.d_possible)
        r = [qaclasses.TOKEN_CLS] + ['hi', 'how', 'are', 'you','?'] + \
            [qaclasses.TOKEN_SEP] + ['i','feel','great','today','!'] + \
            [qaclasses.TOKEN_SEP] #+ ([qaclasses.TOKEN_PAD]*(qaclasses.MAX_TOKENS - 13))
        r1 = [0]*7 + [1]*6 #+ #[0]*(qaclasses.MAX_TOKENS - 13)
        # self.assertEqual(len(r),len(r1))
        # self.assertEqual(len(r),qaclasses.MAX_TOKENS)
        self.assertEqual((r,r1,7),a._pack())

    def test_squad_pack_impossible(self):
        a = qaclasses.Squad(**self.d_impossible)
        r = [qaclasses.TOKEN_CLS] + ['hi', 'how', 'are', 'you','?'] + \
            [qaclasses.TOKEN_SEP] + ['the','weather','is','cloudy'] + \
            [qaclasses.TOKEN_SEP] #+ ([qaclasses.TOKEN_PAD]*(qaclasses.MAX_TOKENS - 12))
        r1 = [0]*7 + [1]*5 #+ #[0]*(qaclasses.MAX_TOKENS - 12)
        # self.assertEqual(len(r),len(r1))
        # self.assertEqual(len(r),qaclasses.MAX_TOKENS)
        self.assertEqual((r,r1,7),a._pack())

    def test_squad_encode(self):
        if qaclasses.VOCAB_FILE != 'bertmaster/uncased_L-12_H-768_A-12/vocab.txt':
            raise ValueError('tests must be run with qaconfig.VOCAB_FILE bertmaster/uncased_L-12_H-768_A-12/vocab.txt')
        a = qaclasses.Squad(**self.d_possible)
        r = [101] + [7632, 2129, 2024, 2017,1029] + \
            [102] + [1045,2514,2307,2651,999] + \
            [102] + ([0]*(qaclasses.MAX_TOKENS - 13))
        r1 = [0]*7 + [1]*6 + [0]*(qaclasses.MAX_TOKENS - 13)
        r2 = [1]*13 + [0]*(qaclasses.MAX_TOKENS - 13)
        r3 = 9
        r4 = 10
        # self.assertEqual(len(r),len(r1))
        # self.assertEqual(len(r),qaclasses.MAX_TOKENS)
        self.assertEqual(a.encode(),(r,r1,r2,r3,r4))

    def test_squad_encode_impossible(self):
        if qaclasses.VOCAB_FILE != 'bertmaster/uncased_L-12_H-768_A-12/vocab.txt':
            raise ValueError('tests must be run with qaconfig.VOCAB_FILE bertmaster/uncased_L-12_H-768_A-12/vocab.txt')
        a = qaclasses.Squad(**self.d_impossible)
        r = [101] + [7632, 2129, 2024, 2017,1029] + \
            [102] + [1996,4633,2003,24706] + \
            [102] + ([0]*(qaclasses.MAX_TOKENS - 12))
        r1 = [0]*7 + [1]*5 + [0]*(qaclasses.MAX_TOKENS - 12)
        r2 = [1]*12 + [0]*(qaclasses.MAX_TOKENS - 12)
        r3 = 0
        r4 = 0
        # self.assertEqual(len(r),len(r1))
        # self.assertEqual(len(r),qaclasses.MAX_TOKENS)
        self.assertEqual(a.encode(),(r,r1,r2,r3,r4))

# TOKEN_UNK = '[UNK]'  # Token for unknown words
# TOKEN_CLS = '[CLS]'  # Token for classification 101
# TOKEN_SEP = '[SEP]'  # Token for separation 102
# TOKEN_PAD = ''  # Token for padding 0

    # def test_squad_answer_end_idx(self):
    #     a = qaclasses.Squad(**self.d_possible)
    #     self.assertEqual(3,a.answer_end_idx())
    #
    # def test_squad_answer_end_idx_impossible(self):
    #     a = qaclasses.Squad(**self.d_impossible)
    #     self.assertEqual(-1,a.answer_end_idx())


class TestParseSquad(unittest.TestCase):
    def test_parse_squad(self):
        r = qaclasses.parse_squad(d)
        question = 'In what country is Normandy located?'
        context = '''The Normans (Norman: Nourmands; French: Normands; Latin: No'''+\
        '''rmanni) were the people who in the 10th and 11th centuries gave their name t'''+\
        '''o Normandy, a region in France. They were descended from Norse ("Norman" com'''+\
        '''es from "Norseman") raiders and pirates from Denmark, Iceland and Norway who'''+\
        ''', under their leader Rollo, agreed to swear fealty to King Charles III of We'''+\
        '''st Francia. Through generations of assimilation and mixing with the native F'''+\
        '''rankish and Roman-Gaulish populations, their descendants would gradually mer'''+\
        '''ge with the Carolingian-based cultures of West Francia. The distinct cultura'''+\
        '''l and ethnic identity of the Normans emerged initially in the first half of '''+\
        '''the 10th century, and it continued to evolve over the succeeding centuries.'''
        answer_str = 'France'
        answer_start = 159
        is_impossible = False
        a = qaclasses.Squad(context,question,answer_str,answer_start,is_impossible)
        self.assertEqual(a,r[0])

    def test_parse_squad_impossible(self):
        r = qaclasses.parse_squad(d)
        question = 'When did the Frankish identity emerge?'
        context = '''The Normans (Norman: Nourmands; French: Normands; Latin: No'''+\
        '''rmanni) were the people who in the 10th and 11th centuries gave their name t'''+\
        '''o Normandy, a region in France. They were descended from Norse ("Norman" com'''+\
        '''es from "Norseman") raiders and pirates from Denmark, Iceland and Norway who'''+\
        ''', under their leader Rollo, agreed to swear fealty to King Charles III of We'''+\
        '''st Francia. Through generations of assimilation and mixing with the native F'''+\
        '''rankish and Roman-Gaulish populations, their descendants would gradually mer'''+\
        '''ge with the Carolingian-based cultures of West Francia. The distinct cultura'''+\
        '''l and ethnic identity of the Normans emerged initially in the first half of '''+\
        '''the 10th century, and it continued to evolve over the succeeding centuries.'''
        answer_str = None
        answer_start = None
        is_impossible = True
        a = qaclasses.Squad(context,question,answer_str,answer_start,is_impossible)
        self.assertEqual(a,r[23])




if __name__ == '__main__':
    unittest.main()
    # suite = unittest.TestLoader().loadTestsFromTestCase(TestQABase)
    # unittest.TextTestRunner(verbosity=2).run(suite)
    # suite = unittest.TestLoader().loadTestsFromTestCase(TestSquad)
    # unittest.TextTestRunner(verbosity=2).run(suite)
    # suite = unittest.TestLoader().loadTestsFromTestCase(TestParseSquad)
    # unittest.TextTestRunner(verbosity=2).run(suite)
