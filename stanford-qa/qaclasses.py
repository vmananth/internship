from dataclasses import dataclass
from bertmaster import tokenization
import typing
import qaconfig

VOCAB_FILE = qaconfig.VOCAB_FILE
MAX_TOKENS = qaconfig.MAX_TOKENS
TOKEN_UNK = qaconfig.TOKEN_UNK
TOKEN_CLS = qaconfig.TOKEN_CLS
TOKEN_SEP = qaconfig.TOKEN_SEP
TOKEN_PAD = qaconfig.TOKEN_PAD

# VOCAB_FILE = 'bertmaster/uncased_L-12_H-768_A-12/vocab.txt'

TOKENIZER = tokenization.FullTokenizer(VOCAB_FILE,do_lower_case=True)

# MAX_TOKENS = 512
#
# TOKEN_UNK = '[UNK]'  # Token for unknown words
# TOKEN_CLS = '[CLS]'  # Token for classification
# TOKEN_SEP = '[SEP]'  # Token for separation
# TOKEN_PAD = ''  # Token for padding


@dataclass
class QandABase():
    context: str
    question: str
    answer_str: str
    answer_start: str
    is_impossible: bool

    def tokenize_context(self):
        raise NotImplementedError('not implemented')

    def tokenize_question(self):
        raise NotImplementedError('not implemented')

    def answer_token_idx(self):
        raise NotImplementedError('not implemented')

    def extract(self):
        raise NotImplementedError('not implemented')

class Squad(QandABase):
    def __init__(self,context,question,answer_str,answer_start,is_impossible):
        super().__init__(context,question,answer_str,answer_start,is_impossible)

    def tokenize_context(self):
        return TOKENIZER.tokenize(self.context)

    def tokenize_question(self):
        return TOKENIZER.tokenize(self.question)

    def answer_token_idx(self):
        if self.is_impossible:
            return -1,-1
        start_idx = len(TOKENIZER.tokenize(self.context[:self.answer_start]))
        return start_idx,start_idx+len(TOKENIZER.tokenize(self.answer_str))

    def encode(self,max_tokens=MAX_TOKENS):
        tokens,segment_ids,q_len = self._pack(max_tokens)
        unk_id = TOKENIZER.vocab.get(TOKEN_UNK)
        ids = [TOKENIZER.vocab.get(token,unk_id) for token in tokens]
        l = (MAX_TOKENS - len(tokens))
        ids += ([0]*l)
        segment_ids += ([0]*l)
        input_mask = [1]*len(tokens) + [0]*l
        start_idx,end_idx = self.answer_token_idx()
        if start_idx >= 0:
            start_idx = (q_len + start_idx)
            if start_idx <= MAX_TOKENS:
                end_idx = min(q_len + end_idx,MAX_TOKENS)
            else:
                start_idx = 0
                end_idx = 0
        else:
            start_idx = 0
            end_idx = 0
        return ids,segment_ids,input_mask,start_idx,end_idx

    def _pack(self,max_tokens=MAX_TOKENS):
        qt = self.tokenize_question()
        ct = self.tokenize_context()
        tokens = ([TOKEN_CLS] + qt + [TOKEN_SEP] + ct)[:(MAX_TOKENS - 1)] + \
                [TOKEN_SEP]
                # + [TOKEN_PAD]*(MAX_TOKENS - len(qt) - len(ct) - 3)
        segment_ids = ([0]*(len(qt)+2) + [1]*(len(ct)))[:(MAX_TOKENS - 1)] + \
                [1]
                # [0]*(MAX_TOKENS - len(qt) - len(ct) - 3)
        return tokens,segment_ids,len(qt)+2



def parse_squad(d):
    results = []
    for topic in d['data']:
        for paragraph in topic['paragraphs']:
            context = paragraph['context']
            for qa in paragraph['qas']:
                # print(qa)
                question = qa['question']
                # print(question)
                # print(qa['answers'])
                is_impossible = qa['is_impossible']
                id = qa['id']
                if not is_impossible:
                    for x in qa['answers']:
                        answer_str = x['text']
                        answer_start = x['answer_start']
                        a = Squad(context,question,answer_str,answer_start,is_impossible)
                        results.append(a)

                else:
                    answer_str,answer_start = None,None
                    a = Squad(context,question,answer_str,answer_start,is_impossible)
                    results.append(a)
    return results
