IP='10.161.4.37'
ssh -i ~/Documents/key/dxd036_ieh201.pem ubuntu@$IP
lsblk #supresses /dev/
# NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
# xvda    202:0    0   75G  0 disk
# └─xvda1 202:1    0   75G  0 part /
# xvdb    202:16   0  256G  0 disk
# loop0     7:0    0 91.1M  1 loop /snap/core/6531
# loop1     7:1    0 87.9M  1 loop /snap/core/5742
# loop2     7:2    0 17.9M  1 loop /snap/amazon-ssm-agent/1068
# loop3     7:3    0 16.5M  1 loop /snap/amazon-ssm-agent/784
# loop4     7:4    0 89.3M  1 loop /snap/core/6673
sudo file -s /dev/xvdb
#/dev/xvdb: data
#since it doesn't already have a file system, create one:
# sudo mkfs -t xfs /dev/xvdb
#now:
sudo file -s /dev/xvdb
# /dev/xvdb: SGI XFS filesystem data (blksz 4096, inosz 512, v2 dirs)

sudo mkdir /data
sudo mount /dev/xvdb /data
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html
sudo chown `whoami` /data




# from mac:
# IP='10.161.4.37'
# scp -r -i ~/Documents/key/dxd036_ieh201.pem ~/projects/stanford-qa ubuntu@$IP:/data
curl -kLs \
-o /tmp/art_setup.sh \
-- https://bitbucket.amfam.com/snippets/raw/4111b4f914f34d789b34b30854b8cb1b/artifactory-user-setup.sh
#modify art_setup.sh so _my_corporate_user=dxd036
chmod +x /tmp/art_setup.sh
/tmp/art_setup.sh

rm -f /tmp/art_setup.sh

sed -i 's/\/Users\/dxd036/\/home\/ubuntu/g' ~/.pip/pip.conf
pip install amfam.aws-cli-utils

source activate tensorflow_p36
pip install keras_bert
pip install amfam.aws-cli-utils

aws-adfs-login -u dxd036
source /home/ubuntu/.aws/sessions/amfam-ieh201-exp-engineer-r.env
aws s3 cp --recursive ./ s3://amfam-ieh201-squad-pre-trained



nvidia-smi

# sudo chown `whoami` /mnt
# sudo chmod 777 /mnt

# conda create -n qatf37 python=3.7 pip
# pip install -r req.txt
# conda activate qatf37

# scp -i ~/Documents/key/dxd036_ieh201.pem ubuntu@$IP:/data/stanford-qa/weights-bl_12-03.hdf5 ./
# scp -i ~/Documents/key/dxd036_ieh201.pem ubuntu@$IP:/data/stanford-qa/squad_training-bl_12.log ./
# scp -i ~/Documents/key/dxd036_ieh201.pem ubuntu@$IP:/data/stanford-qa/squad_model-bl_12.json ./
arn:aws:s3:::amfam-ieh201-squad-pre-trained
