from bertfam.pretrain.utils import fill_in
from bertfam import load_pretrained_tf_model
from bertfam.model import EMBEDDINGS, set_pretrained_weights, get_layer_names


sample = False

sents = [
    "customer called because [MASK] [MASK] [MASK] , i think this one is a [MASK] [MASK] .",
    "customer called because [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .",
    "[MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , so they need a rental car.",
    "[MASK] wants to know if they are [MASK] for the [MASK] discount .",
    "customer is wondering [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .",
    "you are eligible if [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] . ",
]

sents = [
    "equipment or accessories of the camper that are usual to a private passenger car . [MASK] [MASK] [MASK] "
]

sents = [
    "equipment or accessories of the camper that are usual to a [MASK] [MASK] car . "
]

sents = ["customer is calling because [MASK] [MASK] [MASK] ."]


def main():

    # base_model = load_pretrained_tf_model(pretraining=True, max_seq_len=128)
    fam_model = load_pretrained_tf_model(pretraining=True)
    # fam_model.get_layer(EMBEDDINGS).pos_embeddings.trainable = False

    set_pretrained_weights(
        fam_model, "models/bertfam-base-uncased-weights.hdf5", ignore_pos=True
    )

    for sent in sents:
        print(f"template: {sent}".replace("[MASK]", "_____"))
        # print(f"base model: {fill_in(base_model, sent, sample=sample)}")
        print(f"amfam model: {fill_in(fam_model, sent, sample=sample)}")
        print()


if __name__ == "__main__":
    main()
