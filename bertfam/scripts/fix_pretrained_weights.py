import h5py
from tensorflow.python.keras.engine import saving


TO_FIX = "bertfam-base-uncased.hdf5"
FIXED = TO_FIX.replace(".hdf5", "-fixed.hdf5")

assert TO_FIX != FIXED

with h5py.File(TO_FIX, "r") as to_fix:
    with h5py.File(FIXED, "w") as fixed:
        layer_names = saving.load_attributes_from_hdf5_group(to_fix, "layer_names")
        saving.save_attributes_to_hdf5_group(
            fixed, "layer_names", [n.encode() for n in layer_names]
        )
        for layer_name in layer_names:
            fixed.create_group(layer_name)
            g = to_fix[layer_name]
            weight_names = saving.load_attributes_from_hdf5_group(g, "weight_names")
            saving.save_attributes_to_hdf5_group(
                fixed[layer_name], "weight_names", [n.encode() for n in weight_names]
            )
            new_weight_names = []
            for weight_name in weight_names:
                x = to_fix[layer_name][weight_name]
                if "/" not in weight_name:
                    if layer_name == "pooled_output":
                        weight_name = "pooler/" + weight_name
                    if layer_name == "mlm_output":
                        weight_name = "mlm/" + weight_name
                        weight_name = weight_name.replace("_1", "")
                    if layer_name == "nsp_output":
                        weight_name = "nsp/" + weight_name
                        weight_name = weight_name.replace("_2", "")
                    print(layer_name, weight_name)
                fixed[layer_name].create_dataset(weight_name, data=x)
                new_weight_names.append(weight_name.encode())
            saving.save_attributes_to_hdf5_group(
                fixed[layer_name], "weight_names", new_weight_names
            )
