"""A script for fixing corrupted tf records files"""
import tensorflow as tf

tf.enable_eager_execution()
import argparse
import pathlib
from itertools import chain
from bertfam.utils import tqdm
from multiprocessing import Pool


parser = argparse.ArgumentParser()

parser.add_argument("inputs", nargs="+")


def is_corrupted(filename: str) -> bool:

    print(f"checking if {filename} is corrupted")

    dataset = tf.data.TFRecordDataset(filename).batch(10000)

    diter = iter(tqdm(dataset))

    while True:
        try:
            next(diter)
        except StopIteration:
            return False
        except tf.errors.DataLossError:
            return True


def fix_record(filename: str):
    if not is_corrupted(filename):
        print(f"{filename} is not corrupted, skipping...")
        return

    print(f"{filename} is corrupted, fixing...")

    fixed = filename + "-fixed"

    dataset = tf.data.TFRecordDataset(filename)

    diter = iter(tqdm(dataset))

    writer = tf.python_io.TFRecordWriter(fixed)

    n = 0
    while True:
        try:
            x = next(diter)
            writer.write(x.numpy())
            n += 1
        except tf.errors.DataLossError:
            break
        except StopIteration:
            break
    print(f"wrote {n} good records to {fixed}")


def main():

    args = parser.parse_args()

    inputs = args.inputs

    inputs = [pathlib.Path(".").glob(i) for i in inputs]
    inputs = list(map(str, chain(*inputs)))

    with Pool() as pool:
        pool.map(fix_record, inputs)


if __name__ == "__main__":
    main()
