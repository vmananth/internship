from bertfam.model import set_pretrained_weights, load_pretrained_tf_model


model = load_pretrained_tf_model()
print(model.layers)
# model.summary()
model.save("test.hdf5")
