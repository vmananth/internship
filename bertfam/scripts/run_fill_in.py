from bertfam.pretrain.utils import fill_in
from bertfam import load_pretrained_tf_model
from bertfam.model import EMBEDDINGS
import tensorflow as tf


def main():

    with tf.device("/cpu:0"):
        model = load_pretrained_tf_model(pretraining=True, max_seq_len=128)
        model.get_layer(EMBEDDINGS).pos_embeddings.trainable = False
        # model.load_weights("bertfam-base-uncased.hdf5")

        print(
            fill_in(
                model,
                "customer called because [MASK] [MASK] [MASK] , i think this one is a [MASK] [MASK] .",
                sample=True,
            )
        )

        print(
            fill_in(
                model,
                "welcome to american family where we protect your [MASK] .",
                sample=False,
            )
        )

        print(fill_in(model, "your dreams are [MASK] .", sample=False))

        print(fill_in(model, "[MASK] fearlessly , [MASK] carefully .", sample=False))


if __name__ == "__main__":
    main()
