from bertfam.pretrain.utils import get_nlp, read_jsonl, write_jsonl
from bertfam.utils import tqdm
from bertfam.pretrain.preprocessing import doc_file_to_tf_records
from bertfam import Tokenizer
from typing import Iterable, List, Union
from pyathena import connect
from itertools import cycle, islice
import os
from functools import partial
from multiprocessing import Pool


ATHENA_TABLE = "nlpresearch.claimnotes_asof20190328"
CHAT_JSON = "s3://sda-text/data/clean/chats.jsonl"
CHAT_JSON = "./chats.jsonl"
TOKENIZER_VOCAB = os.path.expanduser("~/bertfam_vocab.txt")
DOCS_FILE = "docs.jsonl"
DOCS_FILE = "sample.jsonl"

OUTPUT_LOC = "./data"
OUTPUT_FILES = [os.path.join(OUTPUT_LOC, f"tfrecord{i:02d}") for i in range(32)]


def main():

    tokenizer = Tokenizer.from_textfile(TOKENIZER_VOCAB)

    process = partial(input_to_doc, tokenizer=tokenizer)

    # print(f"preprocessing and saving to {DOCS_FILE}")

    # with Pool() as pool:
    # write_jsonl(
    # DOCS_FILE,
    # tqdm(
    # filter(None, pool.imap_unordered(to_features, load_all(), chunksize=1024))
    # ),
    # )

    print("done preprocessing")

    doc_file_to_tf_records(DOCS_FILE, tokenizer, 128, OUTPUT_FILES)


def roundrobin(*iterables):
    """roundrobin('ABC', 'D', 'EF') --> A D E B F C
    Recipe credited to George Sakkis

    From https://docs.python.org/3/library/itertools.html#recipes"""

    num_active = len(iterables)
    nexts = cycle(iter(it).__next__ for it in iterables)
    while num_active:
        try:
            for n in nexts:
                yield n()
        except StopIteration:
            # Remove the iterator we just exhausted from the cycle.
            num_active -= 1
            nexts = cycle(islice(nexts, num_active))


def chat_filter(chat: dict) -> bool:
    return not chat["chatbot_flag"]


def utterance_filter(utterance: dict) -> bool:
    if utterance["speaker"] == "sys":
        return False
    if utterance["speaker"] == "rep" and utterance["utterance"].lower().startswith(
        "thank you for contacting"
    ):
        return False
    if utterance["speaker"] == "rep" and utterance["utterance"].lower().startswith(
        "warning: due to"
    ):
        return False
    if utterance["utterance"].lower().startswith("due to inactivity"):
        return False
    return True


def chat_to_doc(chat: dict, tokenizer: Tokenizer) -> List[List[str]]:
    utterances = filter(utterance_filter, (u for u in chat["chat"]))
    utterances = (u["utterance"].strip() for u in utterances if u["utterance"])
    utterances = filter(None, utterances)
    utterances = get_nlp().pipe(utterances)
    doc = tokenizer.tokenize_texts(str(s) for d in utterances for s in d.sents)
    return doc


def note_to_doc(note: str, tokenizer: Tokenizer) -> List[List[str]]:
    note = get_nlp()(note)
    doc = tokenizer.tokenize_texts(str(s) for s in note.sents)
    return doc


def input_to_doc(x: Union[dict, str], tokenizer: Tokenizer) -> List[List[str]]:
    if isinstance(x, str):
        return note_to_doc(x, tokenizer)
    elif isinstance(x, dict):
        return chat_to_doc(x, tokenizer)
    else:
        raise TypeError(f"got {type(x)}, expected dict or str")


def load_chats() -> Iterable[dict]:
    for i, chat in enumerate(filter(chat_filter, read_jsonl(CHAT_JSON))):
        yield chat


def load_notes() -> Iterable[str]:
    connection = connect(s3_staging_dir="s3://sda-nlpresearch/athena/textrep/")

    for row in connection.cursor().execute(f"SELECT notebody FROM {ATHENA_TABLE};"):
        notebody = row[0]
        if notebody:
            notebody = notebody.strip()
        if notebody:
            yield notebody


def load_all() -> Iterable[Union[str, dict]]:
    return islice(load_chats(), 100)
    return roundrobin(load_chats(), load_notes())


if __name__ == "__main__":
    main()
