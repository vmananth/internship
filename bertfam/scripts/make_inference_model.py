from bertfam.inference.model import from_pooled_model
from bertfam import load_model, BertTokenizer
from bertfam.model import get_layer_names

model = load_model("models/bertfam-base-uncased-pooled.hdf5")
model.summary()
model = from_pooled_model(model)
model.get_layer("embeddings").trainable = False
model.get_layer("embeddings").cls_embedding = True
tokenizer = BertTokenizer.from_model(model)
print(list(get_layer_names(model)))

print(model.predict(tokenizer("hello world")))

model.summary()
