# tf.enable_eager_execution()
from bertfam import load_pretrained_tf_model
from bertfam.model import set_pretrained_weights

model = load_pretrained_tf_model()

set_pretrained_weights(model, "bertfam-base-uncased-fixed.hdf5", ignore_pos=True)

model.save("bertfam-base-uncased-model.hdf5")
