import tensorflow as tf
from bertfam import load_pretrained_tf_model
from bertfam.pretrain.load import tf_records_to_dataset
from bertfam.model import EMBEDDINGS
import argparse
import pathlib
from itertools import chain
from tensorflow.python.client import device_lib

N_GPUS = sum(map(lambda x: x.device_type == "GPU", device_lib.list_local_devices()))

print(f"{N_GPUS} gpus found")

parser = argparse.ArgumentParser()

parser.add_argument("files", nargs="+")


def main():

    args = parser.parse_args()

    files = [pathlib.Path(".").glob(i) for i in args.files]
    files = list(map(str, chain(*files)))

    adam = tf.keras.optimizers.Adam(2e-5)

    if N_GPUS == 1:
        device = "/gpu:0"
    else:
        device = "/cpu:0"

    with tf.device(device):
        model = load_pretrained_tf_model(
            max_seq_len=128, pretraining=True, optimizer=adam
        )

    embeddings = model.get_layer(EMBEDDINGS)
    embeddings.pos_embeddings.trainable = False

    batch_size = 42

    if N_GPUS > 1:
        cpu_model = model
        model = tf.keras.utils.multi_gpu_model(cpu_model, N_GPUS)
        model.callback_model = cpu_model
        batch_size *= N_GPUS
        model.compile(adam, cpu_model.loss)

    callbacks = [
        tf.keras.callbacks.ModelCheckpoint(
            "bertfam-base-uncased.hdf5", save_weights_only=True
        )
    ]

    dataset = tf_records_to_dataset(files, batch_size, 128)

    samples_per_epoch = 1_000_000

    model.fit(
        dataset,
        steps_per_epoch=samples_per_epoch // batch_size,
        epochs=1000,
        callbacks=callbacks,
    )


if __name__ == "__main__":
    main()
