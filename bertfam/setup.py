from setuptools import setup, find_packages
import os
import time

package_init = os.path.join(os.path.dirname(__file__), "bertfam", "__init__.py")
readme_file = os.path.join(os.path.dirname(__file__), "README.md")

with open(readme_file) as f:
    readme = f.read()


VERSION = None
with open(package_init) as f:
    for line in f:
        if "__version__" in line:
            VERSION = line.split(" = ")[-1].replace('"', "").strip()
            break
if VERSION is None:
    raise ValueError("bertfam.__version__ not found")


if os.environ.get("PYTHON_PACKAGING_ENV") == "development":
    VERSION = f"{VERSION}.dev.{time.strftime('%Y%m%d%H%M%S', time.gmtime())}"


setup(
    name="amfam.bertfam",
    version=VERSION,
    description="A hackable Keras/Tensorflow implementation of BERT maintained by AmFam's DSE",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://bitbucket.amfam.com/projects/SDA/repos/sda-graphs/browse",
    maintainer="DSE",
    maintainer_email="dconatha@amfam.com",
    packages=find_packages(exclude=["tests"]),
    include_package_data=True,
    zip_safe=False,
    install_requires=["tensorflow>=1.13,<2", "requests"],
)
