# bertfam 

```
BERT + AmFam = bertfam 
```

This is a DSE-maintained Python package that implements [BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding](https://arxiv.org/abs/1810.04805) using `keras` and `tensorflow`.  With this package you can:
  - Automagically load pretrained models released by [Google AI Research](https://github.com/google-research/bert#pre-trained-models)
  - Tokenize and encode strings/sentences/sentence pairs/etc. into their BERT representations (`numpy.ndarray`)
  - Use DSE additions/modifications via the `bertfam.contrib` module or hack your own!

## Quickstart

To install:

```
pip install amfam.bertfam
```

This assumes you have [configured artifactory access](https://dse-coe.amfam.com/artifactory/).


Once installed:

> (Note: this will automatically download the pretrained tensorflow model to `~/.keras/bertfam`.  It will default to the `bert-base-uncased` model)


```python
import bertfam

model = bertfam.load_pretrained_tf_model()  # a tf.keras.Model model
tokenizer = bertfam.BertTokenizer.from_pretrained_tf_model()
X = model.predict(tokenizer("hello world!"))
```


`X` should now be a `np.ndarray` of shape `(1, 5, 768)` comprising the `768`-dimensional vectors for the tokens `[CLS]`, `hello`, `world`, `!`, `[SEP]`.


## Tutorial & Documentation

### Tokenizers

There are two tokenizer classes that are important, `BertTokenizer` and `BertBertTokenizer`:
 - `BertTokenizer` is for tokenizing single-sequence items
 - `BertBertTokenizer` is for tokenizing pairs of sequences (e.g. question/answer pairs for SQuAD)
 
 Tokenizer classes implement `__call__` and take `str`s as inputs and produce `np.ndarray`s as outputs, in general they will "do the right thing" for interacting with our BERT models.
 
 Tokenizers take two import keyword boolean arguments, `mask` and `seg_ids`.
  - With `mask=True`, the tokenizer will also return a `np.ndarray` corresponding to a mask (i.e. which tokens are not pad tokens)
  - if `seg_ids=True`, the tokenizer will also return a `np.ndarray` corresponding to a mask specifying which tokens are from the first and second sequences. You will really only need this if you're working on pair tasks with the `BertBertTokenizer`.
  
  Note that `load_tf_model` also accepts the `mask` and `seg_ids` keyword arguments.  In general, the settings of your tokenizer should match the settings of your model, because these settings determine how many outputs the tokenizer produces and how many inputs the model accepts.
  
  
```python
from bertfam import BertTokenizer, BertBertTokenizer

attention_mask
bert_tokenizer = BertTokenizer.from_pretrained_tf_model()
 
# you can pass a single string to the tokenizer
X, mask = bert_tokenizer("hello world")
 
# or a list/iterable of strings
X, mask = bert_tokenizer(["hello world", "this is a test"])
```
 
 Now let's try with a `BertBertTokenizer` for sequence pairs:
```python
# will default to mask=True and seg_ids=True
bertbert_tokenizer = BertBertTokenizer.from_pretrained_tf_model()

# you can pass pairs of strings as tuples:
X, mask, seg_ids = bertbert_tokenizer(("how are you?", "i'm good"))

# or a list/iterables of pairs/tuples:
X, mask, seg_ids = bertbert_tokenizer([("how are you?", "i'm good"), ("what's your favorite food?", "pizza")])
```

To be explicit, *you can pass an iterable of strings* to the tokenizer, which can be more memory efficient:
```python
with open("very_big_file.txt") as f:
    X, mask = bert_tokenizer(line for line in f)
```

In general, the tokenizer-to-model pipeline should "just work" as long as the settings match (i.e. `mask` and `seg_ids` are the same).

```python
from bertfam import load_pretrained_tf_model 

model = load_pretrained_tf_model(mask=True, seg_ids=True)
tokenizer = BertTokenizer.from_tf_model(mask=True, seg_ids=True)

X = model.predict(tokenizer("hello world!"))
```

etc.


## Dev Setup

The environment is managed through [pipenv](https://pipenv.readthedocs.io/en/latest/).  We use Python 3.7.  To install/activate the environment:

```
pipenv sync
pipenv shell
```

Optionally use `pipenv sync --dev` to get the dev dependencies (e.g. to run tests).

