import tensorflow as tf
from bertfam import Encoder, Embeddings
import numpy as np


vocab_size = 7
vocab = list(map(str, range(vocab_size)))
dim = 10
max_seq_len = 15
num_layers = 2
num_heads = 5
hidden_dim = 9


def test_model():

    embeddings = Embeddings(vocab, dim, max_seq_len)
    embeddings.build((None,))
    encoder = Encoder(num_layers, num_heads, dim, hidden_dim)
    encoder.build((None, dim))

    i = tf.keras.Input(shape=(None,))

    x = embeddings(i)
    x = encoder(x)

    model = tf.keras.Model(i, x)

    X = model.predict(np.array([[1, 2, 3, 0]], dtype=np.int32))

    assert X.shape == (1, 4, dim)


def test_model_with_masking():

    embeddings = Embeddings(vocab, dim, max_seq_len)
    embeddings.build((None,))
    encoder = Encoder(num_layers, num_heads, dim, hidden_dim)
    encoder.build((None, dim))

    i = tf.keras.Input(shape=(None,))
    mask = tf.keras.Input(shape=(None,))

    x = embeddings(i)
    x = encoder(x, padding_mask=mask)

    model = tf.keras.Model([i, mask], x)

    X_input = np.array([[1, 2, 3, 0], [0, 0, 1, 2]], dtype=np.int32)
    X_mask = np.zeros_like(X_input)
    X_mask[0, 2] = 1
    X_mask[0, 3] = 1

    X = model.predict([X_input, X_mask])

    assert X.shape == (2, 4, dim)


def test_model_with_seg_ids():

    embeddings = Embeddings(vocab, dim, max_seq_len)
    embeddings.build((None,))
    encoder = Encoder(num_layers, num_heads, dim, hidden_dim)
    encoder.build((None, dim))

    i = tf.keras.Input(shape=(None,))
    seg_ids = tf.keras.Input(shape=(None,))

    x = embeddings(i, seg_ids=seg_ids)
    x = encoder(x)

    model = tf.keras.Model([i, seg_ids], x)

    X_input = np.array([[1, 2, 3, 0], [0, 0, 1, 2]], dtype=np.int32)
    X_seg = np.zeros_like(X_input)
    X_seg[0, 2] = 1
    X_seg[0, 3] = 1

    X = model.predict([X_input, X_seg])

    assert X.shape == (2, 4, dim)
