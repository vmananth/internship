import pytest
from extract_features import (
    model_fn_builder,
    InputExample,
    convert_examples_to_features,
    input_fn_builder,
)
from typing import List, Tuple
from tokenization import FullTokenizer
from modeling import BertConfig, BertModel
import numpy as np
from bertfam.model import download_pretrained_tf_model
from bertfam import load_pretrained_tf_model, load_model
import os
import tensorflow as tf

from bertfam import BertTokenizer


MODEL = "bert-base-uncased"


def google_bert_mlm():

    model_path = download_pretrained_tf_model(MODEL)

    config = BertConfig.from_json_file(os.path.join(model_path, "bert_config.json"))

    def model_fn(features, labels, mode, params):

        model = BertModel(
            config=config,
            is_training=False,
            input_ids=features["input_ids"],
            input_mask=features["input_mask"],
            token_type_ids=features["input_type_ids"],
        )

        tvars = tf.trainable_variables()

    pass


def encode_google_bert(
    text: List[str], seg_ids: bool = False
) -> Tuple[np.ndarray, ...]:

    model_path = download_pretrained_tf_model(MODEL)

    config = BertConfig.from_json_file(os.path.join(model_path, "bert_config.json"))
    tokenizer = FullTokenizer(
        vocab_file=os.path.join(model_path, "vocab.txt"), do_lower_case=True
    )

    model_fn = model_fn_builder(
        config, os.path.join(model_path, "bert_model.ckpt"), [-1], False, False
    )

    run_config = tf.contrib.tpu.RunConfig(
        master=None,
        tpu_config=tf.contrib.tpu.TPUConfig(
            num_shards=8,
            per_host_input_for_training=tf.contrib.tpu.InputPipelineConfig.PER_HOST_V2,
        ),
    )

    estimator = tf.contrib.tpu.TPUEstimator(
        use_tpu=False,
        model_fn=model_fn,
        config=run_config,
        predict_batch_size=1,
        params={},
    )

    examples = [InputExample(0, t, None) for t in text]
    x = convert_examples_to_features(
        examples, config.max_position_embeddings, tokenizer
    )
    n_tokens_0 = len(x[0].tokens)
    n_tokens_1 = len(x[1].tokens)
    if seg_ids:
        x[0].input_type_ids[: n_tokens_0 // 2] = [1] * (n_tokens_0 // 2)
        x[1].input_type_ids[: n_tokens_1 // 2] = [1] * (n_tokens_1 // 2)
    input_fn = input_fn_builder(x, config.max_position_embeddings)

    output = list(estimator.predict(input_fn))
    y0 = output[0]["layer_output_0"]
    y1 = output[1]["layer_output_0"]

    return y0[:n_tokens_0], y1[:n_tokens_1]


def encode_bertfam(
    text: List[str], seg_ids: bool = False, reload: bool = False
) -> Tuple[np.ndarray, ...]:

    model = load_pretrained_tf_model(MODEL, seg_ids=True)
    if reload:
        tmp_filename = "tmp_bertfam_model.hdf5"
        try:
            model.save(tmp_filename)
            model = load_model(tmp_filename)
        finally:
            if os.path.exists(tmp_filename):
                os.remove(tmp_filename)
    tokenizer = BertTokenizer.from_model(model)

    tokens = tokenizer(text)
    n_tokens0 = sum(tokens[1][0])
    n_tokens1 = sum(tokens[1][1])

    # set second half of sentence to have seg_id=1
    if seg_ids:
        tokens[2][0][: n_tokens0 // 2] = 1
        tokens[2][1][: n_tokens1 // 2] = 1

    y = model.predict(tokens)

    return y[0][:n_tokens0], y[1][:n_tokens1]


@pytest.mark.skipif(f"{not os.environ.get('ALL_BERTFAM_TESTS')}")
def test_compare_seq_output():

    text = ["hello world, this is a test, and how are you?", "test test"]

    x0_bertfam, x1_bertfam = encode_bertfam(text)
    x0_google, x1_google = encode_google_bert(text)
    print(
        "frobenius norm of sentence matrix diff between google-research/bert and bertfam:"
    )
    print("without some padding/masking:")
    print(np.linalg.norm(x0_google - x0_bertfam))
    print("with some padding/masking:")
    print(np.linalg.norm(x1_google - x1_bertfam))
    assert np.linalg.norm(x0_google - x0_bertfam) < 0.01
    assert np.linalg.norm(x1_google - x1_bertfam) < 0.01

    print("with second half of sequence as seq_id=1:")
    x0_google, x1_google = encode_google_bert(text, True)
    x0_bertfam, x1_bertfam = encode_bertfam(text, True)
    print("without some padding/masking:")
    print(np.linalg.norm(x0_google - x0_bertfam))
    print("with some padding/masking:")
    print(np.linalg.norm(x1_google - x1_bertfam))
    assert np.linalg.norm(x0_google - x0_bertfam) < 0.01
    assert np.linalg.norm(x1_google - x1_bertfam) < 0.01

    print("with saving/loading of bertfam model (using model.save())")
    x0_google, x1_google = encode_google_bert(text)
    x0_bertfam, x1_bertfam = encode_bertfam(text, reload=True)
    print("without some padding/masking:")
    print(np.linalg.norm(x0_google - x0_bertfam))
    print("with some padding/masking:")
    print(np.linalg.norm(x1_google - x1_bertfam))
    assert np.linalg.norm(x0_google - x0_bertfam) < 0.01
    assert np.linalg.norm(x1_google - x1_bertfam) < 0.01


@pytest.mark.skipif(f"{not os.environ.get('ALL_BERTFAM_TESTS')}")
def test_compare_mlm_output():
    pass


if __name__ == "__main__":
    test_compare_seq_output()
