from bertfam.layers import activations
import tensorflow as tf
import numpy as np


def test_gelu_correct_shape():

    for shape in [(1,), (2,), (2, 3), (2, 3, 4)]:
        x = np.random.rand(*shape)
        y = activations.gelu(x)
        with tf.Session() as sess:
            y = sess.run(y)
        assert y.shape == x.shape


def test_approx_gelu_correct_shape():

    for shape in [(1,), (2,), (2, 3), (2, 3, 4)]:
        x = np.random.rand(*shape)
        y = activations.approx_gelu(x)
        with tf.Session() as sess:
            y = sess.run(y)
        assert y.shape == x.shape


def test_approx_gelu_is_approx():

    x = np.random.rand(2, 3, 4)

    y_approx = activations.approx_gelu(x)
    y_exact = activations.gelu(x)

    with tf.Session() as sess:
        y_approx, y_exact = sess.run((y_approx, y_exact))

    assert tf.assert_near(y_exact, y_exact)


if __name__ == "__main__":
    test_gelu_correct_shape()
