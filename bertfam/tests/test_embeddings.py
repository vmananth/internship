from bertfam.layers.embeddings import Embeddings
import tensorflow as tf


vocab_size = 2
vocab = list(map(str, range(vocab_size)))
dim = 3
batch_size = 5
seq_len = 4
max_seq_len = 6


def test_can_build():

    e = Embeddings(vocab, dim, max_seq_len)
    e.build((batch_size, seq_len))


def test_embedding_dims():

    e = Embeddings(vocab, dim, max_seq_len)
    e.build((batch_size, seq_len))

    assert len(e.token_embeddings.weights) == 1
    assert e.token_embeddings.weights[0].shape == (vocab_size, dim)
    assert len(e.pos_embeddings.weights) == 1
    assert e.pos_embeddings.weights[0].shape == (max_seq_len, dim)
    assert len(e.seg_embeddings.weights) == 1
    assert e.seg_embeddings.weights[0].shape == (2, dim)
