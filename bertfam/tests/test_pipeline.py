"""
These tests are for testing the tokenizer + model
"""
import tensorflow as tf
from bertfam import Embeddings, Encoder, Tokenizer, BertTokenizer, BertBertTokenizer


BUILTIN_TOKENS = ["[UNK]", "[PAD]", "[SEP]", "[CLS]", "[MASK]"]


vocab_size = 7
vocab = list(map(str, range(vocab_size - len(BUILTIN_TOKENS)))) + BUILTIN_TOKENS
dim = 10
max_seq_len = 15
num_layers = 2
num_heads = 5
hidden_dim = 9


def test_model_with_tokenizers():

    embeddings = Embeddings(vocab, dim, max_seq_len)
    embeddings.build((None,))
    encoder = Encoder(num_layers, num_heads, dim, hidden_dim)
    encoder.build((None, dim))

    i = tf.keras.Input(shape=(None,))

    x = embeddings(i)
    x = encoder(x)

    model = tf.keras.Model(i, x)

    tokenizer = Tokenizer(["foo", "bar"] + BUILTIN_TOKENS)

    X = model.predict(tokenizer("foo bar"))
    assert X.shape == (1, 2, dim)
    X = model.predict(tokenizer(["foo bar", "bar foo bar"]))
    assert X.shape == (2, 3, dim)

    tokenizer = BertTokenizer(["foo", "bar"] + BUILTIN_TOKENS)

    X = model.predict(tokenizer("foo bar"))
    assert X.shape == (1, 4, dim)
    X = model.predict(tokenizer(["foo bar", "bar foo bar"]))
    assert X.shape == (2, 5, dim)

    tokenizer = BertBertTokenizer(["foo", "bar"] + BUILTIN_TOKENS)
    X = model.predict(tokenizer(("foo bar", "foo")))
    assert X.shape == (1, 6, dim)
    X = model.predict(tokenizer([("foo bar", "foo"), ("bar foo bar", "bar")]))
    assert X.shape == (2, 7, dim)


def test_model_with_tokenizers_masking():

    embeddings = Embeddings(vocab, dim, max_seq_len)
    embeddings.build((None,))
    encoder = Encoder(num_layers, num_heads, dim, hidden_dim)
    encoder.build((None, dim))

    i = tf.keras.Input(shape=(None,))
    mask = tf.keras.Input(shape=(None,))

    x = embeddings(i)
    x = encoder(x, padding_mask=mask)

    model = tf.keras.Model([i, mask], x)

    tokenizer = Tokenizer(["foo", "bar"] + BUILTIN_TOKENS, attention_mask=True)

    X = model.predict(tokenizer("foo bar"))
    assert X.shape == (1, 2, dim)
    X = model.predict(tokenizer(["foo bar", "bar foo bar"]))
    assert X.shape == (2, 3, dim)

    tokenizer = BertTokenizer(["foo", "bar"] + BUILTIN_TOKENS, attention_mask=True)

    X = model.predict(tokenizer("foo bar"))
    assert X.shape == (1, 4, dim)
    X = model.predict(tokenizer(["foo bar", "bar foo bar"]))
    assert X.shape == (2, 5, dim)

    tokenizer = BertBertTokenizer(["foo", "bar"] + BUILTIN_TOKENS, attention_mask=True)
    X = model.predict(tokenizer(("foo bar", "foo")))
    assert X.shape == (1, 6, dim)
    X = model.predict(tokenizer([("foo bar", "foo"), ("bar foo bar", "bar")]))
    assert X.shape == (2, 7, dim)


def test_model_with_tokenizers_seg_ids():

    embeddings = Embeddings(vocab, dim, max_seq_len)
    embeddings.build((None,))
    encoder = Encoder(num_layers, num_heads, dim, hidden_dim)
    encoder.build((None, dim))

    i = tf.keras.Input(shape=(None,))
    seg_ids = tf.keras.Input(shape=(None,))

    x = embeddings(i, seg_ids=seg_ids)
    x = encoder(x)

    model = tf.keras.Model([i, seg_ids], x)

    tokenizer = Tokenizer(["foo", "bar"] + BUILTIN_TOKENS, seg_ids=True)

    X = model.predict(tokenizer("foo bar"))
    assert X.shape == (1, 2, dim)
    X = model.predict(tokenizer(["foo bar", "bar foo bar"]))
    assert X.shape == (2, 3, dim)

    tokenizer = BertTokenizer(["foo", "bar"] + BUILTIN_TOKENS, seg_ids=True)

    X = model.predict(tokenizer("foo bar"))
    assert X.shape == (1, 4, dim)
    X = model.predict(tokenizer(["foo bar", "bar foo bar"]))
    assert X.shape == (2, 5, dim)

    tokenizer = BertBertTokenizer(["foo", "bar"] + BUILTIN_TOKENS, seg_ids=True)
    X = model.predict(tokenizer(("foo bar", "foo")))
    assert X.shape == (1, 6, dim)
    X = model.predict(tokenizer([("foo bar", "foo"), ("bar foo bar", "bar")]))
    assert X.shape == (2, 7, dim)


def test_model_with_tokenizers_masking_seg_ids():

    embeddings = Embeddings(vocab, dim, max_seq_len)
    embeddings.build((None,))
    encoder = Encoder(num_layers, num_heads, dim, hidden_dim)
    encoder.build((None, dim))

    i = tf.keras.Input(shape=(None,))
    seg_ids = tf.keras.Input(shape=(None,))
    mask = tf.keras.Input(shape=(None,))

    x = embeddings(i, seg_ids=seg_ids)
    x = encoder(x, padding_mask=mask)

    model = tf.keras.Model([i, mask, seg_ids], x)

    tokenizer = Tokenizer(
        ["foo", "bar"] + BUILTIN_TOKENS, seg_ids=True, attention_mask=True
    )

    X = model.predict(tokenizer("foo bar"))
    assert X.shape == (1, 2, dim)
    X = model.predict(tokenizer(["foo bar", "bar foo bar"]))
    assert X.shape == (2, 3, dim)

    tokenizer = BertTokenizer(
        ["foo", "bar"] + BUILTIN_TOKENS, seg_ids=True, attention_mask=True
    )

    X = model.predict(tokenizer("foo bar"))
    assert X.shape == (1, 4, dim)
    X = model.predict(tokenizer(["foo bar", "bar foo bar"]))
    assert X.shape == (2, 5, dim)

    tokenizer = BertBertTokenizer(
        ["foo", "bar"] + BUILTIN_TOKENS, seg_ids=True, attention_mask=True
    )
    X = model.predict(tokenizer(("foo bar", "foo")))
    assert X.shape == (1, 6, dim)
    X = model.predict(tokenizer([("foo bar", "foo"), ("bar foo bar", "bar")]))
    assert X.shape == (2, 7, dim)
