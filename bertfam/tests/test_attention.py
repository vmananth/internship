import tensorflow as tf
import pytest
from bertfam.layers import attention
import numpy as np

batch_size = 2
seq_len = 3
head_dim = 5
num_heads = 7
dim = head_dim * num_heads


def test_create_mha():

    attention.MultiHeadAttention(2, 4)
    with pytest.raises(ValueError):
        attention.MultiHeadAttention(2, 3)


def test_split_heads_shapes():

    X = tf.random_normal((batch_size, seq_len, dim))
    mha = attention.MultiHeadAttention(num_heads, dim)
    with tf.Session() as sess:
        Y = sess.run(mha.split_heads(X))
    assert Y.shape == (batch_size, num_heads, seq_len, head_dim)


def test_combine_heads_shapes():

    X = tf.random_normal((batch_size, num_heads, seq_len, head_dim))
    mha = attention.MultiHeadAttention(num_heads, dim)
    with tf.Session() as sess:
        Y = sess.run(mha.combine_heads(X))
    assert Y.shape == (batch_size, seq_len, dim)


def test_split_and_combine_heads_are_inverses():

    X = tf.random_normal((batch_size, seq_len, dim))
    mha = attention.MultiHeadAttention(num_heads, dim)
    with tf.Session() as sess:
        Y = sess.run(mha.combine_heads(mha.split_heads(X)))

    tf.assert_equal(X, Y)

    X = tf.random_normal((batch_size, num_heads, seq_len, head_dim))
    with tf.Session() as sess:
        Y = sess.run(mha.split_heads(mha.combine_heads(X)))

    tf.assert_equal(X, Y)


def test_mha_call_no_mask_shapes():

    mha = attention.MultiHeadAttention(num_heads, dim)

    input_shapes = [tf.TensorShape((seq_len, dim))] * 3

    mha.build(input_shapes)

    Q = tf.random_normal((batch_size, seq_len, dim))
    K = tf.random_normal((batch_size, seq_len, dim))
    V = tf.random_normal((batch_size, seq_len, dim))

    X, a = mha.call([Q, K, V])

    assert X.shape == (batch_size, seq_len, dim)
    assert a.shape == (batch_size, num_heads, seq_len, seq_len)


def test_mha_model_no_mask_shapes():

    i1 = tf.keras.Input(shape=(seq_len, dim))
    i2 = tf.keras.Input(shape=(seq_len, dim))
    i3 = tf.keras.Input(shape=(seq_len, dim))

    mha = attention.MultiHeadAttention(num_heads, dim)((i1, i2, i3))

    model = tf.keras.Model([i1, i2, i3], mha)

    Q = tf.random_normal((batch_size, seq_len, dim))
    K = tf.random_normal((batch_size, seq_len, dim))
    V = tf.random_normal((batch_size, seq_len, dim))

    X, a = model.predict((Q, K, V), steps=1)

    assert X.shape == (batch_size, seq_len, dim)
    assert a.shape == (batch_size, num_heads, seq_len, seq_len)


def test_scaled_dot_product_attention():

    Q = np.array([[np.sqrt(2), 0], [0, 0], [0, 0]], dtype=np.float32)
    K = np.array([[1, 0], [0, 0], [0, 0]], dtype=np.float32)
    V = np.array([[1, 2], [3, 4], [5, 6]], dtype=np.float32)

    X, a = attention.scaled_dot_product_attention(Q, K, V)

    with tf.Session() as sess:
        X, a = sess.run((X, a))

    expected = np.array(
        [[(np.e + 8) / (np.e + 2), (2 * np.e + 10) / (np.e + 2)], [3, 4], [3, 4]]
    )

    assert np.allclose(X, expected)
