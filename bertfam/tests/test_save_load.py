from bertfam import BertTokenizer, load_model, load_pretrained_tf_model
import pytest
import random
import string
import os
import numpy as np
import tensorflow as tf


@pytest.fixture
def tmpfile():
    filename = "".join(random.choice(string.ascii_lowercase) for _ in range(16))
    filename = os.path.join("/tmp", filename)
    yield filename
    if os.path.exists(filename):
        os.remove(filename)


def random_data(num_examples=100, max_seq_len=25, vocab_size=100):
    tokenids_ = [
        np.random.randint(1, vocab_size - 1, size=(np.random.randint(0, max_seq_len)))
        for _ in range(num_examples)
    ]
    tokenids_ = tf.keras.preprocessing.sequence.pad_sequences(
        tokenids_, max_seq_len, padding="post"
    )
    labels = np.random.randint(0, 1, size=num_examples)
    labels = tf.keras.utils.to_categorical(labels, num_classes=2)
    mask = tokenids_ != 0
    return (tokenids_, mask), labels


@pytest.mark.skipif(f"{not os.environ.get('ALL_BERTFAM_TESTS')}")
def test_save_load(tmpfile):

    model = load_pretrained_tf_model()
    tokenizer = BertTokenizer.from_model(model)

    X = tokenizer(["hello world", "how are you today?"])

    y1 = model.predict(X)

    model.save(tmpfile)
    model = load_model(tmpfile)

    y2 = model.predict(X)

    assert np.allclose(y1, y2)


@pytest.mark.skipif(f"{not os.environ.get('ALL_BERTFAM_TESTS')}")
def test_save_load_with_seg_ids(tmpfile):

    model = load_pretrained_tf_model(seg_ids=True)
    tokenizer = BertTokenizer.from_model(model)

    X = tokenizer(["hello world", "how are you today?"])

    y1 = model.predict(X)

    model.save(tmpfile)
    model = load_model(tmpfile)

    y2 = model.predict(X)

    assert np.allclose(y1, y2)
