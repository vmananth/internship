from bertfam import Tokenizer, BertTokenizer, BertBertTokenizer
import pytest


BUILTIN_TOKENS = ["[UNK]", "[PAD]", "[SEP]", "[CLS]", "[MASK]"]


def test_create_tokenizer():

    Tokenizer(["foo"] + BUILTIN_TOKENS)

    with pytest.raises(KeyError):
        Tokenizer(["ab"])


def test_call_one():

    tokenizer = Tokenizer(["foo", "bar", "##s"] + BUILTIN_TOKENS)

    X = tokenizer("foo bar")

    assert X.shape == (1, 2)
    assert (X == [0, 1]).all()

    X = tokenizer("foo bars")

    assert X.shape == (1, 3)
    assert (X == [0, 1, 2]).all()


def test_call_many_same_len():

    tokenizer = Tokenizer(["foo", "bar", "##s"] + BUILTIN_TOKENS)

    X = tokenizer(["foo bar foo", "bar foo x"])

    assert X.shape == (2, 3)
    assert (X == [[0, 1, 0], [1, 0, 3]]).all()


def test_call_many_diff_len():

    tokenizer = Tokenizer(["foo", "bar", "##s"] + BUILTIN_TOKENS)

    X = tokenizer(["foo bar foo", "bar foo"])

    assert X.shape == (2, 3)
    assert (X == [[0, 1, 0], [1, 0, 4]]).all()


def test_number_sign_special_case():
    # before bug fix this raised recursion error

    tokenizer = Tokenizer(["foo", "bar", "#"] + BUILTIN_TOKENS)
    tokenizer("foo foozxcv")


def test_punctuation():

    tokenizer = Tokenizer(
        ["foo", "bar", "!", "is", "a", "woop", ".", ",", ":"] + BUILTIN_TOKENS
    )

    X = tokenizer("foo bar!")

    assert X.shape == (1, 3)
    assert (X == [0, 1, 2]).all()

    X = tokenizer("foo, is a: bar.  Woop!")
    assert X.shape == (1, 9)
    assert (X == [0, 7, 3, 4, 8, 1, 6, 5, 2]).all()


def test_masking():

    tokenizer = Tokenizer(["foo", "bar"] + BUILTIN_TOKENS, attention_mask=True)

    Xs = tokenizer(["foo bar", "foo foo bar"])

    assert isinstance(Xs, tuple)
    assert len(Xs) == 2

    X, X_mask = Xs

    assert X_mask.shape == (2, 3)
    assert (X_mask == [[1, 1, 0], [1, 1, 1]]).all()


def test_seg_ids():
    tokenizer = Tokenizer(["foo", "bar"] + BUILTIN_TOKENS, seg_ids=True)

    Xs = tokenizer(["foo bar", "foo foo bar"])

    assert isinstance(Xs, tuple)
    assert len(Xs) == 2

    X, X_seg = Xs

    assert X_seg.shape == (2, 3)
    assert (X_seg == [[0, 0, 0], [0, 0, 0]]).all()


def test_masking_and_seg_ids():
    tokenizer = Tokenizer(
        ["foo", "bar"] + BUILTIN_TOKENS, attention_mask=True, seg_ids=True
    )

    Xs = tokenizer(["foo bar", "foo foo bar"])

    assert isinstance(Xs, tuple)
    assert len(Xs) == 3

    X, X_mask, X_seg = Xs

    assert X_mask.shape == (2, 3)
    assert (X_mask == [[1, 1, 0], [1, 1, 1]]).all()

    assert X_seg.shape == (2, 3)
    assert (X_seg == [[0, 0, 0], [0, 0, 0]]).all()


def test_bert_masking_and_seg_ids():

    tokenizer = BertTokenizer(
        ["foo", "bar"] + BUILTIN_TOKENS, attention_mask=True, seg_ids=True
    )

    Xs = tokenizer(["foo bar", "foo foo bar"])

    assert isinstance(Xs, tuple)
    assert len(Xs) == 3

    X, X_mask, X_seg = Xs

    assert X_mask.shape == (2, 5)
    assert (X_mask == [[1, 1, 1, 1, 0], [1, 1, 1, 1, 1]]).all()

    assert X_seg.shape == (2, 5)
    assert (X_seg == [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]).all()


def test_bertbert_masking_and_seg_ids():

    tokenizer = BertBertTokenizer(
        ["foo", "bar"] + BUILTIN_TOKENS, attention_mask=True, seg_ids=True
    )

    Xs = tokenizer([("foo bar", "foo"), ("foo foo bar", "bar bar")])

    assert isinstance(Xs, tuple)
    assert len(Xs) == 3

    X, X_mask, X_seg = Xs

    assert X_mask.shape == (2, 8)
    assert (X_mask == [[1, 1, 1, 1, 1, 1, 0, 0], [1, 1, 1, 1, 1, 1, 1, 1]]).all()

    assert X_seg.shape == (2, 8)
    assert (X_seg == [[0, 0, 0, 0, 1, 1, 1, 1], [0, 0, 0, 0, 0, 1, 1, 1]]).all()


def test_special_tokens():

    tokenizer = Tokenizer(["foo", "bar"] + BUILTIN_TOKENS)
    assert tokenizer.tokenize("foo [PAD] bar") == ["foo", "[PAD]", "bar"]

    tokenizer = BertTokenizer(["foo", "bar"] + BUILTIN_TOKENS)
    assert tokenizer.tokenize("foo [PAD] bar") == ["foo", "[PAD]", "bar"]

    tokenizer = BertBertTokenizer(["foo", "bar"] + BUILTIN_TOKENS)
    assert tokenizer.tokenize("foo [PAD] bar") == ["foo", "[PAD]", "bar"]


def test_truncates_long_input():

    tokenizer = BertTokenizer(["foo", "bar"] + BUILTIN_TOKENS, max_seq_len=12)
    X = tokenizer("foo " * 24)

    tokenizer = BertBertTokenizer(["foo", "bar"] + BUILTIN_TOKENS, max_seq_len=12)
    X = tokenizer(("foo " * 24, "bar " * 24))
