from typing import List, Iterator, Union, Tuple
import numpy as np
import re
import json
import itertools
from bertfam.model import (
    DEFAULT_TF_MODEL,
    download_pretrained_tf_model,
    TOKENS_INPUT,
    SEG_INPUT,
    MASK_INPUT,
)
from bertfam.utils import open
from bertfam import Embeddings
import tensorflow as tf
import os
import warnings
import hashlib


SPECIAL_TOKEN_REGEX = re.compile(r"\[[A-Z]+\]")  # e.g. matches "[PAD]"
PUNC_REGEX = re.compile(
    r"(\[[A-Z]+\]|[!\"#$%&'()*+,\-./:;<=>?@\[\\\]^_`{|}~¡§«¶·»¿;·՚՛՜՝՞՟։])"
)
WHITESPACE_REGEX = re.compile(r"\s+")


# TODO learn a new tokenizer from text?


class Tokenizer:
    def __init__(
        self,
        vocab: Iterator[str],
        attention_mask: bool = False,
        seg_ids: bool = False,
        lowercase: bool = True,
        max_seq_len: int = 512,
        pad: bool = False,
        unk_token: str = "[UNK]",
        pad_token: str = "[PAD]",
        cls_token: str = "[CLS]",
        sep_token: str = "[SEP]",
        mask_token: str = "[MASK]",
    ):
        self.lowercase = lowercase
        self.vocab = tuple(vocab)
        self.token2id = {token: i for i, token in enumerate(self.vocab)}
        self.attention_mask = attention_mask
        self.seg_ids = seg_ids
        self.max_seq_len = max_seq_len
        self.pad = pad
        self.unk_token = unk_token
        self.pad_token = pad_token
        self.cls_token = cls_token
        self.sep_token = sep_token
        self.mask_token = mask_token
        missing_tokens = []
        for token in self.builtin_tokens:
            if token not in self.token2id:
                missing_tokens.append(token)
        if missing_tokens:
            raise KeyError(f"vocab must contain: {', '.join(missing_tokens)}")

    def to_base_tokenizer(self, **kwargs):
        config = self.get_config()
        config.update(kwargs)
        return Tokenizer(**config)

    def to_bert_tokenizer(self, **kwargs):
        config = self.get_config()
        config.update(kwargs)
        return BertTokenizer(**config)

    def to_bertbert_tokenizer(self, **kwargs):
        config = self.get_config()
        config.update(kwargs)
        return BertBertTokenizer(**config)

    @property
    def uid(self) -> str:
        config = self.get_config()
        del config["attention_mask"]
        del config["seg_ids"]
        del config["pad"]
        raw = json.dumps(config, sort_keys=True).encode()
        return hashlib.blake2s(raw, digest_size=4).hexdigest()

    @classmethod
    def from_textfile(cls, filename: str, **kwargs):
        with open(filename) as f:
            return cls((token.strip() for token in f), **kwargs)

    @classmethod
    def from_model(cls, model: tf.keras.Model, **kwargs):
        try:
            embeddings = model.get_layer("embeddings")
        except ValueError:
            raise ValueError("No 'embeddings' layer, model must be a bertfam model")
        if TOKENS_INPUT not in model.input_names:
            warnings.warn(
                f"No '{TOKENS_INPUT}' input found. 'model' should be a bertfam model (with named inputs) to use {cls.__name__}.from_model",
                UserWarning,
            )
        if MASK_INPUT in model.input_names:
            kwargs["attention_mask"] = True
        if SEG_INPUT in model.input_names:
            kwargs["seg_ids"] = True
        return cls.from_embeddings(embeddings, **kwargs)

    @classmethod
    def from_embeddings(cls, embeddings: Embeddings, **kwargs):
        return cls(embeddings.vocab, **kwargs)

    @classmethod
    def from_json(cls, filename: str, **kwargs):
        with open(filename) as f:
            args = json.load(f)
        args.update(kwargs)
        return cls(**args)

    @classmethod
    def from_tf_model(cls, model_name: str = DEFAULT_TF_MODEL, **kwargs):
        warnings.warn(
            f"{cls.__name__}.from_tf_model is deprecated. Use {cls.__name__}.from_pretrained_tf_model",
            DeprecationWarning,
        )
        return cls.from_pretrained_tf_model(model_name, **kwargs)

    @classmethod
    def from_pretrained_tf_model(cls, model_name: str = DEFAULT_TF_MODEL, **kwargs):
        model_path = download_pretrained_tf_model(model_name)
        vocab_file = os.path.join(model_path, "vocab.txt")
        return cls.from_textfile(vocab_file, **kwargs)

    def to_json(self, filename: str):
        config = self.get_config()
        with open(filename, "w") as f:
            json.dump(config, f)

    def get_config(self):
        config = {
            "vocab": self.vocab,
            "attention_mask": self.attention_mask,
            "seg_ids": self.seg_ids,
            "max_seq_len": self.max_seq_len,
            "pad": self.pad,
            "lowercase": self.lowercase,
            "unk_token": self.unk_token,
            "pad_token": self.pad_token,
            "cls_token": self.cls_token,
            "sep_token": self.sep_token,
        }
        return config

    def __call__(
        self, text: Union[Iterator[str], str]
    ) -> Union[np.ndarray, Tuple[np.ndarray, ...]]:
        if isinstance(text, str):
            return self([text])
        seqs = self.tokenize_texts(text)
        seqs = [self.tokens_to_ids(tokens) for tokens in seqs]
        batch_size = len(seqs)
        if self.pad:
            seq_len = self.max_seq_len
        else:
            seq_len = min(max(map(len, seqs)), self.max_seq_len)
        X = (
            np.ones((batch_size, seq_len), dtype=np.int32)
            * self.token2id[self.pad_token]
        )
        for i, x in enumerate(seqs):
            X[i, : len(x)] = x

        X = (X,)

        if self.attention_mask:
            X_mask = np.logical_not(X[0] == self.token2id[self.pad_token]).astype(
                np.int32
            )
            X += (X_mask,)

        if self.seg_ids:
            X_seg = np.zeros_like(X[0])
            X += (X_seg,)

        if len(X) == 1:
            X = X[0]

        return X

    def tokens_to_ids(self, tokens: List[str]) -> np.ndarray:
        return np.fromiter((self.token2id[token] for token in tokens), dtype=np.int32)[
            : self.max_seq_len
        ]

    def tokenize(
        self, text: Union[Iterator[str], str]
    ) -> Union[List[str], List[List[str]]]:
        if isinstance(text, str):
            return self.tokenize_text(text)
        else:
            return self.tokenize_texts(text)

    def tokenize_text(self, text: str) -> List[str]:
        text = text.strip()
        text = PUNC_REGEX.sub(r" \g<1> ", text)  # foo, bar! -> foo , bar !
        text = WHITESPACE_REGEX.split(text)
        tokens = []
        for token in text:
            if self.lowercase and not SPECIAL_TOKEN_REGEX.match(token):
                token = token.lower()
            tokens.extend(self.subtokenize_token(token))
        return tokens

    def subtokenize_token(self, token: str) -> List[str]:
        if not token:
            return []
        if token in self.token2id:
            return [token]
        for i in range(-1, -len(token), -1):
            if token[:i] == "##":
                return [self.unk_token]
            if token[:i] in self.token2id:
                return [token[:i]] + self.subtokenize_token("##" + token[i:])
        return [self.unk_token]

    def tokenize_texts(self, texts: Iterator[str]) -> List[List[str]]:
        texts = [self.tokenize_text(text) for text in texts]
        return texts

    @property
    def builtin_tokens(self) -> List[str]:
        return [
            self.cls_token,
            self.sep_token,
            self.pad_token,
            self.unk_token,
            self.mask_token,
        ]

    def __contains__(self, item) -> bool:
        return item in self.token2id

    def __getitem__(self, item) -> Union[str, int]:
        if isinstance(item, str):
            return self.token2id[item]
        elif isinstance(item, (np.integer, int)):
            return self.vocab[item]
        else:
            raise KeyError(
                f"Can't call __getitem__ on item of type {type(item)}, must be str or int."
            )

    def detokenize(self, tokens: List[str]) -> str:
        text = " ".join(tokens)
        text = text.replace(" ##", "")
        return text


class BertTokenizer(Tokenizer):
    def tokens_to_ids(self, tokens: List[str]) -> np.ndarray:
        tokens = [self.cls_token] + tokens + [self.sep_token]
        return super(BertTokenizer, self).tokens_to_ids(tokens)


class BertBertTokenizer(BertTokenizer):
    def tokens_to_ids(self, tokens: Tuple[List[str], List[str]]) -> np.ndarray:
        first, second = tokens
        tokens = first + [self.sep_token] + second
        return super(BertBertTokenizer, self).tokens_to_ids(tokens)

    def __call__(
        self, text: Union[Tuple[str, str], Iterator[Tuple[str, str]]]
    ) -> Union[np.ndarray, Tuple[np.ndarray, ...]]:

        text_iter = iter(text)

        first = next(text_iter)

        if isinstance(first, str):
            second = next(text_iter)
            return self([(first, second)])

        text_iter = itertools.chain([first], text_iter)

        seqs = []

        first_seg_lengths = []

        for first, second in text_iter:
            first_tokens = self.tokenize_text(first)
            second_tokens = self.tokenize_text(second)

            first_seg_lengths.append(
                len(first_tokens) + 2
            )  # + 2 to account for [CLS] and [SEP]

            seqs.append(self.tokens_to_ids((first_tokens, second_tokens)))

        batch_size = len(seqs)
        if self.pad:
            seq_len = self.max_seq_len
        else:
            seq_len = min(max(map(len, seqs)), self.max_seq_len)

        X = (
            np.ones((batch_size, seq_len), dtype=np.int32)
            * self.token2id[self.pad_token]
        )

        X = (X,)

        if self.seg_ids:
            X_seg = np.zeros_like(X[0])
            for i, x in enumerate(seqs):
                X[0][i, : len(x)] = x[:seq_len]
                X_seg[i, first_seg_lengths[i] :] = 1
        else:
            X_seg = None
            for i, x in enumerate(seqs):
                X[0][i, : len(x)] = x[:seq_len]

        if self.attention_mask:
            X_mask = 1 - (X[0] == self.token2id[self.pad_token]).astype(np.int32)
            X += (X_mask,)

        if self.seg_ids:
            X += (X_seg,)

        if len(X) == 1:
            X = X[0]

        return X
