import shutil
import warnings
import zipfile
from typing import Optional, Iterable, List, Union
from bertfam import Encoder, Embeddings, MLM, constants
from bertfam.layers.mlm import MLM_SIM, MLM_FC, MLM_NORM
from bertfam.utils import open, get_custom_objects, download_file
import tensorflow as tf
import os
import json
import numpy as np
import logging
import h5py
from tensorflow.python.keras.engine import saving


logger = logging.getLogger(__name__)

DEFAULT_TF_MODEL = "bert-base-uncased"


# layer names, for consistency throughout bertfam package
TOKENS_INPUT = "token_ids"
MASK_INPUT = "attention_mask"
SEG_INPUT = "seg_ids"

EMBEDDINGS = "embeddings"
ENCODER = "seq_output"

NSP_OUTPUT = "nsp_output"
POOLED_OUTPUT = "pooled_output"
MLM_OUTPUT = "mlm_output"


def load_tf_config(model_name: str = DEFAULT_TF_MODEL) -> dict:

    model_path = download_pretrained_tf_model(model_name)
    config_json = os.path.join(model_path, "bert_config.json")

    with open(config_json) as f:
        bert_config = json.load(f)

    config = {
        "dim": bert_config["hidden_size"],
        "max_seq_len": bert_config["max_position_embeddings"],
        "num_layers": bert_config["num_layers"],
        "hidden_dim": bert_config["intermediate_size"],
    }

    return config


def load_pretrained_tf_model(
    model_name: str = DEFAULT_TF_MODEL,
    max_seq_len=None,
    vocab: Union[Iterable[str], str] = None,
    **kwargs,
) -> tf.keras.Model:

    model_path = download_pretrained_tf_model(model_name)
    config_json = os.path.join(model_path, "bert_config.json")
    checkpoint_file = os.path.join(model_path, "bert_model.ckpt")
    vocab_file = os.path.join(model_path, "vocab.txt")

    with open(config_json) as f:
        bert_config = json.load(f)

    if isinstance(vocab, str):
        vocab_file = vocab
        vocab = None

    if vocab is None:
        with open(vocab_file) as f:
            vocab = [line.strip() for line in f]
    else:
        vocab = list(vocab)

    vocab_size = bert_config["vocab_size"]
    if len(vocab) != vocab_size:
        raise ValueError(
            "vocab_size in config not same size as vocab loaded from vocab.txt"
        )

    dim = bert_config["hidden_size"]
    if max_seq_len is not None:
        if max_seq_len > bert_config["max_position_embeddings"]:
            raise ValueError(
                f"max_seq_len cannot be larger than it was in pretrained model ({bert_config['max_position_embeddings']})"
            )
    else:
        max_seq_len = bert_config["max_position_embeddings"]
    num_layers = kwargs.get("num_layers")
    if num_layers is not None:
        num_layers = list(range(bert_config["num_hidden_layers"]))[num_layers]
    else:
        num_layers = bert_config["num_hidden_layers"]
    num_heads = bert_config["num_attention_heads"]
    hidden_dim = bert_config["intermediate_size"]

    model = build_bert(
        vocab=vocab,
        num_layers=num_layers,
        num_heads=num_heads,
        dim=dim,
        hidden_dim=hidden_dim,
        max_seq_len=max_seq_len,
        **kwargs,
    )

    return set_pretrained_tf_weights(model, checkpoint_file)


def set_pretrained_weights(
    model: tf.keras.Model, weights_file: str, ignore_pos=False
) -> tf.keras.Model:
    """
    Modified version of model.load_weights / saving.load_weights_from_hdf5_group
    """

    if ignore_pos:
        name2model_weights = {w.name: w for w in model.weights if "/pos/" not in w.name}
    else:
        name2model_weights = {w.name: w for w in model.weights}
    name2weight_values = {}

    with h5py.File(weights_file, "r") as f:

        layer_names = saving.load_attributes_from_hdf5_group(f, "layer_names")

        for name in layer_names:
            g = f[name]
            weight_names = saving.load_attributes_from_hdf5_group(g, "weight_names")
            for weight_name in weight_names:
                name2weight_values[weight_name] = np.asarray(g[weight_name])

    weight_value_tuples = []

    missing = []

    for name in name2model_weights:
        if name in name2weight_values:
            weight_value_tuples.append(
                (name2model_weights[name], name2weight_values[name])
            )
        else:
            missing.append(name)

    if missing:
        logger.warning(
            f"warning - found the following weights in your model but not in the pretrained weights: {', '.join(missing)}"
        )

    tf.keras.backend.batch_set_value(weight_value_tuples)

    return model


def set_pretrained_tf_weights(
    model: tf.keras.Model, checkpoint_file: str
) -> tf.keras.Model:

    layers = set(get_layer_names(model))

    def load(name: str) -> np.array:
        return tf.train.load_variable(checkpoint_file, name)

    if EMBEDDINGS in layers:
        logger.debug("setting embedding weights")
        embeddings = model.get_layer(EMBEDDINGS)

        max_seq_len = embeddings.pos_embeddings.input_dim

        embeddings.token_embeddings.set_weights(
            [load("bert/embeddings/word_embeddings")]
        )
        embeddings.pos_embeddings.set_weights(
            [load("bert/embeddings/position_embeddings")[:max_seq_len, :]]
        )
        embeddings.seg_embeddings.set_weights(
            [load("bert/embeddings/token_type_embeddings")]
        )

        embeddings.norm.set_weights(
            [
                load("bert/embeddings/LayerNorm/gamma"),
                load("bert/embeddings/LayerNorm/beta"),
            ]
        )

    if ENCODER in layers:
        logger.debug("setting encoder weights")
        encoder = model.get_layer(ENCODER)

        for i, layer in enumerate(encoder.layers):
            layer.set_weights(
                [
                    load(f"bert/encoder/layer_{i}/attention/self/query/kernel"),
                    load(f"bert/encoder/layer_{i}/attention/self/query/bias"),
                    load(f"bert/encoder/layer_{i}/attention/self/key/kernel"),
                    load(f"bert/encoder/layer_{i}/attention/self/key/bias"),
                    load(f"bert/encoder/layer_{i}/attention/self/value/kernel"),
                    load(f"bert/encoder/layer_{i}/attention/self/value/bias"),
                    load(f"bert/encoder/layer_{i}/attention/output/dense/kernel"),
                    load(f"bert/encoder/layer_{i}/attention/output/dense/bias"),
                    load(f"bert/encoder/layer_{i}/attention/output/LayerNorm/gamma"),
                    load(f"bert/encoder/layer_{i}/attention/output/LayerNorm/beta"),
                    load(f"bert/encoder/layer_{i}/intermediate/dense/kernel"),
                    load(f"bert/encoder/layer_{i}/intermediate/dense/bias"),
                    load(f"bert/encoder/layer_{i}/output/dense/kernel"),
                    load(f"bert/encoder/layer_{i}/output/dense/bias"),
                    load(f"bert/encoder/layer_{i}/output/LayerNorm/gamma"),
                    load(f"bert/encoder/layer_{i}/output/LayerNorm/beta"),
                ]
            )

    if POOLED_OUTPUT in layers:

        logger.debug("setting pooling weights")

        pooler = model.get_layer("pooled_output")
        pooler.set_weights(
            [load("bert/pooler/dense/kernel"), load("bert/pooler/dense/bias")]
        )

    if MLM_OUTPUT in layers:

        logger.debug("setting mlm weights")

        model.get_layer(MLM_OUTPUT).set_weights(
            [
                load("cls/predictions/output_bias"),
                load("cls/predictions/transform/dense/kernel"),
                load("cls/predictions/transform/dense/bias"),
                load("cls/predictions/transform/LayerNorm/gamma"),
                load("cls/predictions/transform/LayerNorm/beta"),
            ]
        )

    if NSP_OUTPUT in layers:

        nsp = model.get_layer(NSP_OUTPUT)
        nsp.set_weights(
            [
                np.transpose(load("cls/seq_relationship/output_weights")),
                load("cls/seq_relationship/output_bias"),
            ]
        )

    return model


def build_bert(
    vocab: Iterable[str],
    num_layers: int,
    num_heads: int,
    dim: int,
    hidden_dim: int,
    max_seq_len: int,
    attention_mask=True,
    seg_ids=False,
    seq_output=True,
    pooled_output=False,
    mlm_output=False,
    nsp_output=False,
    pretraining=False,
    optimizer="adam",
):

    if pretraining:
        seq_output = False
        pooled_output = False
        mlm_output = False
        attention_mask = True
        nsp_output = False
        seg_ids = True

    if not any([seq_output, pooled_output, mlm_output, nsp_output, pretraining]):
        raise ValueError(
            f"At least one of seq_output (default), pooled_output, mlm_output, nsp_output, pretraining must be True"
        )

    vocab = list(vocab)

    embeddings = Embeddings(vocab, dim, max_seq_len, name=EMBEDDINGS)
    embeddings.build((None,))

    encoder = Encoder(num_layers, num_heads, dim, hidden_dim, name=ENCODER)
    encoder.build((None, dim))

    i = tf.keras.Input(shape=(None,), name=TOKENS_INPUT, dtype=tf.float32)
    inputs = [i]

    mask_input = None

    if attention_mask:
        mask_input = tf.keras.Input(shape=(None,), name=MASK_INPUT, dtype=tf.float32)
        inputs.append(mask_input)

    if seg_ids:
        seg_ids_input = tf.keras.Input(shape=(None,), name=SEG_INPUT, dtype=tf.float32)
        inputs.append(seg_ids_input)
        x = embeddings([i, seg_ids_input])
    else:
        x = embeddings(i)

    if attention_mask:
        x = encoder([x, mask_input])
    else:
        x = encoder([x])

    outputs = []
    losses = {}

    if seq_output:
        outputs.append(x)

    if pooled_output or pretraining or nsp_output:

        with tf.variable_scope("pooler"):
            pooler = tf.keras.layers.Dense(dim, activation="tanh", name=POOLED_OUTPUT)
            pooler.build(encoder.output_shape)
        pooled = tf.keras.layers.Lambda(lambda _x: _x[:, 0, :], name="select_cls")(x)
        pooled = pooler(pooled)

        if pooled_output:
            outputs.append(pooled)

    if mlm_output or pretraining:

        mlm = MLM(len(vocab), dim, name=MLM_OUTPUT, trainable=True)
        mlm.build([x.shape, embeddings.token_embeddings.weights[0].shape])

        mlm_out = mlm([x, embeddings.token_embeddings.embeddings])

        if pretraining:
            masked_labels = tf.keras.Input(shape=(None,), name="masked_labels")
            mlm_pos = tf.keras.Input(shape=(None,), name="mlm_pos")
            mlm_mask = tf.keras.Input(shape=(None,), name="mlm_mask")

            def _masked_ml_loss(ys):
                y_true, y_pred, y_pos, y_mask = ys
                y_pos = tf.cast(y_pos, tf.int32)
                y_pred = tf.batch_gather(y_pred, y_pos)
                loss = tf.where(
                    tf.cast(y_mask, tf.bool),
                    tf.keras.losses.sparse_categorical_crossentropy(y_true, y_pred),
                    tf.zeros_like(y_mask),
                )
                loss = tf.reduce_sum(loss, keep_dims=True) / (
                    tf.reduce_sum(y_mask) + 1e-7
                )
                return loss

            mlm_loss = tf.keras.layers.Lambda(_masked_ml_loss, name="mlm")(
                [masked_labels, mlm_out, mlm_pos, mlm_mask]
            )

            inputs.append(masked_labels)
            inputs.append(mlm_pos)
            inputs.append(mlm_mask)

            losses["mlm"] = lambda _, y: y  # bypass
            outputs.append(mlm_loss)

        if mlm_output:
            outputs.append(mlm_out)

    if nsp_output or pretraining:

        with tf.variable_scope("nsp"):
            nsp = tf.keras.layers.Dense(2, activation="softmax", name=NSP_OUTPUT)
            nsp.build(pooled.shape)

            nsp_out = nsp(pooled)

        if pretraining:
            nsp_labels = tf.keras.Input(shape=(None,), name="nsp_labels")

            def _nsp_loss(ys):
                y_true, y_pred = ys
                return tf.keras.losses.categorical_crossentropy(y_true, y_pred)

            nsp_loss = tf.keras.layers.Lambda(_nsp_loss, name="nsp")(
                [nsp_labels, nsp_out]
            )

            losses["nsp"] = lambda _, y: y

            inputs.append(nsp_labels)
            outputs.append(nsp_loss)

        if nsp_output:
            outputs.append(nsp_out)

    if len(inputs) == 1:
        inputs = inputs[0]

    if len(outputs) == 1:
        outputs = outputs[0]

    model = tf.keras.Model(inputs, outputs)

    if losses:
        model.compile(optimizer, losses)

    return model


def download_pretrained_tf_model(model_name: str) -> str:
    """downloads and extracts if it doesn't exist"""
    if model_name not in PRETRAINED_TF_MODEL_URLS:
        raise ValueError(
            f"Unknown tensorflow pretrained model {model_name}, must be one of {', '.join(PRETRAINED_TF_MODEL_URLS.keys())}"
        )
    url = PRETRAINED_TF_MODEL_URLS[model_name]

    extractpath = os.path.join(constants.MODELS_DIR, model_name)
    zipfilename = extractpath + ".zip"

    files = [
        "vocab.txt",
        "bert_config.json",
        "bert_model.ckpt.index",
        "bert_model.ckpt.meta",
    ]

    if all(os.path.exists(os.path.join(extractpath, f)) for f in files):
        return extractpath

    zipfilename = download_file(url, zipfilename)

    if os.path.exists(extractpath):
        if os.path.isdir(extractpath):
            shutil.rmtree(extractpath)
        else:
            os.remove(extractpath)

    with zipfile.ZipFile(zipfilename) as zf:
        logging.info(f"extracting {zipfilename} to {extractpath}")
        for info in zf.infolist():
            info.filename = os.path.split(info.filename)[-1]
            if info.filename:
                zf.extract(info, extractpath)

    os.remove(zipfilename)

    return extractpath


PRETRAINED_TF_MODEL_URLS = {
    "bert-base-uncased": "https://storage.googleapis.com/bert_models/2018_10_18/uncased_L-12_H-768_A-12.zip",
    "bert-large-uncased": "https://storage.googleapis.com/bert_models/2018_10_18/uncased_L-24_H-1024_A-16.zip",
    "bert-base-cased": "https://storage.googleapis.com/bert_models/2018_10_18/cased_L-12_H-768_A-12.zip",
    "bert-large-cased": "https://storage.googleapis.com/bert_models/2018_10_18/cased_L-24_H-1024_A-16.zip",
}


def load_tf_model(*args, **kwargs):
    warnings.warn(
        "bertfam.utils.load_tf_model is deprecated. Use bertfam.load_pretrained_tf_model instead",
        DeprecationWarning,
    )
    return load_pretrained_tf_model(*args, **kwargs)


def load_model(
    filename: str, custom_objects: Optional[dict] = None, **kwargs
) -> tf.keras.models.Model:
    base_custom_objects = get_custom_objects()
    if custom_objects:
        base_custom_objects.update(custom_objects)
    return tf.keras.models.load_model(
        filename, custom_objects=base_custom_objects, **kwargs
    )


def get_layer_names(model: tf.keras.layers.Layer) -> List[str]:
    return [layer.name for layer in model.layers]
