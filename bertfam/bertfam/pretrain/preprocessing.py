from typing import Iterable, List, Union

from bertfam import Tokenizer
from bertfam.pretrain.create_pretraining_data import (
    TrainingInstance,
    create_masked_lm_predictions,
    truncate_seq_pair,
    create_int_feature,
    create_float_feature,
)
from bertfam.pretrain.utils import get_nlp, read_jsonl
from bertfam.utils import tqdm
import json
import tensorflow as tf
import collections
from functools import partial
from multiprocessing import Pool, Queue, cpu_count
from itertools import chain, islice
import random
import logging

logger = logging.getLogger(__name__)


rand_doc_queue = Queue(cpu_count())


def preprocess_sentence(sent, sub_ent_prob: float = 0.1) -> str:

    if isinstance(sent, str):
        sent = get_nlp()(sent)

    rng = random.Random()

    new_sent = list(map(str, sent))
    for span in sent.ents:
        if rng.random() < sub_ent_prob:
            new_sent[span.start - sent.start] = f"[{span[0].ent_type_}]"
            for i in range(span.start - sent.start + 1, span.end - sent.start):
                new_sent[i] = ""
    new_sent = " ".join(map(str, new_sent))

    return new_sent


def docs_to_instances(
    docs: Iterable[List[List[str]]],
    tokenizer: Tokenizer,
    max_seq_len: int,
    dupe_factor: int = 10,
    short_seq_prob: float = 0.1,
    masked_lm_prob: float = 0.15,
    max_predictions_per_seq: int = 20,
) -> List[TrainingInstance]:

    rng = random.Random()

    if not isinstance(docs, list):
        docs = list(docs)
    rng.shuffle(docs)

    print("creating instances from documents")

    instances = []
    for i, doc in enumerate(tqdm(docs)):
        for _ in range(dupe_factor):
            instances.extend(
                create_instances_from_document(
                    docs,
                    i,
                    max_seq_len,
                    short_seq_prob,
                    masked_lm_prob,
                    max_predictions_per_seq,
                    tokenizer,
                    rng,
                )
            )
    rng.shuffle(instances)
    return instances


def docs_to_tf_records(
    docs: Iterable[List[List[str]]],
    tokenizer: Tokenizer,
    max_seq_len: int,
    output_files: Union[List[str], str],
    dupe_factor: int = 10,
    short_seq_prob: float = 0.1,
    masked_lm_prob: float = 0.15,
    max_predictions_per_seq: int = 20,
):
    instances = docs_to_instances(
        docs,
        tokenizer,
        max_seq_len,
        dupe_factor,
        short_seq_prob,
        masked_lm_prob,
        max_predictions_per_seq,
    )

    if isinstance(output_files, str):
        output_files = [output_files]

    write_instance_to_example_files(
        instances, tokenizer, max_seq_len, max_predictions_per_seq, output_files
    )


def doc_file_to_tf_records(
    doc_file: str,
    tokenizer: Tokenizer,
    max_seq_len: int,
    output_files: Union[List[str], str],
    dupe_factor: int = 10,
    short_seq_prob: float = 0.1,
    masked_lm_prob: float = 0.15,
    max_predictions_per_seq: int = 20,
):
    instances = doc_file_to_instances(
        doc_file,
        tokenizer,
        max_seq_len,
        dupe_factor,
        short_seq_prob,
        masked_lm_prob,
        max_predictions_per_seq,
    )

    if isinstance(output_files, str):
        output_files = [output_files]

    write_instance_to_example_files(
        instances, tokenizer, max_seq_len, max_predictions_per_seq, output_files
    )


def doc_file_to_instances(
    doc_file: str,
    tokenizer: Tokenizer,
    max_seq_len: int,
    dupe_factor: int = 10,
    short_seq_prob: float = 0.1,
    masked_lm_prob: float = 0.15,
    max_predictions_per_seq: int = 20,
) -> Iterable[TrainingInstance]:

    rng = random.Random()

    f = partial(
        create_instances_from_document,
        rand_docs=None,
        max_seq_len=max_seq_len,
        short_seq_prob=short_seq_prob,
        masked_lm_prob=masked_lm_prob,
        max_predictions_per_seq=max_predictions_per_seq,
        tokenizer=tokenizer,
        rng=rng,
    )

    with Pool() as pool:
        logger.debug("launching rand doc worker")
        pool.apply_async(rand_doc_worker, args=(doc_file,))
        for _ in range(dupe_factor):
            instances = chain.from_iterable(
                pool.imap_unordered(f, read_jsonl(doc_file), chunksize=1024)
            )
            for instance in instances:
                yield instance


def rand_doc_gen(doc_file: str) -> Iterable[List[List[str]]]:

    logger.debug("rand doc gen starting")

    rng = random.Random()

    while True:
        with open(doc_file) as f:
            while True:
                try:
                    raw = next(islice(f, rng.randint(0, 1000), None))
                    yield json.loads(raw)
                except StopIteration:
                    break


def rand_doc_worker(doc_file: str):
    logger.info("rand doc worker starting")
    for doc in rand_doc_gen(doc_file):
        rand_doc_queue.put(doc)


def create_instances_from_document(
    document: List[List[str]],
    rand_docs: Iterable[List[List[str]]],
    max_seq_len: int,
    short_seq_prob: float,
    masked_lm_prob: float,
    max_predictions_per_seq: int,
    tokenizer,
    rng,
):
    """Creates `TrainingInstance`s for a single document."""

    # Account for [CLS], [SEP], [SEP]
    max_num_tokens = max_seq_len - 3

    # We *usually* want to fill up the entire sequence since we are padding
    # to `max_seq_len` anyways, so short sequences are generally wasted
    # computation. However, we *sometimes*
    # (i.e., short_seq_prob == 0.1 == 10% of the time) want to use shorter
    # sequences to minimize the mismatch between pre-training and fine-tuning.
    # The `target_seq_length` is just a rough target however, whereas
    # `max_seq_len` is a hard limit.
    target_seq_length = max_num_tokens
    if rng.random() < short_seq_prob:
        target_seq_length = rng.randint(2, max_num_tokens)

    # We DON'T just concatenate all of the tokens from a document into a long
    # sequence and choose an arbitrary split point because this would make the
    # next sentence prediction task too easy. Instead, we split the input into
    # segments "A" and "B" based on the actual "sentences" provided by the user
    # input.
    instances = []
    current_chunk = []
    current_length = 0
    i = 0
    while i < len(document):
        segment = document[i]
        current_chunk.append(segment)
        current_length += len(segment)
        if i == len(document) - 1 or current_length >= target_seq_length:
            if current_chunk:
                # `a_end` is how many segments from `current_chunk` go into the `A`
                # (first) sentence.
                a_end = 1
                if len(current_chunk) >= 2:
                    a_end = rng.randint(1, len(current_chunk) - 1)

                tokens_a = []
                for j in range(a_end):
                    tokens_a.extend(current_chunk[j])

                tokens_b = []
                # Random next
                if len(current_chunk) == 1 or rng.random() < 0.5:
                    is_random_next = True
                    target_b_length = target_seq_length - len(tokens_a)

                    # This should rarely go for more than one iteration for large
                    # corpora. However, just to be careful, we try to make sure that
                    # the random document is not the same as the document
                    # we're processing.
                    for _ in range(10):
                        if rand_docs is None:
                            random_document = rand_doc_queue.get()
                        else:
                            random_document = next(rand_docs)
                        if random_document != document:
                            break

                    random_start = rng.randint(0, len(random_document) - 1)
                    for j in range(random_start, len(random_document)):
                        tokens_b.extend(random_document[j])
                        if len(tokens_b) >= target_b_length:
                            break
                    # We didn't actually use these segments so we "put them back" so
                    # they don't go to waste.
                    num_unused_segments = len(current_chunk) - a_end
                    i -= num_unused_segments
                # Actual next
                else:
                    is_random_next = False
                    for j in range(a_end, len(current_chunk)):
                        tokens_b.extend(current_chunk[j])
                truncate_seq_pair(tokens_a, tokens_b, max_num_tokens, rng)

                assert len(tokens_a) >= 1
                assert len(tokens_b) >= 1

                tokens = []
                segment_ids = []
                tokens.append(tokenizer.cls_token)
                segment_ids.append(0)
                for token in tokens_a:
                    tokens.append(token)
                    segment_ids.append(0)

                tokens.append(tokenizer.sep_token)
                segment_ids.append(0)

                for token in tokens_b:
                    tokens.append(token)
                    segment_ids.append(1)
                tokens.append(tokenizer.sep_token)
                segment_ids.append(1)

                (
                    tokens,
                    masked_lm_positions,
                    masked_lm_labels,
                ) = create_masked_lm_predictions(
                    tokens, masked_lm_prob, max_predictions_per_seq, tokenizer, rng
                )
                instance = TrainingInstance(
                    tokens=tokens,
                    segment_ids=segment_ids,
                    is_random_next=is_random_next,
                    masked_lm_positions=masked_lm_positions,
                    masked_lm_labels=masked_lm_labels,
                )
                instances.append(instance)
            current_chunk = []
            current_length = 0
        i += 1

    return instances


def write_instance_to_example_files(
    instances, tokenizer, max_seq_length, max_predictions_per_seq, output_files
):
    """Create TF example files from `TrainingInstance`s."""
    writers = []
    for output_file in output_files:
        writers.append(tf.python_io.TFRecordWriter(output_file))

    writer_index = 0

    logger.info(
        f"writing instances to {len(output_files)} file(s) like {output_files[0]}"
    )

    total_written = 0
    for (inst_index, instance) in enumerate(tqdm(instances)):
        input_ids = tokenizer.tokens_to_ids(instance.tokens).tolist()
        input_mask = [1] * len(input_ids)
        segment_ids = list(instance.segment_ids)
        assert len(input_ids) <= max_seq_length

        while len(input_ids) < max_seq_length:
            input_ids.append(0)
            input_mask.append(0)
            segment_ids.append(0)

        assert len(input_ids) == max_seq_length
        assert len(input_mask) == max_seq_length
        assert len(segment_ids) == max_seq_length

        masked_lm_positions = list(instance.masked_lm_positions)
        masked_lm_ids = tokenizer.tokens_to_ids(instance.masked_lm_labels).tolist()
        masked_lm_weights = [1.0] * len(masked_lm_ids)

        while len(masked_lm_positions) < max_predictions_per_seq:
            masked_lm_positions.append(0)
            masked_lm_ids.append(0)
            masked_lm_weights.append(0.0)

        next_sentence_label = 1 if instance.is_random_next else 0

        features = collections.OrderedDict()
        features["input_ids"] = create_int_feature(input_ids)
        features["input_mask"] = create_int_feature(input_mask)
        features["segment_ids"] = create_int_feature(segment_ids)
        features["masked_lm_positions"] = create_int_feature(masked_lm_positions)
        features["masked_lm_ids"] = create_int_feature(masked_lm_ids)
        features["masked_lm_weights"] = create_float_feature(masked_lm_weights)
        features["next_sentence_labels"] = create_int_feature([next_sentence_label])

        tf_example = tf.train.Example(features=tf.train.Features(feature=features))

        writers[writer_index].write(tf_example.SerializeToString())
        writer_index = (writer_index + 1) % len(writers)

        total_written += 1

    for writer in writers:
        writer.close()
