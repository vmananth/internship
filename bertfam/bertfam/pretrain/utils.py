from functools import lru_cache
from typing import Iterable, Any
import json
from bertfam.utils import open
from bertfam.model import MLM_OUTPUT
from bertfam.layers.mlm import MLM_SIM
from bertfam import BertTokenizer, Tokenizer
import tensorflow as tf
import numpy as np


@lru_cache(1)
def get_nlp():
    try:
        import spacy
    except ImportError:
        print(
            "Preprocessing texts needs spacy and the `en_core_web_lg` model installed:"
        )
        print("\n    pip install spacy")
        print("    python -m spacy download en_core_web_lg\n")
        raise
    try:
        nlp = spacy.load("en_core_web_lg")
    except OSError:
        print("Preprocessing texts needs the `en_core_web_lg` model installed:")
        print("\n    python -m spacy download en_core_web_lg\n")
        raise
    return nlp


def read_jsonl(location: str) -> Iterable[Any]:
    line_no = 0
    with open(location) as f:
        for line in f:
            line_no += 1
            line = line.strip()
            if line == "":
                continue
            try:
                yield json.loads(line)
            except ValueError:
                raise ValueError(f"Invalid JSON on line {line_no}: {line}")


def write_jsonl(location: str, lines: Iterable[Any]):
    with open(location, "w") as f:
        for line in lines:
            f.write(json.dumps(line) + "\n")


def fill_in(model: tf.keras.Model, x: str, sample=True) -> str:

    mlm_output = model.get_layer("mlm_sim").output

    if isinstance(model.inputs, list):
        inputs = [
            x
            for x in model.inputs
            if x.name.startswith("token_ids")
            or x.name.startswith("attention_mask")
            or x.name.startswith("seg_ids")
        ]
    else:
        inputs = model.inputs

    mlm_model = tf.keras.Model(inputs, mlm_output)
    tokenizer = Tokenizer.from_model(mlm_model)

    tokens = tokenizer.tokenize_text(x)
    tokens = [tokenizer.cls_token] + tokens + [tokenizer.sep_token]

    while tokenizer.mask_token in tokens:

        X = tokenizer(" ".join(tokens))

        mask_index = tokens.index(tokenizer.mask_token)

        y = mlm_model.predict(X)[0]

        y = y[mask_index]

        if sample:
            token_id = np.random.choice(range(len(y)), p=y)
        else:
            token_id = np.argmax(y)

        token = tokenizer[token_id]

        tokens[mask_index] = token

    tokens = tokens[1:-1]

    return tokenizer.detokenize(tokens)
