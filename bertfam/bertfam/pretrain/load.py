"""

"""
from dataclasses import dataclass
from multiprocessing import cpu_count
from typing import Iterable, Union, List

import tensorflow as tf

from bertfam import Tokenizer
from bertfam.pretrain.preprocessing import docs_to_instances, TrainingInstance


def docs_to_dataset(
    docs: Iterable[List[List[str]]],
    tokenizer: Tokenizer,
    max_seq_len: int,
    dupe_factor: int = 10,
    short_seq_prob: float = 0.1,
    masked_lm_prob: float = 0.15,
    max_predictions_per_seq: int = 20,
    training: bool = True,
    seed: int = 12345,
) -> tf.data.Dataset:
    instances = docs_to_instances(
        docs,
        tokenizer,
        max_seq_len,
        dupe_factor,
        short_seq_prob,
        masked_lm_prob,
        max_predictions_per_seq,
        seed,
    )

    processed = process_instances(
        instances, tokenizer, max_seq_len, max_predictions_per_seq
    )

    def gen():
        for i in processed:
            yield (
                (
                    i.input_ids,
                    i.input_mask,
                    i.segment_ids,
                    i.masked_lm_ids,
                    i.masked_lm_positions,
                    i.masked_lm_weights,
                    i.next_sentence_label,
                ),
                (i.input_mask, i.input_mask),
            )

    d = tf.data.Dataset.from_generator(
        gen,
        (
            (tf.int32, tf.int32, tf.int32, tf.int32, tf.int32, tf.float32, tf.int32),
            (tf.int32, tf.int32),
        ),
    )

    if training:
        d = d.shuffle(buffer_size=100)
        d = d.repeat()

    return d


def tf_records_to_dataset(
    filenames: Union[List[str], str],
    batch_size: int,
    max_seq_len: int,
    max_predictions_per_seq: int = 20,
    training=True,
) -> tf.data.Dataset:
    num_cpu_threads = cpu_count()
    if isinstance(filenames, str):
        filenames = [filenames]

    name_to_features = {
        "input_ids": tf.FixedLenFeature([max_seq_len], tf.int64),
        "input_mask": tf.FixedLenFeature([max_seq_len], tf.int64),
        "segment_ids": tf.FixedLenFeature([max_seq_len], tf.int64),
        "masked_lm_positions": tf.FixedLenFeature([max_predictions_per_seq], tf.int64),
        "masked_lm_ids": tf.FixedLenFeature([max_predictions_per_seq], tf.int64),
        "masked_lm_weights": tf.FixedLenFeature([max_predictions_per_seq], tf.float32),
        "next_sentence_labels": tf.FixedLenFeature([1], tf.int64),
    }

    if training:
        d = tf.data.Dataset.from_tensor_slices(tf.constant(filenames))
        d = d.repeat()
        d = d.shuffle(buffer_size=len(filenames))

        # `cycle_length` is the number of parallel files that get read.
        cycle_length = min(num_cpu_threads, len(filenames))

        # `sloppy` mode means that the interleaving is not exact. This adds
        # even more randomness to the training pipeline.
        d = d.apply(
            tf.data.experimental.parallel_interleave(
                tf.data.TFRecordDataset, sloppy=training, cycle_length=cycle_length
            )
        )
        d = d.shuffle(buffer_size=5)
    else:
        d = tf.data.TFRecordDataset(filenames)
        d = d.repeat()

    d = d.apply(
        tf.data.experimental.map_and_batch(
            lambda record: tf.parse_single_example(record, name_to_features),
            batch_size=batch_size,
            num_parallel_batches=num_cpu_threads,
            drop_remainder=True,
        )
    )

    d = d.map(
        lambda x: [
            (
                x["input_ids"],
                x["input_mask"],
                x["segment_ids"],
                x["masked_lm_ids"],
                x["masked_lm_positions"],
                x["masked_lm_weights"],
                x["next_sentence_labels"],
            ),
            (x["input_mask"], x["input_mask"]),
        ]
    )

    return d


@dataclass
class ProcessedInstance:
    input_ids: List[int]
    input_mask: List[int]
    segment_ids: List[int]
    masked_lm_positions: List[int]
    masked_lm_ids: List[int]
    masked_lm_weights: List[float]
    next_sentence_label: int


def process_instance(
    instance: TrainingInstance,
    tokenizer: Tokenizer,
    max_seq_len: int,
    max_predictions_per_seq: int = 20,
) -> ProcessedInstance:

    input_ids = list(tokenizer.tokens_to_ids(instance.tokens))
    input_mask = [1] * len(input_ids)
    segment_ids = list(instance.segment_ids)

    while len(input_ids) < max_seq_len:
        input_ids.append(0)
        input_mask.append(0)
        segment_ids.append(0)

    masked_lm_positions = list(instance.masked_lm_positions)
    masked_lm_ids = list(tokenizer.tokens_to_ids(instance.masked_lm_labels))
    masked_lm_weights = [1.0] * len(masked_lm_ids)

    while len(masked_lm_positions) < max_predictions_per_seq:
        masked_lm_positions.append(0)
        masked_lm_ids.append(0)
        masked_lm_weights.append(0.0)

    next_sentence_label = 1 if instance.is_random_next else 0

    return ProcessedInstance(
        input_ids=input_ids,
        input_mask=input_mask,
        segment_ids=segment_ids,
        masked_lm_positions=masked_lm_positions,
        masked_lm_ids=masked_lm_ids,
        masked_lm_weights=masked_lm_weights,
        next_sentence_label=next_sentence_label,
    )


def process_instances(
    instances: List[TrainingInstance],
    tokenizer: Tokenizer,
    max_seq_len: int,
    max_predictions_per_seq: int = 20,
) -> List[ProcessedInstance]:
    return [
        process_instance(i, tokenizer, max_seq_len, max_predictions_per_seq)
        for i in instances
    ]
