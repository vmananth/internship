import tensorflow as tf


class LayerNorm(tf.keras.layers.Layer):
    def __init__(self, dim: int, **kwargs):
        self.dim = dim
        super(LayerNorm, self).__init__(**kwargs)

    def build(self, input_shape):
        self.gamma = self.add_weight(
            "gamma", (self.dim,), dtype=tf.float32, trainable=self.trainable
        )
        self.beta = self.add_weight(
            "beta", (self.dim,), dtype=tf.float32, trainable=self.trainable
        )
        super(LayerNorm, self).build(input_shape)

    def get_config(self) -> dict:
        config = super(LayerNorm, self).get_config()
        config["dim"] = self.dim
        return config

    def call(self, X: tf.Tensor, **kwargs) -> tf.Tensor:

        u = tf.reduce_mean(X, -1, keep_dims=True)
        s = tf.reduce_mean(tf.pow((X - u), 2), -1, keep_dims=True)
        X = (X - u) / tf.sqrt(s + 1e-12)

        return self.gamma * X + self.beta
