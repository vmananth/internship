from typing import Optional, Union, List
import tensorflow as tf
from tensorflow.python.keras.layers import Layer
from bertfam.layers.transformer import TransformerLayer


class Encoder(Layer):
    """Stack of transformer layers"""

    def __init__(
        self, num_layers: int, num_heads: int, dim: int, hidden_dim: int, **kwargs
    ):

        super(Encoder, self).__init__(**kwargs)

        self.num_layers = num_layers
        self.num_heads = num_heads
        self.dim = dim
        self.hidden_dim = hidden_dim

    def count_params(self):
        return super(Encoder, self).count_params() + sum(
            [l.count_params() for l in self.layers]
        )

    def build(self, input_shape):

        if isinstance(input_shape, list):
            input_shape = input_shape[0]

        self.layers = []

        with tf.variable_scope("encoder"):

            for i in range(self.num_layers):

                with tf.variable_scope(str(i)):

                    layer = TransformerLayer(
                        self.num_heads,
                        self.dim,
                        self.hidden_dim,
                        name=f"transformer_layer{i}",
                        trainable=self.trainable,
                    )
                    layer.build(input_shape)

                    self.layers.append(layer)

            super(Encoder, self).build(input_shape)

    @property
    def trainable_weights(self):
        if not self.trainable:
            return []
        # weights = super(Encoder, self).trainable_weights
        weights = []
        for l in self.layers:
            weights += l.trainable_weights
        return weights

    @property
    def non_trainable_weights(self):
        # weights = super(Encoder, self).non_trainable_weights
        weights = []
        for layer in self.layers:
            weights += layer.non_trainable_weights
        if not self.trainable:
            for layer in self.layers:
                weights += layer.trainable_weights
        return weights

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self) -> dict:
        config = super(Encoder, self).get_config()
        config["num_layers"] = self.num_layers
        config["num_heads"] = self.num_heads
        config["dim"] = self.dim
        config["hidden_dim"] = self.hidden_dim
        return config

    def call(self, X: Union[tf.Tensor, List[tf.Tensor]], **kwargs):
        """
        args:
            X: tensor of shape (batch_size, seq_len, dim)
            mask: tensor of shape (batch_size, seq_len, seq_len)

        returns:
            X: tensor of shape (batch_size, seq_len, dim)
        """

        attention_mask = None
        if isinstance(X, list):
            if len(X) == 1:
                X = X[0]
            elif len(X) == 2:
                X, attention_mask = X
            else:
                raise ValueError(
                    f"Input to {self.__class__.__name__} must be a single tensor (token_ids), "
                    f"list of one tensor ([token_ids]), "
                    f"or a list of two tensors ([token_ids, attention_mask])"
                )

        for layer in self.layers:
            X = layer([X, attention_mask], **kwargs)

        return X
