from typing import List
import tensorflow as tf
from bertfam.layers.norm import LayerNorm
from bertfam import Embeddings
from bertfam.layers.activations import approx_gelu

MLM_FC = "mlm_fc"
MLM_NORM = "mlm_norm"
MLM_SIM = "mlm_sim"


def build_mlm_output(embeddings: Embeddings, dim: int):

    X = tf.keras.Input(shape=(None, dim), name="seq_input")

    X = tf.keras.layers.Dense(dim)(X)
    X = LayerNorm(dim)(X)

    def _emb_weights(x):
        weights = embeddings.token_embeddings.weights[0]
        weights = weights[tf.newaxis, :, :]
        return weights

    weights = tf.keras.layers.Lambda(_emb_weights)(X)

    X = tf.keras.layers.Dot(axes=[2, 2])([X, weights])

    raise NotImplementedError


class MLM(tf.keras.layers.Layer):
    def __init__(self, vocab_size: int, dim: int, **kwargs):

        self.vocab_size = vocab_size
        self.dim = dim

        super(MLM, self).__init__(**kwargs)

    def build(self, input_shape: List[tf.TensorShape]):

        with tf.variable_scope("mlm"):

            self.bias = self.add_weight(
                "mlm_bias",
                (self.vocab_size,),
                dtype=tf.float32,
                trainable=self.trainable,
            )
            self.fc = tf.keras.layers.Dense(
                self.dim, trainable=self.trainable, name=MLM_FC, activation=approx_gelu
            )
            self.fc.build(input_shape[0])
            input_shape = self.fc.compute_output_shape(input_shape[0])

            self.norm = LayerNorm(self.dim, trainable=self.trainable, name=MLM_NORM)
            self.norm.build(input_shape)

        super(MLM, self).build(input_shape)

    def get_config(self):
        config = super(MLM, self).get_config()
        config["vocab_size"] = self.vocab_size
        config["dim"] = self.dim
        return config

    def compute_output_shape(self, input_shape: List[tf.TensorShape]):

        X_shape, embeddings_shape = input_shape

        return tf.TensorShape([X_shape[0], self.vocab_size])

    def call(self, inputs: List[tf.Tensor], **kwargs):

        X, embeddings = inputs

        X = self.fc(X)
        X = self.norm(X)

        def _sim(x):
            x = tf.tensordot(x, embeddings, axes=(2, 1))
            x = tf.nn.bias_add(x, self.bias)
            return x

        X = tf.keras.layers.Lambda(_sim, name="mlm_sim")(X)

        X = tf.keras.layers.Softmax()(X)

        return X
