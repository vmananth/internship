"""
Implementation borrowed heavily from https://www.tensorflow.org/alpha/tutorials/sequences/transformer
"""
from typing import Union, List

import tensorflow as tf
from bertfam.layers.attention import MultiHeadAttention
from bertfam.layers.activations import gelu, approx_gelu
from bertfam.layers.norm import LayerNorm


class TransformerLayer(tf.keras.layers.Layer):
    def __init__(self, num_heads: int, dim: int, hidden_dim: int, **kwargs):

        super(TransformerLayer, self).__init__(**kwargs)

        self.num_heads = num_heads
        self.dim = dim
        self.hidden_dim = hidden_dim

    def compute_output_shape(self, input_shape):
        return input_shape

    def build(self, input_shape):

        if isinstance(input_shape, list):
            input_shape = input_shape[0]

        with tf.variable_scope("mha"):

            self.multi_head_attention = MultiHeadAttention(
                self.num_heads, self.dim, trainable=self.trainable
            )
            self.multi_head_attention.build([input_shape, input_shape, input_shape])
        # input_shape = self.multi_head_attention.compute_output_shape(input_shape)

        with tf.variable_scope("norm1"):
            self.norm1 = LayerNorm(self.dim, trainable=self.trainable)
            self.norm1.build(input_shape)
        with tf.variable_scope("ff1"):
            self.ff1 = tf.keras.layers.Dense(
                self.hidden_dim, activation=approx_gelu, trainable=self.trainable
            )
            self.ff1.build(input_shape)
            input_shape = self.ff1.compute_output_shape(input_shape)

        with tf.variable_scope("ff2"):
            self.ff2 = tf.keras.layers.Dense(self.dim, trainable=self.trainable)
            self.ff2.build(input_shape)

        with tf.variable_scope("norm2"):
            self.norm2 = LayerNorm(self.dim, trainable=self.trainable)
            self.norm2.build(input_shape)
        self.dropout = tf.keras.layers.Dropout(0.1)

        super(TransformerLayer, self).build(input_shape)

    def get_config(self) -> dict:
        config = super().get_config()
        config["num_heads"] = self.num_heads
        config["dim"] = self.dim
        config["hidden_dim"] = self.hidden_dim
        return config

    def call(self, X: Union[tf.Tensor, List[tf.Tensor]], **kwargs):
        """
        args:
            X: tensor of shape (batch_size, seq_len, dim)
          or
            [X, attention_mask]: list of two tensors
              - X: tensor of shape (batch_size, seq_len, dim)
              - attention_mask: tensor of shape (batch_size, seq_len) or (batch_size, seq_len, seq_len)

        returns:
            X: tensor of shape (batch_size, seq_len, dim)
        """

        attention_mask = None

        if isinstance(X, list):
            if len(X) == 1:
                X = X[0]
            elif len(X) == 2:
                X, attention_mask = X
            else:
                raise ValueError(
                    f"Input to {self.__class__.__name__} must be a single tensor (token_ids), "
                    f"list of one tensor ([token_ids]), "
                    f"or a list of two tensors ([token_ids, attention_mask])"
                )

        if attention_mask is not None:
            if len(attention_mask.shape) == 2:
                attention_mask = tf.keras.layers.Lambda(lambda x: tf.expand_dims(x, 1))(
                    attention_mask
                )

        mha_out, _ = self.multi_head_attention([X, X, X, attention_mask])
        mha_out = self.dropout(mha_out)
        mha_out = self.norm1(mha_out + X)

        X = self.ff1(mha_out)
        X = self.ff2(X)
        X = self.dropout(X)
        X = self.norm2(mha_out + X)

        return X
