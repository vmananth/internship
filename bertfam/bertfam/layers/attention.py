"""
Implementation borrowed heavily from https://www.tensorflow.org/alpha/tutorials/sequences/transformer
"""
from typing import Optional, Tuple, List
import tensorflow as tf


def scaled_dot_product_attention(
    Q: tf.Tensor, K: tf.Tensor, V: tf.Tensor, mask: Optional[tf.Tensor] = None
) -> Tuple[tf.Tensor, tf.Tensor]:
    """
    args:
        Q: tensor of shape (batch_size, num_heads, seq_len, head_dim)
        K: tensor of shape (batch_size, num_heads, seq_len, head_dim)
        V: tensor of shape (batch_size, num_heads, seq_len, head_dim)
        mask: tensor of shape (batch_size, seq_len, seq_len) or (batch_size, num_heads, seq_len, seq_len), defaults to no mask

    returns:
        X: tensor of shape (batch_size, num_heads, seq_len, head_dim)
        a: tensor of shape (batch_size, num_heads, seq_len, seq_len)
    """

    X = tf.matmul(Q, K, transpose_b=True)  # (batch_size, num_heads, seq_len, seq_len)

    head_dim = tf.cast(tf.shape(K)[-1], tf.float32)
    X = X / tf.math.sqrt(head_dim)

    if mask is not None:
        if len(mask.shape) == 3:
            mask = tf.expand_dims(mask, 1)
        X = X + tf.multiply((tf.add(1.0, -mask)), -1e9)

    a = tf.nn.softmax(X, axis=-1)  # (batch_size, num_heads, seq_len, seq_len)

    X = tf.matmul(a, V)  # (batch_size, num_heads, seq_len, head_dim)

    return X, a


class MultiHeadAttention(tf.keras.layers.Layer):
    def __init__(self, num_heads: int, dim: int, **kwargs):

        super(MultiHeadAttention, self).__init__(**kwargs)

        if not (dim % num_heads == 0):
            msg = f"dim must be divisible by num_heads, got dim={dim} and num_heads={num_heads}"
            raise ValueError(msg)

        self.num_heads = num_heads
        self.dim = dim
        self.head_dim = dim // num_heads

    def build(self, input_shape: List[tf.TensorShape]):

        with tf.variable_scope("q"):
            self.W_q = tf.keras.layers.Dense(
                self.dim, name="queries", trainable=self.trainable
            )
            self.W_q.build(input_shape[0])

        with tf.variable_scope("k"):
            self.W_k = tf.keras.layers.Dense(
                self.dim, name="keys", trainable=self.trainable
            )
            self.W_k.build(input_shape[1])

        with tf.variable_scope("v"):
            self.W_v = tf.keras.layers.Dense(
                self.dim, name="values", trainable=self.trainable
            )
            self.W_v.build(input_shape[2])

        self.fc = tf.keras.layers.Dense(self.dim, name="fc", trainable=self.trainable)
        self.fc.build(input_shape[2])

        super().build(input_shape)

    def compute_output_shape(self, input_shape: List[tf.TensorShape]):
        batch_size, seq_len, dim = input_shape[0]
        X_shape = tf.TensorShape(input_shape[0])
        attention_shape = tf.TensorShape((batch_size, seq_len, seq_len))
        return X_shape, attention_shape

    def get_config(self) -> dict:
        config = super().get_config()
        config["num_heads"] = self.num_heads
        config["dim"] = self.dim
        return config

    def call(self, inputs: List[tf.Tensor], **kwargs) -> Tuple[tf.Tensor, tf.Tensor]:
        """
        args:
            inputs: list of three tensors of shape (batch_size, seq_len, dim)
            mask: tensor of shape (batch_size, seq_len, seq_len) or (batch_size, 1, seq_len), defaults to no mask

        returns:
            X: tensor of shape (batch_size, seq_len, dim)
            a: tensor of shape (batch_size, num_heads, seq_len, seq_len)
        """

        if len(inputs) == 4:
            Q, K, V, attention_mask = inputs
        elif len(inputs) == 3:
            Q, K, V = inputs
            attention_mask = None
        else:
            raise ValueError(
                f"Input to {self.__class__.__name__} must be a list of three tensors ([queries, keys, values]), "
                f"or a list of four tensors ([queries, keys, values, attention_mask])"
            )

        # (batch_size, seq_len, dim)
        Q = self.W_q(Q)
        K = self.W_k(K)
        V = self.W_v(V)

        # (batch_size, num_heads, seq_len, head_dim)
        Q = self.split_heads(Q)
        K = self.split_heads(K)
        V = self.split_heads(V)

        # X: (batch_size, num_heads, seq_len, head_dim)
        # a: (batch_size, num_heads, seq_len, seq_len)
        X, a = scaled_dot_product_attention(Q, K, V, attention_mask)

        X = self.combine_heads(X)  # (batch_size, seq_len, dim)
        X = self.fc(X)  # (batch_size, seq_len, dim)

        return X, a

    def combine_heads(self, X: tf.Tensor) -> tf.Tensor:
        """
        args:
            X: tensor of shape (batch_size, num_heads, seq_len, head_dim)

        returns:
            X: tensor of shape (batch_size, seq_len, dim)
        """
        batch_size = tf.shape(X)[0]
        X = tf.transpose(X, perm=[0, 2, 1, 3])
        X = tf.reshape(X, (batch_size, -1, self.dim))
        return X

    def split_heads(self, X: tf.Tensor) -> tf.Tensor:
        """
        args:
            X:  tensor of shape (batch_size, seq_len, dim)

        returns:
            X: tensor of shape (batch_size, num_heads, seq_len, head_dim)
        """
        batch_size = tf.shape(X)[0]
        X = tf.reshape(X, (batch_size, -1, self.num_heads, self.head_dim))
        X = tf.transpose(X, perm=[0, 2, 1, 3])
        return X
