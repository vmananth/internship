from typing import Union, Iterable, List
import tensorflow as tf
from bertfam.layers.norm import LayerNorm


class Embeddings(tf.keras.layers.Layer):
    def __init__(self, vocab: Iterable[str], dim: int, max_seq_len: int, **kwargs):
        self.dim = dim
        self.max_seq_len = max_seq_len
        self.vocab = list(vocab)
        super(Embeddings, self).__init__(**kwargs)

    def build(self, input_shape):

        if isinstance(input_shape, list):
            input_shape = input_shape[0]

        with tf.variable_scope("embeddings"):

            with tf.variable_scope("token"):
                self.token_embeddings = tf.keras.layers.Embedding(
                    len(self.vocab),
                    self.dim,
                    name="token_embeddings",
                    trainable=self.trainable,
                )
                self.token_embeddings.build(input_shape)
                input_shape = self.token_embeddings.compute_output_shape(input_shape)

            with tf.variable_scope("pos"):
                self.pos_embeddings = tf.keras.layers.Embedding(
                    self.max_seq_len,
                    self.dim,
                    name="pos_embeddings",
                    trainable=self.trainable,
                )
                self.pos_embeddings.build(input_shape)

            with tf.variable_scope("seg"):
                self.seg_embeddings = tf.keras.layers.Embedding(
                    2, self.dim, name="seg_embeddings", trainable=self.trainable
                )
                self.seg_embeddings.build(input_shape)
            self.norm = LayerNorm(self.dim, trainable=self.trainable)
            self.norm.build(input_shape)
            self.dropout = tf.keras.layers.Dropout(0.1)

            return super().build(input_shape)

    def get_config(self):
        config = super(Embeddings, self).get_config()
        config["dim"] = self.dim
        config["max_seq_len"] = self.max_seq_len
        config["vocab"] = self.vocab
        return config

    def compute_output_shape(self, input_shape):
        return input_shape.concatenate(self.dim)

    def call(self, X: Union[tf.Tensor, List[tf.Tensor]], **kwargs) -> tf.Tensor:
        """
        args:
            X: tensor of shape (batch_size, seq_len)
        """
        seg_ids = None

        if isinstance(X, list):
            if len(X) == 1:
                X = X[0]
            elif len(X) == 2:
                X, seg_ids = X
            else:
                raise ValueError(
                    f"Input to {self.__class__.__name__} must be a single tensor (token_ids), "
                    f"list of one tensor ([token_ids]), "
                    f"or a list of two tensors ([token_ids, seg_ids])"
                )

        batch_size = tf.shape(X)[0]
        seq_len = tf.shape(X)[1]

        pos_ids = tf.reshape(tf.tile(tf.range(seq_len), [batch_size]), (batch_size, -1))

        X_pos = self.pos_embeddings(pos_ids)  # (batch_size, seq_len, dim)

        X_tokens = self.token_embeddings(X)  # (batch_size, seq_len, dim)

        if seg_ids is None:
            seg_ids = tf.zeros_like(pos_ids)

        X_seg = self.seg_embeddings(seg_ids)

        X = X_pos + X_tokens + X_seg

        X = self.norm(X)
        X = self.dropout(X)

        return X
