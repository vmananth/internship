import tensorflow as tf
import numpy as np


SQRT2 = np.sqrt(2.0)


def gelu(x):
    return x * 0.5 * (1.0 + tf.erf(x / SQRT2))


def approx_gelu(x):
    cdf = 0.5 * (1.0 + tf.tanh((np.sqrt(2 / np.pi) * (x + 0.044715 * tf.pow(x, 3)))))
    return x * cdf
