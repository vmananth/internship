from typing import Tuple
from bertfam import (
    gelu,
    approx_gelu,
    Embeddings,
    Encoder,
    LayerNorm,
    MLM,
    MultiHeadAttention,
    TransformerLayer,
    constants,
)
import os
import shutil
from typing import Optional
import requests
import logging
import re

try:
    from smart_open import open
except ImportError:
    open = open
try:
    from tqdm import tqdm
except ImportError:
    tqdm = iter

logger = logging.getLogger(__name__)


def get_custom_objects():
    return {
        "Embeddings": Embeddings,
        "Encoder": Encoder,
        "LayerNorm": LayerNorm,
        "gelu": gelu,
        "approx_gelu": approx_gelu,
        "MLM": MLM,
        "TransformerLayer": TransformerLayer,
        "MultiHeadAttention": MultiHeadAttention,
    }


def download_file(url: str, localpath: Optional[str] = None) -> str:

    if localpath is None:
        localpath = _infer_localpath(url)
    if os.path.exists(localpath):
        return localpath

    os.makedirs(os.path.dirname(localpath), exist_ok=True)

    logger.info(f"downloading {url} to {localpath}")

    if url.startswith("https://artifactory.amfam.com"):
        user, apikey = get_artifactory_creds()
        if user and apikey:
            url = url[len("https://") :]
            url = f"https://{user}:{apikey}@{url}"

    if "googleapis.com/bert_models/" in url.lower():
        response = requests.get(url, stream=True, verify=False)
    else:
        response = requests.get(url, stream=True)

    if localpath.endswith("json"):
        with open(localpath, "w") as f:
            for chunk in response.iter_content(None, True):
                f.write(chunk)
    else:
        with open(localpath, "wb") as f:
            shutil.copyfileobj(response.raw, f)

    return localpath


def _infer_localpath(url: str) -> str:
    filename = os.path.split(url)[-1]
    if "model" in url.lower() or "artifactory.amfam.com" in url.lower():
        return os.path.join(constants.MODELS_DIR, filename)
    if "dataset" in url.lower():
        if "squad" in url.lower():
            return os.path.join(constants.DATASETS_DIR, "squad", filename)
        return os.path.join(constants.DATASETS_DIR, filename)
    else:
        return os.path.join(constants.BERTFAM_HOME, filename)


def get_artifactory_creds() -> Tuple[str, str]:

    user = os.environ.get("ARTIFACTORY_USER")
    apikey = os.environ.get("ARTIFACTORY_APIKEY")

    if user and apikey:
        return user, apikey

    env_file = os.path.expanduser("~/.jfrog/artifactory.env")

    logger.warning(
        f"ARTIFACTORY_USER and/or ARTIFACTORY_APIKEY not set, attempting to read from {env_file}"
    )

    if not os.path.exists(env_file):
        logger.warning(
            f"{env_file} not found, have you done configured artifactory? https://cloudeng-docs.amfam.com/aws/toolchain-setup/macos-basic/#configure-artifactory"
        )
        return "", ""

    with open(env_file) as f:
        for line in f:
            line = line.strip()
            m = re.match(r'export ARTIFACTORY_USER="(.*)"', line)
            if m:
                user = m.group(1)
            m = re.match(r'export ARTIFACTORY_APIKEY="(.*)"', line)
            if m:
                apikey = m.group(1)

    if not (user and apikey):
        logger.warning(
            f"Could not find ARTIFACTORY_USER and/or ARTIFACTORY_APIKEY in {env_file}"
        )
        return "", ""

    return user, apikey
