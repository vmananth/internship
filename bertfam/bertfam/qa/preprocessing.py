from dataclasses import dataclass
from typing import Optional, Iterable, Tuple, Union, List
from bertfam.qa.utils import get_squad_data
from bertfam.utils import tqdm
from bertfam import BertBertTokenizer, Tokenizer, constants
import os
import tensorflow as tf
import numpy as np
import pickle
import logging

logger = logging.getLogger(__name__)


@dataclass
class SquadFeatures:
    id: str
    token_ids: np.ndarray
    attention_mask: np.ndarray
    seg_ids: np.ndarray
    answer_start: int
    answer_end: int
    is_impossible: int


@dataclass
class SquadExample:
    id: str
    context: str
    question: str
    answer: Optional[str]
    answer_start: Optional[int]
    is_impossible: bool

    def to_features(self, tokenizer: Tokenizer) -> SquadFeatures:

        if not isinstance(tokenizer, BertBertTokenizer):
            tokenizer = tokenizer.to_bertbert_tokenizer()

        tokenizer.pad = True
        tokenizer.attention_mask = True
        tokenizer.seg_ids = True

        X_tokens, X_mask, X_seg = tokenizer((self.question, self.context))

        q_len = np.logical_not(X_seg).sum()  # question is where X_seg = 0

        if self.is_impossible:
            answer_start = answer_end = -1
        else:
            answer_start = q_len + len(
                tokenizer.tokenize_text(self.context[: self.answer_start])
            )
            answer_end = answer_start + len(tokenizer.tokenize_text(self.answer))

        return SquadFeatures(
            id=self.id,
            token_ids=X_tokens[0],
            attention_mask=X_mask[0],
            seg_ids=X_seg[0],
            answer_start=answer_start,
            answer_end=answer_end,
            is_impossible=int(self.is_impossible),
        )


def get_squad_datasets(
    tokenizer: Tokenizer = None, batch_size: int = 1
) -> Tuple[tf.data.Dataset, tf.data.Dataset]:

    train_features, dev_features = get_squad_features(tokenizer)

    train_dataset = features_to_dataset(train_features, batch_size)
    dev_dataset = features_to_dataset(dev_features, batch_size)

    return train_dataset, dev_dataset


def features_to_dataset(
    features: List[SquadFeatures], batch_size: int = 1, training: bool = True
) -> tf.data.Dataset:
    def gen():
        for f in features:
            yield (
                (f.token_ids, f.attention_mask, f.seg_ids),
                (f.is_impossible, f.answer_start, f.answer_end),
            )

    dataset = tf.data.Dataset.from_generator(
        gen, ((tf.int32, tf.int32, tf.int32), (tf.int32, tf.int32, tf.int32))
    )

    if training:
        dataset = dataset.shuffle(buffer_size=1000)
        dataset = dataset.repeat()

    dataset = dataset.batch(batch_size)

    return dataset


def get_squad_features(
    tokenizer: Tokenizer = None
) -> Tuple[List[SquadFeatures], List[SquadFeatures]]:

    if tokenizer is None:
        tokenizer = BertBertTokenizer.from_pretrained_tf_model()

    uid = tokenizer.uid
    train_features_pickle = os.path.join(
        constants.SQUAD_DATA_DIR, f"train_features_{uid}.pickle"
    )
    dev_features_pickle = os.path.join(
        constants.SQUAD_DATA_DIR, f"dev_features_{uid}.pickle"
    )

    if not (
        os.path.exists(train_features_pickle) and os.path.exists(dev_features_pickle)
    ):

        train, dev = get_squad_data()

        train_features = (x.to_features(tokenizer) for x in get_examples(train))
        dev_features = (x.to_features(tokenizer) for x in get_examples(dev))

        # for debugging
        # train_features = islice(train_features, 100)
        # dev_features = islice(dev_features, 100)

        logger.info("processing squad train dataset")
        train_features = list(tqdm(train_features))
        logger.info("processing squad dev dataset")
        dev_features = list(tqdm(dev_features))

        with open(train_features_pickle, "wb") as f:
            pickle.dump(train_features, f)
        with open(dev_features_pickle, "wb") as f:
            pickle.dump(dev_features, f)

    else:
        with open(train_features_pickle, "rb") as f:
            train_features = pickle.load(f)
        with open(dev_features_pickle, "rb") as f:
            dev_features = pickle.load(f)

    return train_features, dev_features


def get_examples(squad_data: Union[dict, str]) -> Iterable[SquadExample]:
    """
    squad_data is what is contained in the e.g. train-v2.0.json file

    Usage:

    >>> train, dev = get_squad_data()
    >>> train_examples = get_examples(train)
    >>> dev_examples = get_examples(dev)
    """
    if isinstance(squad_data, str):
        if squad_data == "train":
            squad_data, _ = get_squad_data()
        elif squad_data == "dev":
            _, squad_data = get_squad_data()
        else:
            raise ValueError(
                f"unknown squad data {squad_data}, did you mean 'train' or 'dev'?"
            )
    for topic in squad_data["data"]:
        for paragraph in topic["paragraphs"]:
            context = paragraph["context"]
            for qa in paragraph["qas"]:
                question = qa["question"]
                is_impossible = bool(qa["is_impossible"])
                qa_id = qa["id"]
                if not is_impossible:
                    for x in qa["answers"]:
                        answer = x["text"]
                        answer_start = x["answer_start"]
                        ex = SquadExample(
                            id=qa_id,
                            context=context,
                            question=question,
                            answer=answer,
                            answer_start=answer_start,
                            is_impossible=is_impossible,
                        )
                        yield ex
                else:
                    ex = SquadExample(
                        id=qa_id,
                        context=context,
                        question=question,
                        answer=None,
                        answer_start=None,
                        is_impossible=is_impossible,
                    )
                    yield ex
