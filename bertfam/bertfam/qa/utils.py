from bertfam.utils import download_file
from typing import Tuple
import json


SQUAD_JSON_URLS = {
    "train": "https://rajpurkar.github.io/SQuAD-explorer/dataset/train-v2.0.json",
    "dev": "https://rajpurkar.github.io/SQuAD-explorer/dataset/dev-v2.0.json",
}


def get_squad_data() -> Tuple[dict, dict]:
    train_json = download_file(SQUAD_JSON_URLS["train"])
    dev_json = download_file(SQUAD_JSON_URLS["dev"])

    with open(train_json) as f:
        train = json.load(f)
    with open(dev_json) as f:
        dev = json.load(f)

    return train, dev
