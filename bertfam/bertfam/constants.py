import os

BERTFAM_HOME = os.environ.get(
    "BERTFAM_HOME", os.path.join(os.path.expanduser("~"), ".keras", "bertfam")
)

MODELS_DIR = os.path.join(BERTFAM_HOME, "models")
DATASETS_DIR = os.path.join(BERTFAM_HOME, "datasets")
SQUAD_DATA_DIR = os.path.join(DATASETS_DIR, "squad")

for d in [MODELS_DIR, DATASETS_DIR]:
    os.makedirs(d, exist_ok=True)
