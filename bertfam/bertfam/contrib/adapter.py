from typing import Optional
from bertfam.layers.transformer import TransformerLayer
from bertfam.layers.activations import gelu
from bertfam.layers.encoder import Encoder
from bertfam.layers.embeddings import Embeddings
import tensorflow as tf


class Adapter(tf.keras.layers.Layer):
    """
    Implements an Adapter Layer from [Parameter-Efficient Transfer Learning for NLP](https://arxiv.org/abs/1902.00751)
    """

    def __init__(self, dim: int, adapter_dim: int, **kwargs):

        super(Adapter, self).__init__(**kwargs)
        self.dim = dim
        self.adapter_dim = adapter_dim

    def build(self, input_shape):

        self.fc1 = tf.keras.layers.Dense(
            self.adapter_dim,
            kernel_initializer="zeros",
            bias_initializer="zeros",
            activation=gelu,
        )
        self.fc1.build(input_shape)
        input_shape = self.fc1.compute_output_shape(input_shape)

        self.fc2 = tf.keras.layers.Dense(
            self.dim, kernel_initializer="zeros", bias_initializer="zeros"
        )
        self.fc2.build(input_shape)
        input_shape = self.fc2.compute_output_shape

        super(Adapter, self).build(input_shape)

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self) -> dict:
        config = super(Adapter, self).get_config()
        config["dim"] = self.dim
        config["adapter_dim"] = self.adapter_dim
        return config

    def call(self, X: tf.Tensor, mask: Optional[tf.Tensor] = None, **kwargs):

        Xa = self.fc1(X)
        Xa = self.fc2(Xa)

        return X + Xa


class AdaptedTransformerLayer(TransformerLayer):
    def __init__(
        self, num_heads: int, dim: int, hidden_dim: int, adapter_dim: int, **kwargs
    ):
        self.adapter_dim = adapter_dim

        super(AdaptedTransformerLayer, self).__init__(
            num_heads, dim, hidden_dim, **kwargs
        )

    def build(self, input_shape):

        self.adapter1 = Adapter(self.dim, self.adapter_dim)
        self.adapter1.build(input_shape)
        self.adapter2 = Adapter(self.dim, self.adapter_dim)
        self.adapter2.build(input_shape)

        super(AdaptedTransformerLayer, self).build(input_shape)

    def get_config(self) -> dict:
        config = super(AdaptedTransformerLayer, self).get_config()
        config["adapter_dim"] = self.adapter_dim
        return config

    def call(self, X: tf.Tensor, mask: Optional[tf.Tensor] = None):

        if mask is not None:
            if len(mask.shape) == 2:
                mask = mask[:, tf.newaxis, :]

        mha_out, _ = self.multi_head_attention([X, X, X], mask=mask)
        mha_out = self.dropout(mha_out)
        mha_out = self.adapter1(mha_out)
        mha_out = self.norm1(mha_out + X)

        X = self.ff1(mha_out)
        X = self.ff2(X)
        X = self.dropout(X)
        X = self.adapter2(X)
        X = self.norm2(mha_out + X)

        return X


def adapt_transformer_layer(
    layer: TransformerLayer,
    adapter_dim: int = 8,
    freeze_transformer: bool = True,
    freeze_adapters: bool = False,
    freeze_norms: bool = False,
    inplace: bool = False,
) -> AdaptedTransformerLayer:
    """
    Turns a normal TransformerLayer into an AdaptedTransformerLayer by inserting the Adapter layers

    args:
        layer: instance of TransformerLayer to convert
        adapter_dim: the hidden "bottleneck" dimension inside the adapter ("m" in the paper)
        freeze_transformer: whether to set the TransformerLayer weights to be trainable or not
        freeze_adapters: whether to set teh Adapter weights to be trainable or not
        freeze_norms: whether to set the LayerNorm weights of the TransformerLayer to be trainable or not (paper recommends unfreezing them)
        inplace: whether to create a new layer and copy weights (slow) or refer to existing weights

    returns:
        adapted_layer: the AdaptedTransformerLayer version of the supplied TransformerLayer
    """

    adapted_layer = AdaptedTransformerLayer(
        layer.num_heads, layer.dim, layer.hidden_dim, adapter_dim, trainable=True
    )
    adapted_layer.build((None, layer.dim))

    if inplace:

        # Layer.__setattr__ appends values to Layer._layers, so we want to get rid of the non-adapter layers first
        adapted_layer._layers = adapted_layer._layers[:2]

        adapted_layer.multi_head_attention = layer.multi_head_attention
        adapted_layer.ff1 = layer.ff1
        adapted_layer.ff2 = layer.ff2
        adapted_layer.norm1 = layer.norm1
        adapted_layer.norm2 = layer.norm2

    else:

        adapted_layer.multi_head_attention.set_weights(
            layer.multi_head_attention.get_weights()
        )
        adapted_layer.ff1.set_weights(layer.ff1.get_weights())
        adapted_layer.ff2.set_weights(layer.ff2.get_weights())
        adapted_layer.norm1.set_weights(layer.norm1.get_weights())
        adapted_layer.norm2.set_weights(layer.norm2.get_weights())

    if freeze_transformer:
        adapted_layer.multi_head_attention.trainable = False
        adapted_layer.ff1.trainable = False
        adapted_layer.ff2.trainable = False

    if freeze_norms:
        adapted_layer.norm1.trainable = False
        adapted_layer.norm2.trainable = False

    if freeze_adapters:
        adapted_layer.adapter1.trainable = False
        adapted_layer.adapter2.trainable = False

    return adapted_layer


def adapt_encoder(
    encoder: Encoder,
    adapter_dim: int = 8,
    freeze_transformers: bool = True,
    freeze_adapters: bool = False,
    freeze_norms: bool = False,
    inplace: bool = False,
) -> Encoder:
    """
    Replaces all TransformerLayers of the given Encoder with AdaptedTransformerLayers

    See the doc of `adapt_transformer_layer` for more info
    """
    if inplace:
        adapted_encoder = encoder
    else:
        adapted_encoder = Encoder(**encoder.get_config())
        adapted_encoder.build((None, encoder.dim))

    for i, layer in enumerate(encoder.layers):
        adapted_encoder.layers[i] = adapt_transformer_layer(
            layer,
            adapter_dim,
            freeze_transformers,
            freeze_adapters,
            freeze_norms,
            inplace,
        )
    return adapted_encoder


def adapt_model(
    model: tf.keras.Model,
    adapter_dim: int = 8,
    mask: bool = True,
    seg_ids: bool = False,
    freeze_embeddings: bool = True,
    freeze_encoder: bool = True,
    freeze_adapters: bool = False,
    freeze_norms: bool = False,
    inplace: bool = False,
) -> tf.keras.Model:
    """
    Replaces all TransformerLayers of the given bertfam keras Model with AdaptedTransformerLayers

    See the doc of `adapt_transformer_layer` for more info
    """

    if inplace:
        adapted_model = model
    else:
        adapted_model = tf.keras.models.clone_model(model)
        adapted_model.set_weights(model.get_weights())

    x = adapted_model.input

    if mask:
        if seg_ids:
            x, mask, seg_ids = x
        else:
            x, mask = x
            seg_ids = None
    elif seg_ids:
        x, seg_ids = x
        mask = None

    for layer in adapted_model.layers:
        if isinstance(layer, Encoder):
            # since we cloned the model if not inplace, remaining things can be done inplace
            layer = adapt_encoder(
                layer, adapter_dim, freeze_encoder, freeze_adapters, freeze_norms, True
            )
        if isinstance(layer, Embeddings) and freeze_embeddings:
            layer.trainable = False
            x = layer(x, seg_ids=seg_ids)
        elif isinstance(layer, Encoder):
            x = layer(x, mask=mask)
        elif not isinstance(layer, tf.keras.layers.InputLayer):
            x = layer(x)

    return tf.keras.Model(adapted_model.input, x)
