import tensorflow as tf
from typing import List, Optional


def create_attention_model(
    model: tf.keras.models.Model,
    attn_from: Optional[List[int]] = None,
    encoder_layer_name: Optional[str] = "seq_output",
) -> tf.keras.models.Model:
    """create a keras model that outputs attention from encoder of `model`

    args:
        model : bertfam model
        attn_from : which transformer layers to extract attention from
        encoder_layer_name : name in `model` of the encoder layer
    returns:
        model : model. when `predict` is called, model returns a list of tensors
        of shape [batch_size,num_heads,num_tokens,num_tokens]. One tensor is
        returned for each transformer specified in `attn_from`, and num_heads
        is the number of heads in the corresponding transformer.

    """
    encoder_layers = model.get_layer(encoder_layer_name).layers
    if attn_from is None:
        attn_from = range(len(encoder_layers))
    transformers = [encoder_layers[i] for i in attn_from]
    outputs = []
    for t in transformers:
        outputs.append(t.multi_head_attention.output[1])
    return tf.keras.models.Model(model.input, outputs)


if __name__ == "__main__":
    try:
        import matplotlib
    except ImportError:
        print("#" * 80)
        print(
            "matplotlib is necessary for this example (but not the rest of bertfam), install it and try again"
        )
        print("#" * 80)
        raise
    matplotlib.use("TkAgg")
    import matplotlib.pyplot as plt
    import numpy as np
    from bertfam import load_tf_model

    model = load_tf_model(
        seq_output=True, pooled_output=False, mlm_output=False, nsp_output=False
    )
    attn_model = create_attention_model(model)

    X = np.array([[100, 200, 300, 400], range(4000, 4004, 1)])

    y = attn_model.predict(X)
    # sample_num is which of the examples in the batch, e.g. < batch_size
    # t_num is the transformer to get attention from, e.g < len(encoder.layers)
    # head_num is which attention head to read, e.g. < num_heads for the given transformer
    for sample_num, t_num, head_num in [[0, 0, 0], [0, 5, 2], [1, 0, 0], [1, 5, 2]]:
        tokens = [token_id for token_id in X[sample_num]]
        plt.figure()
        plt.imshow(y[t_num][sample_num, head_num])
        plt.colorbar()
        plt.title(
            f"sample {sample_num}, transformer {t_num}, attention head {head_num}"
        )
        plt.xticks(range(X.shape[1]), tokens)
        plt.xlabel("token id")
        plt.yticks(range(X.shape[1]), tokens)
        plt.ylabel("token id")
        plt.show()
