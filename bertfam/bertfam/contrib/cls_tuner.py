from typing import Optional
import tensorflow as tf
from bertfam import Embeddings
from bertfam.layers.norm import LayerNorm


class EmbeddingsWithCLS(Embeddings):
    def __init__(
        self, vocab_size: int, dim: int, max_seq_len: int, cls_id: int, **kwargs
    ):
        self.cls_id = cls_id
        super().__init__(vocab_size=vocab_size, dim=dim, max_seq_len=max_seq_len)

    def build(self, input_shape):

        self.cls_embedding = tf.keras.layers.Embedding(
            1, self.dim, name="cls_embedding", trainable=self.trainable
        )
        self.cls_embedding.build(input_shape)

        super().build(input_shape)

        self.token_embeddings = tf.keras.layers.Embedding(
            self.vocab_size, self.dim, name="token_embeddings", trainable=self.trainable
        )
        self.token_embeddings.build(input_shape)
        input_shape = self.token_embeddings.compute_output_shape(input_shape)

        self.pos_embeddings = tf.keras.layers.Embedding(
            self.max_seq_len, self.dim, name="pos_embeddings", trainable=self.trainable
        )
        self.pos_embeddings.build(input_shape)
        self.seg_embeddings = tf.keras.layers.Embedding(
            2, self.dim, name="seg_embeddings", trainable=self.trainable
        )
        self.seg_embeddings.build(input_shape)
        self.norm = LayerNorm(self.dim, trainable=self.trainable)
        self.norm.build(input_shape)
        self.dropout = tf.keras.layers.Dropout(0.1)

        return super().build(input_shape)

    def get_config(self):
        config = super().get_config()
        config["cls_id"] = self.cls_id
        return config

    def call(self, X: tf.Tensor, **kwargs) -> tf.Tensor:
        """
        args:
            X: tensor of shape (batch_size, seq_len)
            :param **kwargs:
        """
        batch_size = tf.shape(X)[0]
        seq_len = tf.shape(X)[1]

        pos_ids = tf.reshape(tf.tile(tf.range(seq_len), [batch_size]), (batch_size, -1))

        cls_mask = tf.cast(tf.equal(X, self.cls_id), tf.float32)
        X_cls = tf.multiply(self.cls_embedding(tf.zeros_like(X)), cls_mask)

        X_pos = self.pos_embeddings(pos_ids)  # (batch_size, seq_len, dim)
        X_tokens = tf.multiply(
            self.token_embeddings(X), 1 - cls_mask
        )  # (batch_size, seq_len, dim)

        if seg_ids is None:
            seg_ids = tf.zeros_like(pos_ids)

        X_seg = self.seg_embeddings(seg_ids)

        X = X_pos + X_tokens + X_seg + X_cls

        X = self.norm(X)
        X = self.dropout(X)

        return X


def modify_embeddings(embeddings: Embeddings, cls_id: int) -> EmbeddingsWithCLS:

    modified = EmbeddingsWithCLS(
        embeddings.vocab_size, embeddings.dim, embeddings.max_seq_len, cls_id
    )
    modified.build((None,))

    modified._layers = modified._layers[:1]

    modified.token_embeddings = embeddings.token_embeddings
    modified.pos_embeddings = embeddings.pos_embeddings
    modified.seg_embeddings = embeddings.seg_embeddings
    modified.norm = embeddings.norm
    modified.dropout = embeddings.dropout

    return modified


def modify_model(
    model: tf.keras.Model, cls_id: int, mask: bool = True, seg_ids: bool = False
) -> tf.keras.Model:

    x = model.input

    if mask:
        if seg_ids:
            x, mask, seg_ids = x
        else:
            print(x)
            x, mask = x
            seg_ids = None
    elif seg_ids:
        x, seg_ids = x
        mask = None

    for layer in model.layers:
        if isinstance(layer, Embeddings):
            layer = modify_embeddings(layer, cls_id)
            x = layer(x, seg_ids=seg_ids)
        elif not isinstance(layer, tf.keras.layers.InputLayer):
            x = layer(x, mask=mask)

    return tf.keras.Model(model.input, x)
