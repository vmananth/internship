from typing import Union, List
import tensorflow as tf
from bertfam import Embeddings, Tokenizer
from bertfam.model import build_bert, EMBEDDINGS, ENCODER, POOLED_OUTPUT


INFERENCE_FC = "inference_fc"


class InferenceEmbeddings(Embeddings):
    def build(self, input_shape):
        with tf.variable_scope("embeddings"):
            self.cls_embeddings = tf.keras.layers.Embedding(
                1, self.dim, name="cls_embedding", trainable=self.trainable
            )
            self.cls_embeddings.build(input_shape)
        super().build(input_shape)

    def call(self, X: Union[tf.Tensor, List[tf.Tensor]], **kwargs) -> tf.Tensor:
        """
        args:
            X: tensor of shape (batch_size, seq_len)
        """
        X = super().call(X, **kwargs)
        batch_size = tf.shape(X)[0]
        X = tf.keras.layers.Lambda(lambda x: x[:, 1:, :], name="select_noncls")(X)
        cls_X = self.cls_embeddings(tf.zeros((batch_size, 1), dtype=tf.int32))
        X = tf.keras.layers.concatenate([cls_X, X], axis=1)
        return X


def build_model(n_outputs: int = 2) -> tf.keras.Model:

    model = build_bert(
        attention_mask=True,
        seg_ids=True,
        pooled_output=True,
        seq_output=False,
        num_layers=12,
        num_heads=12,
        dim=768,
        hidden_dim=768,
        max_seq_len=128,
        vocab=Tokenizer.from_pretrained_tf_model().vocab,
    )
    orig_embeddings = model.get_layer(EMBEDDINGS)

    embeddings = InferenceEmbeddings(**orig_embeddings.get_config())

    tokens_input, mask_input, seg_input = model.input

    X = embeddings([tokens_input, seg_input])

    encoder = model.get_layer(ENCODER)

    X = encoder([X, mask_input])

    X = tf.keras.layers.Lambda(lambda _x: _x[:, 0, :], name="select_cls")(X)

    pooler = model.get_layer(POOLED_OUTPUT)

    X = pooler(X)
    X = tf.layers.Dense(n_outputs, activation="softmax", name=INFERENCE_FC)(X)

    return tf.keras.Model(model.inputs, X, name="bertfam_inference")


def from_pooled_model(
    model: tf.keras.Model, n_outputs: int = 2, dropout: float = 0.5
) -> tf.keras.Model:

    embeddings = model.get_layer(EMBEDDINGS)

    # new_embeddings = build_embeddings(model.get_layer(EMBEDDINGS))

    new_embeddings = InferenceEmbeddings(**embeddings.get_config())

    X = new_embeddings([model.inputs[0], model.inputs[2]])
    X = model.get_layer(ENCODER)([X, model.inputs[1]])
    X = tf.keras.layers.Lambda(lambda _x: _x[:, 0, :], name="select_cls")(X)
    X = model.get_layer(POOLED_OUTPUT)(X)
    X = tf.keras.layers.Dense(n_outputs, activation="softmax", name=INFERENCE_FC)(X)
    X = tf.keras.layers.Dropout(dropout)(X)

    new_model = tf.keras.Model(model.inputs, X)

    return new_model


def build_embeddings(embeddings: Embeddings) -> tf.keras.Model:

    tokenizer = Tokenizer.from_embeddings(embeddings)
    cls_id = tokenizer.token2id[tokenizer.cls_token]

    cls_embedding = tf.keras.layers.Embedding(1, embeddings.dim, name="cls_embedding")
    cls_embedding.build((None,))

    cls_weight = embeddings.get_weights()[0][cls_id]
    cls_embedding.set_weights([cls_weight.reshape((1, -1))])

    X = embeddings.input
    X = embeddings(X)
    print(embeddings.name)

    return tf.keras.Model(embeddings.input, X)
    seg_ids = None

    if isinstance(X, list):
        if len(X) == 1:
            X = X[0]
        elif len(X) == 2:
            X, seg_ids = X

    def _make_pos_ids(x):
        batch_size = tf.shape(x)[0]
        seq_len = tf.shape(x)[1]
        return tf.reshape(tf.tile(tf.range(seq_len), [batch_size]), (batch_size, -1))

    pos_ids = tf.keras.layers.Lambda(_make_pos_ids)(X)

    def _make_zeros(x):
        return tf.zeros_like(x)

    if seg_ids is None:
        seg_ids = tf.keras.layers.Lambda(_make_zeros)(X)

    cls_mask = tf.cast(tf.equal(X, cls_id), tf.float32)
    cls_mask = tf.reshape(
        tf.tile(cls_mask, [embeddings.dim, 1]), (1, -1, embeddings.dim)
    )

    X_tokens = embeddings.token_embeddings(X)

    def _remove_cls(x):
        x = tf.multiply(x, 1 - cls_mask)
        return x

    def _remove_noncls(x):
        x = tf.multiply(x, cls_mask)
        return x

    X_cls = cls_embedding(tf.keras.layers.Lambda(_make_zeros)(X))

    X_tokens = tf.keras.layers.Lambda(_remove_cls)(X_tokens)
    X_cls = tf.keras.layers.Lambda(_remove_noncls)(X_cls)
    X_pos = embeddings.pos_embeddings(pos_ids)
    X_seg = embeddings.seg_embeddings(seg_ids)

    X = tf.keras.layers.Add()([X_tokens, X_cls, X_seg, X_pos])

    X = embeddings.norm(X)
    X = embeddings.dropout(X)

    return tf.keras.Model(embeddings.input, X, name="inference_embeddings")
