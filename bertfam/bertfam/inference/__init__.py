from bertfam.inference.model import from_pooled_model

__all__ = ["from_pooled_model"]
