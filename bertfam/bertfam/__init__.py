import warnings
import logging

warnings.filterwarnings("ignore", category=DeprecationWarning)
import tensorflow as tf

tf.logging.set_verbosity(logging.ERROR)
from bertfam.layers.encoder import Encoder
from bertfam.layers.embeddings import Embeddings
from bertfam.layers.norm import LayerNorm
from bertfam.layers.transformer import TransformerLayer
from bertfam.layers.attention import MultiHeadAttention
from bertfam.layers.activations import gelu, approx_gelu
from bertfam.layers.mlm import MLM
from bertfam.tokenizer import Tokenizer, BertTokenizer, BertBertTokenizer
from bertfam.model import (
    load_pretrained_tf_model,
    load_tf_model,
    load_model,
    set_pretrained_weights,
)


logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.INFO)


__all__ = [
    "BertTokenizer",
    "BertBertTokenizer",
    "Encoder",
    "Embeddings",
    "LayerNorm",
    "TransformerLayer",
    "MultiHeadAttention",
    "MLM",
    "Tokenizer",
    "gelu",
    "approx_gelu",
    "load_pretrained_tf_model",
    "load_model",
]

__version__ = "1.1.3"
