from bertfam import load_pretrained_tf_model, load_model, BertBertTokenizer
from bertfam_qa.model import from_bertfam
from bertfam_qa.data import json_to_tf_dataset
from dse_qa_utils.datasets.squad import ALL


# BERTFAM_MODEL = "/Users/dmc073/idea/bertfam/models/bertfam-base-uncased-pooled.hdf5"


def main():

    # for google's pretrained model
    model = load_pretrained_tf_model(pooled_output=True, seg_ids=True)

    # for bertfam pretrained model
    # model = load_model(BERTFAM_MODEL)

    tokenizer = BertBertTokenizer.from_model(model)

    model = from_bertfam(model)

    train = json_to_tf_dataset(ALL, tokenizer, batch_size=1)

    model.compile(optimizer="adam", loss="categorical_crossentropy")

    model.summary()

    model.fit(train, steps_per_epoch=10)


if __name__ == "__main__":
    main()
