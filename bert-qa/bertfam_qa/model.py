from tensorflow.python.keras import Model
from tensorflow.python.keras.layers import Dense, Lambda
from tensorflow.python.keras.activations import softmax
import tensorflow as tf
from bertfam.model import ENCODER, POOLED_OUTPUT


def from_bertfam(model: Model) -> Model:
    """
    Converts a bertfam tf.keras.Model model into a SQuAD-style QA model

    model must have both the "sequence output" and "pooled output"

    There are three outputs:
        - shape (2,) scores for (answerable, not answerable)
        - shape (seq_len,) scores for start of answer
        - shape (seq_len,) scores for end of answer
    """

    # get seq and pooled outputs from model
    seq = model.get_layer(ENCODER).output  # (batch_size, seq_len, dim)
    pooled = model.get_layer(POOLED_OUTPUT).output  # (batch_size, dim)

    # one output for answerable/not answerable
    answerable = Dense(2, activation="softmax", name="answerable_scores")(
        pooled
    )  # (batch_size, 2)

    # one output for predicting the answer start index
    start_index_pred = Dense(1, name="answer_start_logit")(
        seq
    )  # (batch_size, seq_len, 1)
    start_index_pred = Lambda(
        lambda x: softmax(tf.squeeze(x, axis=-1)), name="answer_start_scores"
    )(
        start_index_pred
    )  # (batch_size, seq_len)

    # one output for predicting the answer end index
    end_index_pred = Dense(1, name="answer_end_logit")(seq)  # (batch_size, seq_len, 1)
    end_index_pred = Lambda(
        lambda x: softmax(tf.squeeze(x, axis=-1)), name="answer_end_scores"
    )(
        end_index_pred
    )  # (batch_size, seq_len)

    # define the model with the same inputs as the original model and the new outputs
    model = Model(model.inputs, [answerable, start_index_pred, end_index_pred])

    return model
