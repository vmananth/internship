from dse_qa_utils import load_examples
import tensorflow as tf
from bertfam import BertBertTokenizer
import numpy as np


def json_to_tf_dataset(
    filename: str, tokenizer: BertBertTokenizer, train: bool = True, batch_size: int = 2
) -> tf.data.Dataset:
    """
    This function takes a qa examples jsonl file (one obtained from dse_qa_utils.save_examples()) and turns it into a tf.data.Dataset for use to train/eval a bertfam qa model
    """

    tokenizer.pad = True
    n = tokenizer.max_seq_len

    def data_gen():
        # noinspection PyArgumentList
        data = load_examples(filename)
        for ex in data:
            # start index is the sum of the mask input
            start_index = sum(
                tokenizer((ex.question, ex.text_context[: ex.answer_start]))[1][0]
            )
            end_index = start_index + len(tokenizer.tokenize_text(ex.answer))
            answerable = ex.answerable

            # if mask is full then answer is probably after max_seq_len, so we say it's unanswerable
            if start_index == tokenizer.max_seq_len:
                start_index = 0
                end_index = 0
                answerable = False

            X = tokenizer((ex.question, ex.text_context))
            X = tuple(x[0] for x in X)
            y_answerable = np.zeros(2)
            y_answerable[int(answerable)] = 1
            y_start = np.zeros(n)
            y_start[start_index] = 1
            y_end = np.zeros(n)
            y_end[end_index] = 1
            y = (y_answerable, y_start, y_end)
            yield X, y

    dataset = tf.data.Dataset.from_generator(
        data_gen,
        output_types=((tf.int32, tf.int32, tf.int32), (tf.int32, tf.int32, tf.int32)),
        output_shapes=(((n,), (n,), (n,)), ((2,), (n,), (n,))),
    )

    if train:
        dataset = dataset.shuffle(100).repeat()

    dataset = dataset.batch(batch_size)

    return dataset
