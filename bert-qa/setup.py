from setuptools import setup, find_packages
import os
import time

package_init = os.path.join(os.path.dirname(__file__), "bertfam_qa", "__init__.py")
readme_file = os.path.join(os.path.dirname(__file__), "README.md")

with open(readme_file) as f:
    readme = f.read()


VERSION = None
with open(package_init) as f:
    for line in f:
        if "__version__" in line:
            VERSION = line.split(" = ")[-1].replace('"', "").strip()
            break
if VERSION is None:
    raise ValueError("bertfam_qa.__version__ not found")


if os.environ.get("PYTHON_PACKAGING_ENV") == "development":
    VERSION = f"{VERSION}.dev.{time.strftime('%Y%m%d%H%M%S', time.gmtime())}"


setup(
    name="amfam.bertfam-qa",
    version=VERSION,
    description="QA models using bertfam",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://bitbucket.amfam.com/projects/SDA/repos/bertfam-qa/browse",
    maintainer="DSE",
    maintainer_email="SDA-ML-RESEARCH-DL@amfam.com",
    packages=find_packages(exclude=["tests"]),
    include_package_data=True,
    zip_safe=False,
    install_requires=["amfam.bertfam", "amfam.dse_qa_utils"],
)
