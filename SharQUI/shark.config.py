from ui.config import amfamlabs_config
from ui.config import dto_aws_config
from ui.config import local_config

class Config(object):
    DEBUG = True
    TESTING = False

class Local(Config):
    DEBUG = True
    DATABASE = local_config.MYSQL_LOCAL
    BERT_SERVER = local_config.BERT_SERVER_IP
    BACKEND_SERVER = local_config.QA_BACKEND_SERVER
    BACKEND_API = local_config.QUERY_ENDPOINT

class Development(Config):
    DEBUG = True
    DATABASE = dto_aws_config.MYSQL_DTO_AWS
    BERT_SERVER = dto_aws_config.BERT_SERVER_IP
    BACKEND_SERVER = dto_aws_config.QA_BACKEND_SERVER
    BACKEND_API = dto_aws_config.QUERY_ENDPOINT

class Production(Config):
    DEBUG = True
    DATABASE = amfamlabs_config.MYSQL_AMFAM_LAB
    BERT_SERVER = amfamlabs_config.BERT_SERVER_IP
    BACKEND_SERVER = amfamlabs_config.QA_BACKEND_SERVER
    BACKEND_API = amfamlabs_config.QUERY_ENDPOINT
    WAIT_API = amfamlabs_config.WAIT_ENDPOINT