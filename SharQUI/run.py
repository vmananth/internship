from ui import app
from ui.config import config_set
import sys

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print("Pass Config Env as argument - 1.local,  2.dto_aws,  3.amfamlab_aws   !!!")
        exit()
    else:
        app = config_set.set_env_variables(app, 'amfamlab_aws')
        for a in app.config:
            print(a)
        app.run(debug=True,host='0.0.0.0')
