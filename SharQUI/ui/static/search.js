$("#formoid").submit(function (event) {


    /* stop form from submitting normally */

            $(document).ajaxStart(function () {
                $("#overlay").css("display", "block");
                $("#search_submit").prop('disabled', true);
                $("#main").css("pointer-events", "none");
            });
            $(document).ajaxComplete(function () {
                $("#overlay").css("display", "none");
                 $("#search_submit").prop('disabled', false);
                 $("#main").css("pointer-events", "auto");
            });


            // $(document).ajaxStart(function () {
            //     $("#overlay").css("display", "block");
            // });
            // $(document).ajaxComplete(function () {
            //     $("#overlay").css("display", "none");
            // });

    event.preventDefault();

    /* get the action attribute from the <form action=""> element */
    var question = {
        cont: $('.nav-tabs .active').attr('data-value'),
        query: $('#text_query').val()
    };


    $.ajax({
        url: "/response/",
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        success: function (resp) {
            console.log(resp.data);
            updated_html = resp.html;
            updated_answers = resp.answer_list;
            updated_context = resp.context_list;
            similar_questions = resp.similar_questions;
            best_answer = resp.best_answer;
            full_response = resp.response;

            $('#qa').show();
            $('#context-section').show();

            $('h1#most_probable_answer').html("");
            $('h1#most_probable_answer').append(best_answer);

            $('#main_context').html("");
            var flag = true;
            for (context in updated_context) {
                if(flag){
                $('#main_context').append('<a id="context2" class="list-group-item list-group-item-action active">' + updated_context[context] + '</a>');
                flag = false
                }
                else {
                    $('#main_context').append('<a id="context2" class="list-group-item list-group-item-action">' + updated_context[context] + '</a>');
                }
            }


            var i = 0;
            $('#main_context2').html("");

            function get_answer(value) {
                var answers = '';
                active = true
                JSON.parse(value).forEach(function (element) {
                    if(active) {
                     answers = answers + '        <li class="list-group-item active">'+ element +'</li>\n'
                        active = false
                    }
                    else {
                        answers = answers + '        <li class="list-group-item">'+ element +'</li>\n'
                    }
                });
                return answers;
            }

            for (var index in similar_questions) {
                $('#main_context2').append('<div class="panel-group list-group-item" role="tablist">\n' +
                    '  <div class="panel panel-default">\n' +
                    '    <div class="panel-heading" role="tab" id="collapseListGroupHeading1">\n' +
                    '      <h4 class="panel-title">\n' +
                    '        <a class="collapsed" data-toggle="collapse" href="#collapseListGroup'+i+'" aria-expanded="false" aria-controls="collapseListGroup1" style="color: #2e5790;">\n' +
                             similar_questions[index]['question']+
                    '        </a>\n' +
                    '      </h4>\n' +
                    '    </div>\n' + '<!-- Default unchecked -->\n' +
                    '    <div id="collapseListGroup'+i+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseListGroupHeading1">\n' +
                    '      <ul class="list-group">\n' +
                            get_answer(similar_questions[index]['answer']) +
                    '      </ul>\n' +
                    // '      <div class="panel-footer">Footer</div>\n' +
                    '    </div>\n' +
                    '  </div>\n' +
                    '</div>');
                i = i + 1;
            }



            $('#answers_group').html("");
            for (answer in updated_answers) {
                // $('#answers_group').append('<li class="list-group-item">' + updated_answers[answer] + '<label class="switch "><input type="checkbox" class="primary" name="type" value="' + updated_answers[answer] +'"> <span class="slider round"></span> </label></li>');
                $('#answers_group').append('<div class="list-group">\n' +
                    '  <a id = "link" href="#" onclick="return check()" class="list-group-item">' + updated_answers[answer] +
                    '<label class="switch "><input type="checkbox" class="primary" name="type" value="' + updated_answers[answer] +'"> <span class="slider round"></span> </label></li>\n' +
                    '  </a>\n' +
                    '</div>')
            }


            // jQuery("div#response").html(updated_html);

            // var app = document.querySelector('#response');
            // app.innerHTML = updated_html
            //
            // // html2 = $.parseHTML(updated_html);
            // // $("div#response").append(html2);

            // var html2 = DecodeHTML(updated_html);

            $('div#response').html("");
            $('div#response').append(updated_html);

            $('div#current-html').html("");
            $('div#current-html').append(updated_html);

            // answer_highlight = best_answer.removeStopWords().split(' ');

            // for(i in answer_highlight) {
            //     document.getElementById("response").innerHTML = document.getElementById("response").innerHTML.replace(answer_highlight[i], '<style>b {      font-size: 130%;     }</style></style><strong><b><mark style="background-color: #99ff99">' + answer_highlight[i] + '</mark></b></strong>');
            // }


            // for (i in updated_answers) {
            //
            //     // split_string = updated_answers[i].split(" ")
            //     // end = split_string.length - 1
            //     //
            //     // // var r = new RegExp(split_string[0] + "(.*)" + split_string[end]);
            //     // // while(document.getElementById("response").innerHTML.match(r)) {
            //     // //     end = end - 1
            //     // //     r = new RegExp(split_string[0] + "(.*)" + split_string[end]);
            //     // // }
            //     //
            //     // var r = new RegExp(split_string[0].replace(/[^a-zA-Z ]/g, "") + "(.*)" + split_string[end].replace(/[^a-zA-Z ]/g, ""));
            //     if(window.find(updated_answers[i])) {
            //      document.getElementById("response").innerHTML = document.getElementById("response").innerHTML.replace(updated_answers[i], '<mark style="background-color: #99ff99">' + updated_answers[i] + '</mark>');
            //     }
            //
            // }

            $('div#full').html("");
            $('div#full').data(full_response)


        },
        data: JSON.stringify(question)
    });
});