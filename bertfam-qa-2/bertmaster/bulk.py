import qaclasses36 as qaclasses
from keras.models import Model, model_from_json
import tensorflow as tf
import json
import numpy as np
# import sys
import keras
from keras_bert import load_trained_model_from_checkpoint, Tokenizer
from keras.layers import Dense
from keras.activations import softmax
from keras.callbacks import ModelCheckpoint

import logging


config_path = 'test/bert_config.json'
dict_path = 'test/vocab.txt'
# input_data = 'train-v2.0.json'#'dev-v2.0.json'#'train-v2.0.json'#'dev-v2.0.json'#'test_sample_squad.json'#'dev-v2.0.json'
# intermediate_filepath = 'weights-bl_%s-{epoch:02d}.hdf5'%bert_layer_num
# intermediate_checkpoint = ModelCheckpoint(intermediate_filepath)
input_data = 'test/sample.json'





with open(input_data,'r') as f:
    d = json.load(f)


r = qaclasses.parse_squad(d)

ids,segment_ids,input_mask,start_idx,end_idx = [np.array(_) for _ in zip(*[x.encode() for x in r])]

np.save('preprocessed/train_ids.npy',ids)
np.save('preprocessed/train_segment_ids.npy',segment_ids)
np.save('preprocessed/train_input_mask.npy',input_mask)
np.save('preprocessed/train_start_idx.npy',start_idx)
np.save('preprocessed/train_end_idx.npy',end_idx)
