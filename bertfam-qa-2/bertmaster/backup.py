# import unittest
# from bertmaster import modeling
import random
import sys, os
os.path.dirname(sys.executable)
from bertfam import load_pretrained_tf_model
import numpy as np
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Model
from tensorflow.keras.activations import softmax
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import multi_gpu_model
from sklearn.model_selection import train_test_split

import sys
import logging

version = 'v2'


bert_layer_num = 3
bln = bert_layer_num

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',
        filename='%s/%s_squad_training-bl_%s.log'%(version,version,bert_layer_num),level=logging.INFO)
base_path = '/home/ubuntu/.keras/bertfam/models/bert-base-uncased'
config_path = base_path + '/bert_config.json'
checkpoint_path = base_path + '/bert_model.ckpt'
dict_path = base_path + '/vocab.txt'
input_data = 'train-v2.0.json'#'dev-v2.0.json'#'train-v2.0.json'#'dev-v2.0.json'#'test_sample_squad.json'#'dev-v2.0.json'
intermediate_filepath = '%s/%s_weights-bl_%s-{epoch:02d}.hdf5'%(version,version,bert_layer_num)
intermediate_checkpoint = ModelCheckpoint(intermediate_filepath)

model = load_pretrained_tf_model()
logging.info('bert loaded')
encoding_out = model.layers[bert_layer_num].output

def softmx(x):
    return softmax(x,axis=1)

start_layer = Dense(1,activation=softmx)
start_out = start_layer(encoding_out)
end_layer = Dense(1,activation=softmx)
end_out = end_layer(encoding_out)

squad_model = Model(model.inputs,[start_out,end_out])

# print('loading input')
# ids = np.load('preprocessed/train_ids.npy')
# segment_ids = np.load('preprocessed/train_segment_ids.npy')
# input_mask = np.load('preprocessed/train_input_mask.npy')
# start_idx = np.load('preprocessed/train_start_idx.npy')
# end_idx = np.load('preprocessed/train_end_idx.npy')
#
#
# target_shape = list(ids.shape) + [1]
# start_ids = np.zeros(target_shape,dtype=int)
# end_ids = np.zeros(target_shape,dtype=int)
# for i,idx in enumerate(start_idx):
#     start_ids[i,idx,0] = 1
#
# for i,idx in enumerate(end_idx):
#     end_ids[i,idx,0] = 1

model_json = squad_model.to_json()

with open("%s/%s_squad_model-bl_%s.json"%(version,version,bert_layer_num), "w") as json_file:
    json_file.write(model_json)

for layer in squad_model.layers:
    if layer.count_params() > 0:
        layer.trainable = True


optimizer = Adam(lr=1e-5)
parallel_squad_model = multi_gpu_model(squad_model,gpus=4)
parallel_squad_model.compile(loss='binary_crossentropy',optimizer=optimizer)
logging.info('squad model ready using bert layer %s, %s'%(bln,bert_layer_num))

#max batch size on 1 gpu is 6, using 4 so batch size is 24


def batch_generator(ids_train, segment_train, start_index_train, end_index_train, batch_size = 4):
    indices = np.arange(len(ids_train))
    batch=[]
    while True:
            # it might be a good idea to shuffle your data before each epoch
            np.random.shuffle(indices)
            for i in indices:
                batch.append(i)
                if len(batch)==batch_size:
                    yield [ids_train[batch], segment_train[batch]], [start_index_train[batch], end_index_train[batch]]
                    batch=[]

ids_train = np.load('preprocessed_data/train/ids_train.npy')
segment_train = np.load('preprocessed_data/train/segment_train.npy')
input_mask_train = np.load('preprocessed_data/train/input_mask_train.npy')
start_index_train = np.load('preprocessed_data/train/start_index_train.npy')
end_index_train = np.load('preprocessed_data/train/end_index_train.npy')


ids_test = np.load('preprocessed_data/val/ids_test.npy')
segment_test = np.load('preprocessed_data/val/segment_test.npy')
input_mask_test = np.load('preprocessed_data/val/input_mask_test.npy')
start_index_test = np.load('preprocessed_data/val/start_index_test.npy')
end_index_test = np.load('preprocessed_data/val/end_index_test.npy')


train_generator = batch_generator(ids_train, segment_train, start_index_train, end_index_train, batch_size = 4)


print('fitting model')
# parallel_squad_model.fit([ids,segment_ids],[start_ids,end_ids],batch_size=4,epochs=4)
parallel_squad_model.fit_generator(train_generator, epochs=4, steps_per_epoch = 4)

print('saving model')
# serialize model to JSON
model_json = squad_model.to_json()
with open("result.json", "w") as json_file:
    json_file.write(model_json)

print('saving weights')
output_filepath = '%s/%s_weights-bl_%s-02.hdf5'%(version,version,bert_layer_num)
squad_model.save_weights(output_filepath)

print('weights saved', output_filepath)

output_filepath = '%s/%s_full-bl_%s-02.hdf5'%(version,version,bert_layer_num)
print('model saved')
squad_model.save_weights(output_filepath)



print('training complete')

# if True:
#     x = squad_model.predict([ids,segment_ids])
#     ys = x[0][0]
#     ye = x[1][0]
#
#     for y in zip(zip(*x),start_ids,end_ids):
#         print(y[0][0].argmax(),y[0][1].argmax(),y[1].argmax(),y[2].argmax())


    ####scratch below, works above

    # r[2].encode()
    # ids,segment_ids,input_mask,start_idx,end_idx = r[2].encode()
    # x = squad_model.predict([[np.array(ids)],[np.array(segment_ids)]])
    # ys = x[0]
    # ye = x[1]
    #
    # embedding = model.predict([[np.array(ids)],[np.array(segment_ids)]])
