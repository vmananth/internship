import numpy as np

def batch_generator(ids_train, segment_train, start_index_train, end_index_train, batch_size = 4):
    indices = np.arange(len(ids_train))
    batch=[]
    while True:
            # it might be a good idea to shuffle your data before each epoch
            np.random.shuffle(indices)
            for i in indices:
                batch.append(i)
                if len(batch)==batch_size:
                    yield [ids_train[batch], segment_train[batch]], [start_index_train[batch], end_index_train[batch]]
                    batch=[]

ids_train = np.load('preprocessed_data/train/ids_train.npy')
segment_train = np.load('preprocessed_data/train/segment_train.npy')
input_mask_train = np.load('preprocessed_data/train/input_mask_train.npy')
start_index_train = np.load('preprocessed_data/train/start_index_train.npy')
end_index_train = np.load('preprocessed_data/train/end_index_train.npy')


ids_test = np.load('preprocessed_data/val/ids_test.npy')
segment_test = np.load('preprocessed_data/val/segment_test.npy')
input_mask_test = np.load('preprocessed_data/val/input_mask_test.npy')
start_index_test = np.load('preprocessed_data/val/start_index_test.npy')
end_index_test = np.load('preprocessed_data/val/end_index_test.npy')


train_generator = batch_generator(ids_train, segment_train, start_index_train, end_index_train, batch_size = 4)
val = next(train_generator)
print('done')