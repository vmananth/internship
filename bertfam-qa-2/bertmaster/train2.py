# import unittest
# from bertmaster import modeling

import numpy as np
import keras
from keras.models import Sequential
import random
import sys, os
os.path.dirname(sys.executable)
import numpy as np
from sklearn.model_selection import train_test_split

import sys
import logging

print('loading input')
ids = np.load('preprocessed/train_ids.npy')
segment_ids = np.load('preprocessed/train_segment_ids.npy')
input_mask = np.load('preprocessed/train_input_mask.npy')
start_idx = np.load('preprocessed/train_start_idx.npy')
end_idx = np.load('preprocessed/train_end_idx.npy')
print('loaded input')
print('SHape of ids: ', ids.shape)
print('Shape of Segment ids: ', segment_ids.shape)
print('Shape of input_mask : ', input_mask.shape)
print('Shape of start index : ', start_idx.shape)
print('Shape of start end index : ', end_idx.shape)


target_shape = list(ids.shape) + [1]
start_ids = np.zeros(target_shape,dtype=int)
end_ids = np.zeros(target_shape,dtype=int)
for i,idx in enumerate(start_idx):
    start_ids[i,idx,0] = 1

for i,idx in enumerate(end_idx):
    end_ids[i,idx,0] = 1

ids_train, ids_test, \
segment_train, segment_test, \
input_mask_train, input_mask_test, \
start_index_train, start_index_test, \
end_index_train, end_index_test = train_test_split(ids, segment_ids, input_mask, start_ids, end_ids, test_size=0.20, random_state=42)

np.save('preprocessed_data/train/ids_train.npy', ids_train)
np.save('preprocessed_data/train/segment_train.npy', segment_train)
np.save('preprocessed_data/train/input_mask_train.npy', input_mask_train)
np.save('preprocessed_data/train/start_index_train.npy', start_index_train)
np.save('preprocessed_data/train/end_index_train.npy', end_index_train)


np.save('preprocessed_data/val/ids_test.npy', ids_test)
np.save('preprocessed_data/val/segment_test.npy', segment_test)
np.save('preprocessed_data/val/input_mask_test.npy', input_mask_test)
np.save('preprocessed_data/val/start_index_test.npy', start_index_test)
np.save('preprocessed_data/val/end_index_test.npy', end_index_test)



