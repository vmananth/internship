import json
from keras.models import model_from_json

# load json and create model
json_file = open('v2/v2_squad_model-bl_11.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("v2/v2_full-bl_11-02.hdf5")
print("Loaded model from disk")