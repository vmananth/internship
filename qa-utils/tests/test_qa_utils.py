import pytest
from dse_qa_utils import Example
from dse_qa_utils.data_structures import (
    html_to_answer_indices,
    index_within_parent,
    index_within_soup,
)
from pydantic import ValidationError
from bs4 import BeautifulSoup


def test_parse_example():
    Example(question="foobar", text_context="hello world")


def test_error_if_answer_missing():

    with pytest.raises(ValidationError):
        Example(question="foobar", html_context="", answerable=True)


def test_parse_answer():
    example = Example(
        question="foobar",
        html_context='this is the <span class="answer">answer</span>',
        answerable=True,
    )

    assert example.answer == "answer"


def test_index_within_parent():

    html = "<div>this <b>is</b> a <span>test</span></div>"
    soup = BeautifulSoup(html, features="html.parser")
    tag = soup.find("span")

    index = index_within_parent(tag)

    assert soup.text[index:] == "test"


def test_index_within_soup():

    html = "<ol><li>foobar</li><li><div><div>how are you?</div><div>this <b>is</b> a <span>test</span></div></div></li></ol>"
    soup = BeautifulSoup(html, features="html.parser")
    tag = soup.find("span")

    index = index_within_soup(tag)

    assert soup.text[index:] == "test"


def test_html_to_answer_indices():

    html = """"<div> this is <span class="answer">the answer</span></div>"""

    start, end = html_to_answer_indices(html)
    text = BeautifulSoup(html, features="html.parser").text
    assert text[start:end] == "the answer"

    html = """<h1>hello world</h1>
    this is not in a tag
    <div> this is a <b>test</b></div>
    <div> this is <span class="answer">the answer</span></div>
    and this is not answer."""

    start, end = html_to_answer_indices(html)
    text = BeautifulSoup(html, features="html.parser").text
    assert text[start:end] == "the answer"

    html = """<h1>hello world</h1>
    this is not in a tag
    <div> this is a <b>test</b></div>
    <ol>
    <li>not answer</li>
    <li>not answer</li>
    <li><span class="answer">the answer</span></li>
    </ol>
    and this is not answer."""

    start, end = html_to_answer_indices(html)
    text = BeautifulSoup(html, features="html.parser").text
    assert text[start:end] == "the answer"
