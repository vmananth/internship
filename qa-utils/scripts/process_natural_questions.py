from typing import Iterator, Optional
import os
from dse_qa_utils import Example
from dse_qa_utils.utils import write_jsonl
import pathlib
import gzip
import json
from tqdm import tqdm
import random
from dataclasses import asdict
from multiprocessing import Pool


THIS_DIR = os.path.abspath(os.path.dirname(__file__))
DATA_DIR = os.path.abspath(os.path.join(THIS_DIR, "..", "data", "natural_questions"))

rng = random.Random()


def get_raw() -> Iterator[dict]:
    filenames = sorted(list(map(str, pathlib.Path(DATA_DIR).glob("nq-*"))))

    for filename in filenames:
        with gzip.GzipFile(filename) as f:
            for line in f:
                yield json.loads(line)


def process_example(data: dict) -> Optional[Example]:

    if data["annotations"][0]["yes_no_answer"] != "NONE":
        return None
    short_answers = data["annotations"][0]["short_answers"]
    question = data["question_text"]
    q_len = len(question.split())
    if short_answers:
        short_answer = rng.choice(short_answers)
        text_tokens = [t for t in data["document_tokens"] if not t["html_token"]]
        text = ""
        start_token_i = None
        end_token_i = None
        for i, token in enumerate(text_tokens):
            if token["start_byte"] == short_answer["start_byte"]:
                start_token_i = i
            text += token["token"] + " "
            if token["end_byte"] == short_answer["end_byte"]:
                end_token_i = i
        if start_token_i is None or end_token_i is None:
            return None
        start_context = rng.randint(max(end_token_i - 512 + q_len, 0), start_token_i)
        end_context = start_context + 512 - q_len
        text = ""
        start_index = None
        end_index = None
        for i, token in enumerate(text_tokens):
            if i < start_context:
                continue
            if i > end_context:
                continue
            if i == start_token_i:
                start_index = len(text)
            text += token["token"] + " "
            if i == end_token_i:
                end_index = len(text)
        if start_index is None or end_index is None:
            return None
        # noinspection PyArgumentList
        ex = Example(
            question=question,
            answerable=True,
            text_context=text,
            answer_start=start_index,
            answer_end=end_index,
        )
        return ex
    else:
        if rng.random() > 0.75:
            text = " ".join(
                (t["token"] for t in data["document_tokens"] if not t["html_token"])
            )
            # noinspection PyArgumentList
            ex = Example(question=question, answerable=False, text_context=text)
            return ex
        else:
            return None


def main():
    with Pool() as pool:
        data = pool.imap_unordered(
            process_example, tqdm(get_raw(), total=315000), chunksize=4096
        )
        write_jsonl(
            os.path.join(DATA_DIR, "processed-all.jsonl"),
            map(asdict, filter(None, data)),
        )


if __name__ == "__main__":
    main()
