from dse_qa_utils.utils import tqdm
from dse_qa_utils.datasets.squad import get_examples, ALL
from dse_qa_utils.data_structures import save_examples


def main():

    all_squad = tqdm(get_examples())
    save_examples(ALL, all_squad)


if __name__ == "__main__":
    main()
