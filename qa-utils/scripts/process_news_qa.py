import os
import tarfile

THIS_DIR = os.path.abspath(os.path.dirname(__file__))
DATA_DIR = os.path.abspath(os.path.join(THIS_DIR, "..", "data", "news_qa"))

STORIES_TGZ = os.path.join(DATA_DIR, "cnn.tgz")
QA_TAR_GZ = os.path.join(DATA_DIR, "newsqa-data-v1.tar.gz")


def main():
    with tarfile.open(STORIES_TGZ) as f:
        print(f.list())


if __name__ == "__main__":
    main()
