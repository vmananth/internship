from typing import Iterator
from dse_qa_utils.constants import DATASETS_BUCKET
from dse_qa_utils import Example
from smart_open import open
import os
import json


RAW_TRAIN_JSON = os.path.join(DATASETS_BUCKET, "raw", "squad", "train-v2.0.json")
RAW_DEV_JSON = os.path.join(DATASETS_BUCKET, "raw", "squad", "dev-v2.0.json")

ALL = os.path.join(DATASETS_BUCKET, "processed", "squad", "all.jsonl")

TRAIN = os.path.join(DATASETS_BUCKET, "processed", "squad", "train.jsonl")
VAL = os.path.join(DATASETS_BUCKET, "processed", "squad", "val.jsonl")
TEST = os.path.join(DATASETS_BUCKET, "processed", "squad", "test.jsonl")


def get_examples() -> Iterator[Example]:
    """
    Reads examples from the raw [train|dev]-v2.0.json files and yields them
    """
    for raw in [RAW_TRAIN_JSON, RAW_DEV_JSON]:
        with open(raw) as f:
            data = json.load(f)

        for topic in data["data"]:
            for paragraph in topic["paragraphs"]:
                context = paragraph["context"]
                for qa in paragraph["qas"]:
                    question = qa["question"]
                    is_impossible = bool(qa["is_impossible"])
                    if not is_impossible:
                        for x in qa["answers"]:
                            answer = x["text"]
                            answer_start = x["answer_start"]
                            # noinspection PyArgumentList
                            ex = Example(
                                text_context=context,
                                question=question,
                                answer_start=answer_start,
                                answer_end=answer_start + len(answer),
                                answerable=True,
                            )
                            yield ex
                    else:
                        # noinspection PyArgumentList
                        ex = Example(text_context=context, question=question)
                        yield ex
