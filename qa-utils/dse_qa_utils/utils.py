from smart_open import open
from typing import Iterator
import logging
import json

try:
    from tqdm import tqdm
except ImportError:
    tqdm = iter

logger = logging.getLogger(__name__)


def read_jsonl(location: str) -> Iterator:
    logger.info(f"reading JSON lines from {location}")
    line_no = 0
    with open(location) as f:
        for line in f:
            line_no += 1
            line = line.strip()
            if line == "":
                continue
            try:
                yield json.loads(line)
            except ValueError:
                raise ValueError(f"Invalid JSON on line {line_no}: {line}")


def write_jsonl(location: str, lines: Iterator, append: bool = False):
    if append:
        logger.info(f"appending JSON lines to {location}")
    else:
        logger.info(f"writing JSON lines to {location}")
    if append:
        mode = "a"
    else:
        mode = "w"
    n = 0
    with open(location, mode) as f:
        for line in lines:
            f.write(json.dumps(line) + "\n")
            n += 1
    logger.info(f"{'appended' if append else 'wrote'} {n} lines to {location}")
