from typing import Optional, Tuple, List, Iterator, Iterable
from pydantic.dataclasses import dataclass
from dataclasses import asdict
from pydantic import validator
from bs4 import BeautifulSoup
from bs4.element import Tag
import json
from dse_qa_utils.utils import write_jsonl, read_jsonl


ANSWER_TAG_NAME = "span"
ANSWER_CLASS = "answer"
ANSWER_TAG = Tag(name=ANSWER_TAG_NAME, attrs={"class": ANSWER_CLASS})


@dataclass
class Example:
    """
    The main QA training example data structure

    html_context is expected to be supported in the future, but not fully tested now

    Note that since text_context is not tokenized, answer_start and answer_end should be the index of the character(s), not tokens
    """

    question: str
    answerable: bool = False
    html_context: Optional[str] = None
    text_context: str = ""
    answer_start: Optional[int] = None
    answer_end: Optional[int] = None

    def to_json(self):
        return json.dumps(asdict(self))

    @property
    def answer(self) -> Optional[str]:
        if self.answerable:
            return self.text_context[self.answer_start : self.answer_end]
        return None

    def __post_init__(self):
        if self.answerable and self.html_context:
            if self.answer_start is None or self.answer_end is None:
                self.answer_start, self.answer_end = html_to_answer_indices(
                    self.html_context
                )

    # noinspection PyMethodParameters
    @validator("html_context")
    def html_contains_answer_if_answerable(cls, value, values):
        if values.get("answerable"):
            soup = BeautifulSoup(value, features="html.parser")
            if soup.find(ANSWER_TAG_NAME, attrs={"class": ANSWER_CLASS}):
                return value
            raise ValueError(
                f"{cls.__name__} is `answerable` but no {ANSWER_TAG} tag found"
            )
        return value

    # noinspection PyMethodParameters
    @validator("text_context")
    def set_text_context(cls, value, values):
        if not value:
            html_context = values.get("html_context")
            if not html_context:
                raise ValueError(
                    f"{cls.__name__} must have at least one of `text_context` or `html_context`"
                )
            return BeautifulSoup(html_context, features="html.parser").text
        return value

    # noinspection PyMethodParameters
    @validator("answer_end", "answer_start", always=True)
    def check_answer_is_set(cls, value, values):
        if values.get("answerable") and value is None:
            raise ValueError(f"{cls.__name__} must have answer if `answerable`")
        return value


def save_examples(filename: str, examples: Iterable[Example]):
    write_jsonl(filename, map(asdict, examples))


def load_examples(filename: str) -> Iterator[Example]:
    for raw in read_jsonl(filename):
        yield Example(**raw)


@dataclass
class Dataset:
    examples: List[Example]

    def to_json(self, filename):
        write_jsonl(filename, map(asdict, self.examples))


def html_to_answer_indices(html: str) -> Tuple[int, int]:
    soup = BeautifulSoup(html, features="html.parser")
    answer = soup.find(ANSWER_TAG_NAME, attrs={"class": ANSWER_CLASS})
    start = index_within_soup(answer)
    return start, start + len(answer.text)


def embed_answer_in_html(
    answer_text: str, html: str, answer_start: int, answer_end: int
) -> str:
    pass


def index_within_soup(tag: Tag) -> int:
    index = 0
    while not isinstance(tag, BeautifulSoup):
        index += index_within_parent(tag)
        tag = tag.parent
    return index


def index_within_parent(tag: Tag) -> int:
    index = 0
    for sibling in tag.previous_siblings:
        if isinstance(sibling, Tag):
            index += len(sibling.text)
        else:
            index += len(sibling)
    return index
