import os
import secrets
from PIL import Image
from flask import render_template, url_for, flash, redirect, request, abort, current_app, jsonify
from ui import app, db, bcrypt, mail
from ui.forms import (RegistrationForm, LoginForm, UpdateAccountForm,
                             PostForm, RequestResetForm, ResetPasswordForm)
from ui.models import User, Post, Documents
from flask_login import login_user, current_user, logout_user, login_required
from flask_mail import Message
from flask import Flask, render_template, request
from flask_dropzone import Dropzone
from flask_uploads import UploadSet, configure_uploads, IMAGES, patch_request_class
import os
import flask, json
import requests
from sqlalchemy import desc
from flask_marshmallow import Marshmallow
from ast import literal_eval
import json,urllib.request
import collections
columns = ['document_1','document_2', 'document_3', 'document_4', 'document_5']
from ui.question_service import get_similar_questions

dropzone = Dropzone(app)


application_response = None
SQUAD_URL = 'http://10.160.42.133'
SOLR_URL = 'http://solr:8983/solr/prm/select?fl=*%2Cscore&q='
CERT = False#'nginx.crt'

# Dropzone settings
app.config['DROPZONE_UPLOAD_MULTIPLE'] = True
app.config['DROPZONE_ALLOWED_FILE_CUSTOM'] = True
app.config['DROPZONE_ALLOWED_FILE_TYPE'] = 'image/*'
app.config['DROPZONE_REDIRECT_VIEW'] = 'results'
# Uploads settings
app.config['UPLOADED_PHOTOS_DEST'] = os.getcwd() + '/uploads'
photos = UploadSet('photos', IMAGES)
configure_uploads(app, photos)
patch_request_class(app)  # set maximum file size, default is 16MB



from ui.flask_pager import Pager
app.secret_key = os.urandom(42)
app.config['PAGE_SIZE'] = 1
app.config['VISIBLE_PAGE_COUNT'] = 10


@app.route("/page")
def index():
    return render_template('index.html')


@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('error.html')

@app.route("/")
def home():
    return render_template('home.html')


def get_response(get_id):
    jsonurl = urllib.request.urlopen(app.config['BACKEND_SERVER'] + app.config['BACKEND_WAIT_ENDPOINT'] + get_id['result_guid']).read()
    data = json.loads(jsonurl.decode('utf-8'))

    while(data['-2']['status'] == 'running'):
        jsonurl = urllib.request.urlopen(app.config['BACKEND_SERVER'] + app.config['BACKEND_WAIT_ENDPOINT'] + get_id['result_guid']).read()
        data = json.loads(jsonurl.decode('utf-8'))
    return data


@app.route("/response/", methods = ['POST'])
# @login_required
def response():
    d = {"root_": "", ".txt": "", "_": " "}
    best_answer = 'Error'
    doc_id = 'Error'
    answer_id = 'Error'
    html_data = 'Error'
    context_list = []

    search_request = request.get_json()
    context = search_request['cont']
    query = search_request['query']
    if len(query) > 1:
        result = requests.post(app.config['BACKEND_SERVER'] + app.config['BACKEND_ENDPOINT'], json={'question': query, 'fq': context})
        get_id = result.json()

        response = get_response(get_id)

        print(response)

        best_answer = response['-1']['answer_str']
        doc_id = response['-1']['docid']
        answer_id = response['-1']['answer_id']
        html_data = response[str(doc_id)]['html']
        updated_html_clean_context = response[str(doc_id)]['context']
        similar_questions = []
            # get_similar_questions(query, Post)
        context_answers_html = []
        jarray2 = collections.OrderedDict()

        # print('Similar Questions:' , similar_questions)
        for element in response:
            if element != '-1' and element != '-2':
                jarray2 = collections.OrderedDict()
                answer_list = []
                response[element]['doc_id'] = replace_all(response[element]['doc_id'], d)
                context_list.append(replace_all(response[element]['doc_id'], d))
                jarray2['context'] = replace_all(response[element]['doc_id'], d)
                jarray2['html'] = response[element]['html']
                for context_id in response[element]:
                    if not isinstance(response[element][context_id], float):
                        if ('answer_str' in response[element][context_id]):
                            if response[element][context_id]['answer_str']:
                                answer_list.append(response[element][context_id]['answer_str'])
                jarray2['answer_list'] = answer_list
                context_answers_html.append(jarray2)
        best_doc = context_answers_html[doc_id]
        del context_answers_html[doc_id]
        context_answers_html.insert(0, best_doc)


        # print(answer_list)
        return jsonify({'context_answers_html': context_answers_html, 'best_context': doc_id,  'response': response})

    else:
            abort(401)

@app.route('/_get_data/', methods=['POST'])
def _get_data():
    d = {"root_": "", ".txt": "", "_": " "}
    data_change_request = request.get_json()
    context = data_change_request['cont']
    data = data_change_request['data']
    with open('response.json', 'w') as outfile:
        json.dump(data, outfile)

    answer_list = []
    for element in data:
        if(element != '-1' and element != '-2'):
            if replace_all(data[element]['doc_id'],d).lower() == context.lower():
                html = data[element]['html']
                updated_html_clean_context = data[element]['context']
                for answer in data[element]:
                    if not isinstance(data[element][answer], float):
                        if('answer_str' in data[element][answer]):
                            if data[element][answer]['answer_str']:
                                answer_list.append(data[element][answer]['answer_str'])
    myList = ['Element1', 'Element2', 'Element3']

    return jsonify({'updated_html': html, 'updated_html_clean_context': updated_html_clean_context, 'answer_list': answer_list})

@app.route("/upload")
@login_required
def upload():
  return render_template("upload.html")


@app.route('/results')
@login_required
def results():
    return render_template('results.html')



@app.route("/about")
def about():
    return render_template('about.html', title='About')

@app.route("/qa")
# @login_required
def qa():
    return flask.render_template('qa.html')


@app.route("/get_qa/", methods=['POST'])
# @login_required
def get_qa():
    try:
        data = flask.request.get_json()
        res = requests.post(SQUAD_URL, json=data, verify=CERT)
        return flask.jsonify(res.json())
    except Exception as e:
        print(str(e), e.args)
        return flask.jsonify({'msg':'got it'})
    return flask.render_template('qa.html')



    #
    # if flask.request.method == 'GET':
    #     return flask.render_template('qa.html')
    # if flask.request.method == 'POST':
    #     try:
    #         data = flask.request.get_json()
    #         res = requests.post(SQUAD_URL,json=data,verify=CERT)
    #         return flask.jsonify(res.json())
    #     except Exception as e:
    #         print(str(e),e.args)
    #     return flask.jsonify({'msg':'got it'})
    # return flask.render_template('qa.html')


@app.route("/stats")
def stats():
    return render_template('stats.html')

@app.route("/tables")
def tables():
    return render_template('tables.html')

@app.route("/posts")
@login_required
def posts():
    posts = Post.query.order_by(desc(Post.date_posted)).all()
    return render_template('post.html', title='QA', posts=posts)


@app.route("/get_posts")
def get_posts():
    posts = Post.query.order_by(desc(Post.date_posted)).all()

    ma = Marshmallow(app)

    class PostsSchema(ma.Schema):
        class Meta:
            fields = ('question', 'expected_answer', 'system_generated_answer', 'question_context', 'answer_context', 'system_generated_answer', 'answers', 'updated_html', 'date_posted')

    postsschema = PostsSchema()
    postsschema = PostsSchema(many=True)

    result = postsschema.dump(posts)
    return jsonify(result)




@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    print('received request')
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    print('now to check')
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            print('user logged in')
            next_page = request.args.get('next')
            print('worked till here: Next Page - ', next_page)
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))


def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


@app.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your account has been updated!', 'success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename='profile_pics/' + current_user.image_file)
    return render_template('account.html', title='Account',
                           image_file=image_file, form=form)


def create_new_post(search_request):
    positive_vote = 0
    negative_vote = 0
    column = search_request['vote'].index(1)
    initial_vote = [None, None, None, None, None]
    initial_vote[column] = search_request['positive']
    post = Post(question=search_request['question'].lower(),
                question_context=search_request['context'],
                document_1=initial_vote[0],
                document_2=initial_vote[1],
                document_3=initial_vote[2],
                document_4=initial_vote[3],
                document_5=initial_vote[4],
                author=current_user)

    update_new_vote(post, search_request)

def update_new_vote(post_record, search_request):
    total_positive_vote =0
    total_negative_vote =0
    document_record = Documents.query.filter_by(question=post_record.question,
                                                document=search_request['document_context']).first()
    if not document_record:
        if search_request['positive']:
            total_positive_vote = 1
        else:
            total_negative_vote = 1

        answers = [None, None, None, None, None]
        answers[:len(search_request['answers'])] = search_request['answers']
        document_record = Documents(question=search_request['question'].lower(),
                             question_context=search_request['context'],
                             document=search_request['document_context'],
                             answer_1=answers[0],
                             answer_2=answers[1],
                             answer_3=answers[2],
                             answer_4=answers[3],
                             answer_5=answers[4],
                             total_positive_vote=total_positive_vote,
                             total_negative_vote=total_negative_vote)
        db.session.add(post_record)
        db.session.add(document_record)
        db.session.commit()
        return

    if search_request['positive']:
        document_record.total_positive_vote += 1
    else:
        document_record.total_negative_vote += 1

    db.session.add(post_record)
    db.session.add(document_record)
    db.session.commit()
    resp = jsonify(success=True)
    return resp


def update_existing_vote(post_record, search_request):
    total_positive_vote =0
    total_negative_vote =0
    document_record = Documents.query.filter_by(question=post_record.question,
                                                document=search_request['document_context']).first()
    if not document_record:
        update_new_vote(post_record, search_request)
    if search_request['positive']:
        document_record.total_positive_vote += 1
        if(document_record.total_negative_vote !=0):
            document_record.total_negative_vote -= 1
    else:
        document_record.total_negative_vote += 1
        if (document_record.total_positive_vote != 0):
            document_record.total_positive_vote -= 1

    db.session.add(post_record)
    db.session.add(document_record)
    db.session.commit()
    resp = jsonify(success=True)
    return resp


@app.route("/post/new", methods=['GET', 'POST'])
@login_required
def new_post():
    search_request = request.get_json()

    #### Search if Post Exists

    post_record = Post.query.filter_by(question=search_request['question'].lower(),
                                       question_context=search_request['context'], author = current_user).first()
    if post_record:   # If Exists Skip
        column = search_request['vote'].index(1)
        if columns[column] == 'document_1':
            if post_record.document_1 != search_request['positive']:
                if post_record.document_1==None:
                    post_record.document_1 = search_request['positive']
                    update_new_vote(post_record, search_request)
                else:
                    post_record.document_1 = search_request['positive']
                    update_existing_vote(post_record, search_request)

        if columns[column] == 'document_2':
            if post_record.document_2 != search_request['positive']:
                if post_record.document_2 == None:
                    post_record.document_2 = search_request['positive']
                    update_new_vote(post_record, search_request)
                else:
                    post_record.document_2 = search_request['positive']
                    update_existing_vote(post_record, search_request)

        if columns[column] == 'document_3':
            if post_record.document_3 != search_request['positive']:
                if post_record.document_3 == None:
                    post_record.document_3 = search_request['positive']
                    update_new_vote(post_record, search_request)
                else:
                    post_record.document_3 = search_request['positive']
                    update_existing_vote(post_record, search_request)

        if columns[column] == 'document_4':
            if post_record.document_4 != search_request['positive']:
                if post_record.document_4 == None:
                    post_record.document_4 = search_request['positive']
                    update_new_vote(post_record, search_request)
                else:
                    post_record.document_4 = search_request['positive']
                    update_existing_vote(post_record, search_request)

        if columns[column] == 'document_5':
            if post_record.document_5 != search_request['positive']:
                if post_record.document_5 == None:
                    post_record.document_5 = search_request['positive']
                    update_new_vote(post_record, search_request)
                else:
                    post_record.document_5 = search_request['positive']
                    update_existing_vote(post_record, search_request)
        else:
            resp = jsonify(success=True)
            return resp

    else:
        #### Add New Post
        create_new_post(search_request)
        resp = jsonify(success=True)
        return resp




@app.route("/post/vote", methods=['GET', 'POST'])
@login_required
def new_vote():

    resp = jsonify(success=True)
    return resp

@app.route("/post/<int:post_id>")
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.question, post=post)


@app.route("/post/<int:post_id>/update", methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        db.session.commit()
        flash('Your post has been updated!', 'success')
        return redirect(url_for('post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('create_post.html', title='Update Post',
                           form=form, legend='Update Post')


@app.route("/post/<int:post_id>/delete", methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Your post has been deleted!', 'success')
    return redirect(url_for('home'))


@app.route("/user/<string:username>")
def user_posts(username):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(author=user)\
        .order_by(Post.date_posted.desc())\
        .paginate(page=page, per_page=5)
    return render_template('user_posts.html', posts=posts, user=user)


def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Password Reset Request',
                  sender='noreply@demo.com',
                  recipients=[user.email])
    msg.body = f'''To reset your password, visit the following link:
{url_for('reset_token', token=token, _external=True)}

If you did not make this request then simply ignore this email and no changes will be made.
'''
    mail.send(msg)


@app.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('An email has been sent with instructions to reset your password.', 'info')
        return redirect(url_for('login'))
    return render_template('reset_request.html', title='Reset Password', form=form)


@app.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('That is an invalid or expired token', 'warning')
        return redirect(url_for('reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('Your password has been updated! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('reset_token.html', title='Reset Password', form=form)


@app.route('/query',methods=['GET','POST'])
@login_required
def query():
    if flask.request.method == 'GET':
        return flask.render_template('query_page_aws.html')
    if flask.request.method == 'POST':
        try:
            data = flask.request.get_json()
            # print(data)
            # r = requests.get(SOLR_URL+data['question']+'&fq=id:*'+data['fq']+'*')
            # res = {j:{} for j in range(n_solr_docs)}
            # res[-1] = {'overall':0,'docid':'','answer_str':''}
            # reqs = []
            # for doc_i,doc in enumerate(r.json()['response']['docs'][:n_solr_docs]):
            #     print(doc)
            #     nm = doc['id'].split('/')[-1]
            #     res[doc_i]['doc_id'] = nm
            #     res[doc_i]['score'] = doc['score']
            #     context = doc['content'][0].strip()
            #     res[doc_i]['context'] = context
            #     try:
            #         with open(PRM_HTML+nm.replace('.txt','.html'),'r') as f:
            #             html = f.read()
            #         res[doc_i]['html'] = html
            #     except:
            #         print('didnt find ',PRM_HTML+nm.replace('.txt','.html'))
            #         res[doc_i]['html'] = context
            #     # print(context)
            #     result = requests.post(SQUAD_URL,json={'context':context,'question':data['question']},verify=CERT).json()
            with open('/Users/axg143/Workspace/code_snippets/Python/Flask_Blog/10-Password-Reset-Email/flaskblog/sample.json', 'r') as myfile:
                data = myfile.read()

            result = json.loads(data)

            res = {j: {} for j in range(5)}
            # res[-1] = {'overall':0,'docid':'','answer_str':''}
            # reqs = []
            for i in range(5):
                for k,v in result.items():
                    # print(v)
                    res[i][k] = v
                    if ((res[i]['score']*v['likelihood'] > res[-1]['overall']) and
                            (v['answer_str'] != '')):
                        res[-1] = {'overall':res[i]['score']*v['likelihood'],
                        'docid':i,'answer_str':v['answer_str'],
                        'answer_id':k}
            return flask.jsonify(res)
        except Exception as e:
            print(str(e),e.args)
            # for k,v in res.items():
            #     # print(k)
            #     for k1,v1 in v.items():
            #         print('\t',k1)
            #         print('\t',v1)
            #     print()
        return flask.jsonify({'msg':'got it'})


def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
        text = text.capitalize()
    return text