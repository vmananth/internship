from flask_marshmallow import Marshmallow
from ui.models import User, Post
import requests
from ui import app
from sqlalchemy import desc
from bert_serving.client import BertClient

import numpy as np
from termcolor import colored

def get_similar_questions(question, Post):

    prefix_q = '##### **Q:** '
    topk = 4
    posts = Post.query.order_by(desc(Post.date_posted)).all()
    answer_lists = []
    question_list = []
    similar_questions_string = []
    if posts:
        for Post in posts:
            if Post.get_question() not in question_list:
                question_list.append(Post.get_question())
                answer_lists.append(Post.get_expected_answer())
        bc = BertClient(ip = app.config['BERT_SERVER'])
        doc_vecs = bc.encode(question_list)
        query_vec = bc.encode([question])[0]
        score = np.sum(query_vec * doc_vecs, axis=1) / np.linalg.norm(doc_vecs, axis=1)
        topk_idx = np.argsort(score)[::-1][:topk]
        similar_questions = []
        for idx in topk_idx:
            similar_questions_string.append({"question": question_list[idx], "answer": answer_lists[idx]})

    return similar_questions_string

