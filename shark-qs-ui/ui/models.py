from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from ui import db, login_manager, app
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    password = db.Column(db.String(60), nullable=False)
    up_vote_question_ids = db.Column(db.String(1000))
    down_vote_question_ids = db.Column(db.String(1000))
    posts = db.relationship('Post', backref='author', lazy=True)

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"


class Post(db.Model):
    doc_col_name = ['document_1', 'document_2', 'document_3', 'document_4', 'document_5']

    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.Text, nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    question_context = db.Column(db.Text, nullable=False)
    document_1 = db.Column(db.BOOLEAN, nullable=True, default=None)
    document_2 = db.Column(db.BOOLEAN, nullable=True, default=None)
    document_3 = db.Column(db.BOOLEAN, nullable=True, default=None)
    document_4 = db.Column(db.BOOLEAN, nullable=True, default=None)
    document_5 = db.Column(db.BOOLEAN, nullable=True, default=None)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return f"Post('{self.question}', '{self.date_posted}')"

    def get_question(self):
        return self.question

    def get_expected_answer(self):
        return self.expected_answer

class Documents(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.Text, nullable=False)
    question_context = db.Column(db.Text, nullable=False)
    document = db.Column(db.Text, nullable=False, default='empty')
    answer_1 = db.Column(db.Text, nullable=False, default='empty')
    answer_2 = db.Column(db.Text, nullable=False, default='empty')
    answer_3 = db.Column(db.Text, nullable=False, default='empty')
    answer_4 = db.Column(db.Text, nullable=False, default='empty')
    answer_5 = db.Column(db.Text, nullable=False, default='empty')
    total_positive_vote = db.Column(db.Integer, nullable=False, default=0)
    total_negative_vote = db.Column(db.Integer, nullable=False, default=0)



# class Votes(db.Model):
#     id = db.Column(db.Integer , primary_key=True , autoincrement=True)
#     question = db.Column(db.Text, nullable=False)
#     up_votes = db.Column('up_votes', nullable=False)
#     down_votes = db.Column(db.Integer, nullable=False)
#     object_dump = db.Column(db.JSON, nullable= False)
#
