$("#formoid").submit(function (event) {



    $("#replace_view").hide();
    $('#company_logo').show();

    /* stop form from submitting normally */

            $(document).ajaxStart(function () {
                $("#overlay").css("display", "block");
                $("#search_submit").prop('disabled', true);
                $("#main").css("pointer-events", "none");
            });
            $(document).ajaxComplete(function () {
                $("#overlay").css("display", "none");
                 $("#search_submit").prop('disabled', false);
                 $("#main").css("pointer-events", "auto");
            });


    event.preventDefault();

    /* get the action attribute from the <form action=""> element */
    var question = {
        cont: $('.nav-tabs .active').attr('data-value'),
        query: $('#text_query').val()
    };


    $.ajax({
        url: "/response/",
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        success: function (resp) {
            console.log(resp.data);
            context_answers_html = resp.context_answers_html;
            best_context = resp.best_context;
            full_response = resp.response;

            var store_list = context_answers_html
            $('#qa').show();
            $('#context-section').show();
            $('#context-section2').show();

            $(".context1-btn").text(store_list[0]['context'])
            $(".context2-btn").text(store_list[1]['context'])
            $(".context3-btn").text(store_list[2]['context'])
            $(".context4-btn").text(store_list[3]['context'])
            $(".context5-btn").text(store_list[4]['context'])



            $('#answer_group1').html("");
            for(answer in store_list[0]['answer_list'])
            $('#answer_group1').append(
            '  <a id = "link" href="#a" class="row">' + answer +'. '+store_list[0]['answer_list'][answer]+ '  </a>\n')


            $('#answer_group2').html("");
            for(answer in store_list[1]['answer_list'])
            $('#answer_group2').append(
            '  <a id = "link" href="#a" class="row">' + answer +'. ' + store_list[1]['answer_list'][answer]+ '  </a>\n')


            $('#answer_group3').html("");
            for(answer in store_list[2]['answer_list'])
            $('#answer_group3').append(
            '  <a id = "link" href="#a" class="row">' + answer +'. ' + store_list[2]['answer_list'][answer]+ '  </a>\n')


            $('#answer_group4').html("");
            for(answer in store_list[3]['answer_list'])
            $('#answer_group4').append(
            '  <a id = "link" href="#a" class="row">' + answer +'. ' + store_list[3]['answer_list'][answer]+ '  </a>\n')


            $('#answer_group5').html("");
            for(answer in store_list[4]['answer_list'])
            $('#answer_group5').append(
            '  <a id = "link" href="#a" class="row">' + answer +'. ' + store_list[4]['answer_list'][answer]+ '  </a>\n')


            $("#collapseanswers1").addClass('show');
            $("#index1").addClass('show')
            $(".context1-btn").prop('enabled', true);




            $('div#index_content1').html("");
            $('div#index_content1').append(store_list[0]['html']);

            $('div#index_content2').html("");
            $('div#index_content2').append(store_list[1]['html']);

            $('div#index_content3').html("");
            $('div#index_content3').append(store_list[2]['html']);

            $('div#index_content4').html("");
            $('div#index_content4').append(store_list[3]['html']);

            $('div#index_content5').html("");
            $('div#index_content5').append(store_list[4]['html']);



            $('div#full').html("");
            $('div#full').data(full_response)

            $('div#updated_html_clean_context').html("");
            $('div#updated_html_clean_context').append(updated_html_clean_context)


        },
        data: JSON.stringify(question)
    });
});