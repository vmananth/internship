$(document).on('click', '#context2', function () {

    $('.list-group-item').removeClass('active');
    $(this).closest('.list-group-item').addClass('active');


    $(document).ajaxStart(function () {
                $("#overlay").css("display", "none");
            });
            $(document).ajaxComplete(function () {
                $("#overlay").css("display", "none");
            });

    var context = {
        cont: $(this).text(),
        data: $('#full').data()
    }

    $.ajax({
        url: "/_get_data/",
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        async:false,
        success: function (resp) {
            console.log(resp.data)
            updated_html = resp.updated_html
            updated_html_clean_context = resp.updated_html_clean_context
            updated_answers = resp.answer_list

            // $('#answers_group').html("");
            // for (answer in updated_answers) {
            //     $('#answers_group').append('<li class="list-group-item">' + updated_answers[answer] + '<label class="switch "><input type="checkbox" class="primary" name="type" value="' + updated_answers[answer] +'"> <span class="slider round"></span> </label></li>');
            // }

            $('#answers_group').html("");
            for (answer in updated_answers) {
                // $('#answers_group').append('<li class="list-group-item">' + updated_answers[answer] + '<label class="switch "><input type="checkbox" class="primary" name="type" value="' + updated_answers[answer] +'"> <span class="slider round"></span> </label></li>');
                $('#answers_group').append('<div class="list-group">\n' +
                    '  <a href="#a" class="list-group-item" onclick= "return check()">' + updated_answers[answer] +
                    '<label class="switch "><input type="checkbox" class="primary" name="type" value="' + updated_answers[answer] +'"> <span class="slider round"></span> </label></li>\n' +
                    '  </a>\n' +
                    '</div>')
            }


            $('div#response').html("");
            $('div#response').append(updated_html);

            $('div#current-html').html("");
            $('div#current-html').append(updated_html);

            $('div#updated_html_clean_context').html("");
            $('div#updated_html_clean_context').append(updated_html_clean_context)


            // for (i in updated_answers) {
            //
            //     split_string = updated_answers[i].split(" ")
            //     end = split_string.length - 1
            //
            //     // var r = new RegExp(split_string[0] + "(.*)" + split_string[end]);
            //     // while(document.getElementById("response").innerHTML.match(r)) {
            //     //     end = end - 1
            //     // }
            //
            //     var r = new RegExp(split_string[0] + "(.*)" + split_string[end]);
            //     document.getElementById("response").innerHTML = document.getElementById("response").innerHTML.replace(r, '<mark style="background-color: #99ff99">' + updated_answers[i] + '</mark>');
            //
            // }



            // $('div#full').html("");
            // $('div#full').append(updated_html);

            // var $doc = new DOMParser().parseFromString(updated_html, "text/html");
            // $('div#full').html("");
            // $('div#full', $doc);
        },
        data: JSON.stringify(context)
    });

});