from ui.config import local_config
from ui.config import amfamlabs_config
from ui.config import dto_aws_config

LOCAL = 'local'
DEV = 'dto_aws'
PROD = 'amfamlab_aws'

BERT_SERVER = None
BACKEND_SERVER = None
BACKEND_ENDPOINT = None
BACKEND_WAIT_ENDPOINT = None

def set_env_variables(app, environment):
    if environment == LOCAL:
        app.config['BERT_SERVER'] = local_config.BERT_SERVER_IP
        app.config['BACKEND_SERVER'] = local_config.BERT_SERVER_IP
        app.config['BACKEND_ENDPOINT'] = local_config.QUERY_ENDPOINT
    elif environment == DEV:
        app.config['BERT_SERVER'] = dto_aws_config.BERT_SERVER_IP
        app.config['BACKEND_SERVER'] = dto_aws_config.BERT_SERVER_IP
        app.config['BACKEND_ENDPOINT'] = dto_aws_config.QUERY_ENDPOINT
    elif environment == PROD:
        app.config['BERT_SERVER'] = amfamlabs_config.BERT_SERVER_IP
        app.config['BACKEND_SERVER'] = amfamlabs_config.QA_BACKEND_SERVER
        app.config['BACKEND_ENDPOINT'] = amfamlabs_config.QUERY_ENDPOINT
        app.config['BACKEND_WAIT_ENDPOINT'] = amfamlabs_config.WAIT_ENDPOINT
    return app

