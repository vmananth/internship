from sqlalchemy import create_engine, Column, Integer, Sequence, String, Date, Float, BIGINT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData
from sqlalchemy import *
from ui.config import amfamlabs_config
from ui.models import Post
from ui import db
engine = create_engine(amfamlabs_config.MYSQL_AMFAM_LAB)

# # engine.execute('CREATE TABLE "models" ('
# #                'model VARCHAR,'
# #                'exact Float NOT NULL,'
# #                'f1 Float NOT NULL,'
# # 	           'seq_length VARCHAR,'
# #                'learning_rate VARCHAR,'
# #                'epoch INTEGER NOT NULL,'
# #                'batch_size INTEGER NOT NULL,'
# #                'hardware VARCHAR,'
# #                'time_taken INTEGER NOT NULL,'
# # 	       'PRIMARY KEY (model));')
#
# # engine.execute('INSERT INTO "models" '
# #                '(model, exact, f1, seq_length, learning_rate, epoch, batch_size, hardware, time_taken) '
# #                'VALUES ("Bert_Uncased_Small_Current_Model",60.94, 82.57, 512, "1E-05", 4, 12, "3GPU", 3)')
#
# # metadata = MetaData(engine, reflect=True)
# # users = Table('models', metadata,
# #     Column('model', String(30), primary_key=True),
# #     Column('exact', Float),
# #     Column('f1', Float),
# #     Column('seq_length', String(10)),
# #     Column('learning_rate', String(10)),
# #     Column('epoch', Integer),
# #     Column('batch_size', INTEGER),
# #     Column('hardware', String(10)),
# #     Column('time_taken', Integer)
# # )
# #
# # users.create()
# #
# # i = users.insert()
# # i.execute(model='BERT_Uncased_Small_Current_Model', exact=60.94, f1=82.57, seq_length=512, learning_rate="1E-05", epoch=4, batch_size=12, hardware="3GPU",time_taken=3 )
# # i.execute(model='BERT_Uncased_Small_New_Model', exact=60.26, f1=81.75, seq_length=512, learning_rate="1E-05", epoch=4, batch_size=12, hardware="3GPU",time_taken=3 )
# # i.execute(model='BERT_Uncased_Large_Model', exact=65.37, f1=84.27, seq_length=512, learning_rate="1E-05", epoch=3, batch_size=3, hardware="3GPU",time_taken=9 )
# # i.execute(model='AMFAM_BERT_Model', exact=61.01, f1=83.05, seq_length=512, learning_rate="1E-05", epoch=4, batch_size=12, hardware="3GPU",time_taken=3 )
# # i.execute(model='AMFAM_BERTFAM_Model', exact=57.29, f1=79.92, seq_length=512, learning_rate="1E-05", epoch=4, batch_size=12, hardware="3GPU",time_taken=3 )
# # i.execute(model='XLNet', exact=83.72, f1=86.19, seq_length=512, learning_rate="1E-05", epoch=3, batch_size=12, hardware="4GPU",time_taken=6 )
#
#
base = declarative_base()
metadata = MetaData(engine, reflect=True)
table = metadata.tables.get("Document")
if table is not None:
   base.metadata.drop_all(engine, [table], checkfirst=False)
   print('deleted')

#
#
#
# metadata = MetaData(engine, reflect=True)
# users = Table('votes', metadata,
#     Column('id', Integer , primary_key=True , autoincrement=True),
#     Column('question', String(60), primary_key=True),
#     Column('up_votes', BIGINT),
#     Column('down_votes', BIGINT),
#     Column('object_dump', JSON),
#     Column('answers', String(300))
# )
#
# users.create()




# User.query.filter(User.id == 123).delete()

# Post.query.filter_by(question_context="").delete()
# db.session.commit()
#
# def add_column(engine, table_name, column):
#     column_name = column.compile(dialect=engine.dialect)
#     column_type = column.type.compile(engine.dialect)
#     engine.execute('ALTER TABLE %s ADD COLUMN %s %s' % (table_name, column_name, column_type))
#
# column = Column('up_vote_question_ids', String(1000), primary_key=True)
# add_column(engine, "user", column)