from flask_marshmallow import Marshmallow
from ui.models import User, Post
import requests
from sqlalchemy import desc
from bert_serving.client import BertClient

import numpy as np
from termcolor import colored

from ui import db

prefix_q = '##### **Q:** '
topk = 4
posts = Post.query.order_by(desc(Post.date_posted)).all()
answer_lists = []
question_list = []
for Post in posts:
    if Post.get_question() not in question_list:
        question_list.append(Post.get_question())

bc = BertClient()

doc_vecs = bc.encode(question_list)
while True:
    query = input('your question: ')
    query_vec = bc.encode([query])[0]
    # compute normalized dot product as score
    score = np.sum(query_vec * doc_vecs, axis=1) / np.linalg.norm(doc_vecs, axis=1)
    topk_idx = np.argsort(score)[::-1][:topk]
    for idx in topk_idx:
        print('> %s\t%s' % (score[idx], question_list[idx]))